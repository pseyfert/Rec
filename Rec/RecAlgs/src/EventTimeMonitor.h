/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef EVENTTIMEMONITOR_H
#define EVENTTIMEMONITOR_H 1

// Include files
#include "Event/ODIN.h" // event & run number

// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"

/** @class EventTimeMonitor EventTimeMonitor.h
 *
 *  Creates histograms of event time
 *
 *  @author Patrick Koppenburg
 *  @date   2012-04-19
 */

class EventTimeMonitor final
    : public Gaudi::Functional::Consumer<void( const LHCb::ODIN& ),
                                         Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  /// Standard constructor
  EventTimeMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;                          ///< Algorithm initialization
  void       operator()( const LHCb::ODIN& ) const override; ///< Algorithm execution

private:
  /// the histogram definition (as property)
  Gaudi::Property<Gaudi::Histo1DDef> m_histoS{
      this, "SecondsPlot", {"GPS Seconds", 0, 3600., 3600}, "The parameters of 'delta memory' histogram"};
  Gaudi::Histo1DDef           m_histoH{"GPS Hours", -0.5, 23.5, 24};    // the histogram definition (as property)
  Gaudi::Histo1DDef           m_histoD{"GPS Days", 0.5, 365.5, 366};    // the histogram definition (as property)
  Gaudi::Histo1DDef           m_histoY{"GPS Year", 2008.5, 2028.5, 20}; // the histogram definition (as property)
  mutable AIDA::IHistogram1D* m_plotS = nullptr;                        // the histogram of seconds
  mutable AIDA::IHistogram1D* m_plotH = nullptr;                        // the histogram of hours
  mutable AIDA::IHistogram1D* m_plotD = nullptr;                        // the histogram of day of year
  mutable AIDA::IHistogram1D* m_plotY = nullptr;                        // the histogram of year
};

#endif // EVENTTIMEMONITOR_H
