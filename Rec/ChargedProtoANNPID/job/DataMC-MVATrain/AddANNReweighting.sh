#!/bin/bash
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#export ROOTDIR="/r03/lhcb/jonesc/ANNPID/ProtoParticlePIDtuples/Test/V1/StripFiltered/"
export ROOTDIR="/Volumes/2TB-Mobile/ANNPID/StripFiltered/"

export WORKDIR="output/weights"
export MVADIR=${ROOTDIR}"WithMVAs/V2/"

mkdir -p ${MVADIR}

export FILE="2015-S23r1-ANNPID-BHADRONCOMPLETEEVENT.root"
TMVAReader.exe --TreeName ANNPID/DecayTree --InRootFiles ${ROOTDIR}${FILE} --OutRootFile ${MVADIR}${FILE} --WorkDir ${WORKDIR} --Inputs TMVAInputs-Long.txt 

export FILE="Incb-DevMCJun2015-S23r1-ANNPID-Pythia8.root"
TMVAReader.exe --TreeName ANNPID/DecayTree --InRootFiles ${ROOTDIR}${FILE} --OutRootFile ${MVADIR}${FILE} --WorkDir ${WORKDIR} --Inputs TMVAInputs-Long.txt 

#export FILE="2012-S20-ANNPID-BHADRONCOMPLETEEVENT.root"
#TMVAReader.exe --TreeName ANNPID/DecayTree --InRootFiles ${ROOTDIR}${FILE} --OutRootFile ${MVADIR}${FILE} --WorkDir ${WORKDIR} --Inputs TMVAInputs-Long.txt

#export FILE="Incb-MC2012-S20-ANNPID-Pythia8.root"
#TMVAReader.exe --TreeName ANNPID/DecayTree --InRootFiles ${ROOTDIR}${FILE} --OutRootFile ${MVADIR}${FILE} --WorkDir ${WORKDIR} --Inputs TMVAInputs-Long.txt


exit 0
