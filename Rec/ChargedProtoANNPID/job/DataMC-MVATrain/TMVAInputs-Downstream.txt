
# Event Size Variables
#NumRich1Hits  ; I ; NumRich1Hits
#NumRich2Hits  ; I ; NumRich2Hits
#NumSPDHits    ; I ; NumSPDHits
#NumLongTracks ; I ; NumLongTracks
#NumMuonTracks ; I ; NumMuonTracks
#NumTTracks    ; I ; NumTTracks
#NumPVs        ; I ; NumPVs

TrackP ; F ; TrackP
TrackPt ; F ; TrackPt
TrackChi2PerDof ; F ; TrackChi2PerDof
TrackNumDof ; I ; TrackNumDof
TrackGhostProb ; F ; TrackGhostProbability
TrackFitTChi2 ; F ; TrackFitTChi2
TrackFitTNDoF ; I ; TrackFitTNDoF
RichUsedR1Gas ; I ; RichUsedR1Gas
RichUsedR2Gas ; I ; RichUsedR2Gas
RichAbovePiThres ; I ; RichAbovePiThres
RichAboveKaThres ; I ; RichAboveKaThres
RichDLLe ; F ; RichDLLe
RichDLLmu ; F ; RichDLLmu
RichDLLk ; F ; RichDLLk
RichDLLp ; F ; RichDLLp
RichDLLbt ; F ; RichDLLbt
MuonBkgLL ; F ; MuonBkgLL
MuonMuLL ; F ; MuonMuLL
MuonIsMuon ; I ; MuonIsMuon
MuonNShared ; I ; MuonNShared
InAccMuon ; I ; InAccMuon
MuonIsLooseMuon ; I ; MuonIsLooseMuon
InAccEcal ; I ; InAccEcal
EcalPIDe ; F ; EcalPIDe
EcalPIDmu ; F ; EcalPIDmu
InAccHcal ; I ; InAccHcal
HcalPIDe ; F ; HcalPIDe
HcalPIDmu ; F ; HcalPIDmu
PrsPIDe ; F ; PrsPIDe
