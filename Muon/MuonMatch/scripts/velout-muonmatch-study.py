#!/usr/bin/env python
###############################################################################
# (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Do studies for the VeloUT-Muon matching algorithm.
Must be run with an python environment that supports matplotlib LateX rendering.
'''

__author__ = ['Miguel Ramos Pernas']
__email__ = ['miguel.ramos.pernas@cern.ch']

# Root is special
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

# Python
import argparse
import logging
import matplotlib
import matplotlib.pyplot as plt
import numpy
import pandas
import root_numpy

from collections import namedtuple, OrderedDict
from itertools import chain
from matplotlib.offsetbox import AnchoredText
from scipy import stats
from scipy.optimize import curve_fit
from uncertainties import ufloat, unumpy

PlotVar = namedtuple('PlotVar', ['name', 'title', 'bins', 'range'])

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['axes.labelsize'] = 12
matplotlib.rcParams['axes.titlesize'] = 14
matplotlib.rcParams['font.size'] = 12
matplotlib.rcParams['text.latex.preamble'] = r'\usepackage{amsmath}'

# Logger instance
logger = logging.getLogger(__name__)


def frame_by_evtnumber_and_mcmatch_key(array):
    '''
    Create a pandas.DataFrame object whose indexes will be created
    from the values of RunNumber, EvtNumber and MCMATCH_KEY in the input array.
    '''
    names = list(array.dtype.names)

    keys = ['RunNumber', 'EvtNumber', 'MCMATCH_KEY']

    for n in keys:
        i = names.index(n)
        if i == len(names) - 1:
            names = names[:i]
        else:
            names = names[:i] + names[i + 1:]

    idx = pandas.MultiIndex.from_arrays([array[k] for k in keys], names=keys)

    return pandas.DataFrame(array[names], index=idx)


def access_hits_sample(input_file, sample_type, **kwargs):
    '''
    Access a hits data sample.
    '''
    logger.info('Accessing hits in "{}:{}/Hits"'.format(
        input_file, sample_type))

    arr = root_numpy.root2array(input_file, '{}/Hits'.format(sample_type),
                                **kwargs)

    return frame_by_evtnumber_and_mcmatch_key(arr)


def access_tracks_sample(input_file, sample_type, **kwargs):
    '''
    Access a tracks data sample.
    '''
    logger.info('Accessing tracks in "{}:{}/Tracks"'.format(
        input_file, sample_type))

    arr = root_numpy.root2array(input_file, '{}/Tracks'.format(sample_type),
                                **kwargs)

    return frame_by_evtnumber_and_mcmatch_key(arr)


def samples_with_common_keys(*samples):
    '''
    Take a set of samples and return subsamples so each one
    contains only keys of MCParticles present in the previous.
    '''
    output = [samples[0]]
    for p, n in zip(samples[:-1], samples[1:]):
        output.append(n.loc[n.index.intersection(p.index)])
    return output


def _fit_magnet_focal_plane(tx, zf):
    '''
    Do a fit to the position of the magnet focal plane.
    '''
    (p0, p2), cov = curve_fit(_zf_parametrization, tx, zf, p0=(5300., -400.))

    (sp0, sp2) = numpy.sqrt(numpy.diag(cov))

    return p0, sp0, p2, sp2


def _make_profile(x, y, xrg, npoints):
    '''
    Do a profile in "x" for the given set of points. The space is
    binned according to "npoints".
    '''
    # Create the profile ("num" defines the number of bins plus one)
    edges = numpy.linspace(*xrg, num=npoints)

    centers = (edges[1:] + edges[:-1]) / 2.
    ex = (edges[1:] - edges[:-1]) / 2.

    dig = numpy.digitize(x, edges)

    fvals = [y[dig == i] for i in range(1, len(edges))]

    profile = numpy.array([numpy.mean(a) for a in fvals], dtype=float)

    # Select only values that are not NaN (mean is NaN if empty container is given)
    not_nan = numpy.logical_not(numpy.isnan(profile))
    centers = centers[not_nan]
    profile = profile[not_nan]

    ex = ex[not_nan]
    ey = numpy.array([stats.sem(a) for a in fvals if numpy.size(a)],
                     dtype=float)

    return centers, profile, ex, ey


def _focal_plane_z(v_x, v_z, v_tx, t_x, t_z, t_tx):
    '''
    Calculate the position of the focal plane in Z, taking the values
    of the parameters of the VELO segment from a VELO-UT track
    and those of the T segment from a forwarded track.
    '''
    return (v_x - t_x - v_tx * v_z + t_tx * t_z) * 1. / (t_tx - v_tx)


def _kick_parametrization(p, offset, scale):
    '''
    Parametrization of the difference in the slope as a function of
    the momentum.
    '''
    return scale / (p - offset)


def _zf_parametrization(tx, a, b):
    '''
    Parametrization of the magnet focal plane as a function of the
    slope at the end of the VELO.
    '''
    return a + b * tx * tx


def kick_parametrization(input_file):
    '''
    Calculate the parametrization of the variation in the slope before
    and after the magnet as a function of the track momentum.
    '''
    # Requirements imposed by reconstruction and muon penetrability
    requirements = 'MCMATCH_P > 3000 && (MCMATCH_PID == -13 || MCMATCH_PID == +13) && MCMATCH_WGT > 0.7'

    tracks = access_tracks_sample(
        input_file, 'ForwardFastTracksTuple', selection=requirements)

    logger.info('Applied the following requirements to the tracks: {}'.format(
        requirements))

    tracks.loc[:, 'DTX'] = numpy.abs(tracks['AtT_TX'] - tracks['EndVelo_TX'])
    tracks.loc[:,
               'ERR_DTX2'] = tracks['AtT_ERR_TX2'] + tracks['EndVelo_ERR_TX2']

    fig = plt.figure(figsize=(8, 5))
    ax = fig.gca()

    vdtx = PlotVar('DTX', r'$\left|\Delta t_x\right|$', 100, (0, 0.5))
    vp = PlotVar('P', r'$p~(\text{MeV}/c)$', 100, (0, 5e4))

    (offset, scale), cov = curve_fit(
        _kick_parametrization, tracks['P'], tracks['DTX'], p0=(175., 1255.))

    (s_offset, s_scale) = numpy.sqrt(numpy.diag(cov))

    logger.info('Coefficients of fit to power-law:\n' +
                ' - offset: {:+.2f} +- {:.2f}\n'.format(offset, s_offset) +
                ' - scale:  {:+.2f} +- {:.2f}'.format(scale, s_scale))

    x = numpy.linspace(s_offset + 1000., vp.range[1], num=101)

    ax.plot(
        tracks['P'], tracks['DTX'], 'k.', markersize=0.3, label=r'_nolegend_')
    ax.plot(
        x,
        _kick_parametrization(x, offset, scale),
        'b-',
        label=r'$\left|\Delta t_x\right| = \frac{{{:.2f}}}{{p - {:.2f}}}$'.
        format(scale, offset))
    ax.set_xlabel(vp.title, fontsize=16)
    ax.set_ylabel(vdtx.title, fontsize=16)
    ax.tick_params(labelsize=15)
    ax.set_xlim(*vp.range)
    ax.set_ylim(*vdtx.range)

    ax.legend(fontsize=16)

    fig.tight_layout()

    plt.show()


def magnet_focal_plane(input_file):
    '''
    Calculate the parametrization of the magnet focal plane as a function
    of the slope at the end of the VELO.
    '''
    # Requirements imposed by reconstruction and muon penetrability
    requirements = 'PT > 80 && P > 3000 && abs(MCMATCH_PID) == 13 && MCMATCH_WGT > 0.7 && abs(MCMATCH_MOTHER_PID) != 211'

    tracks = access_tracks_sample(
        input_file, 'ForwardFastTracksTuple', selection=requirements)

    logger.info('Applied the following requirements to the tracks: {}'.format(
        requirements))

    # Calculate the magnet focal plane in Z
    zf = _focal_plane_z(tracks['EndVelo_X'], tracks['EndVelo_Z'],
                        tracks['EndVelo_TX'], tracks['AtT_X'], tracks['AtT_Z'],
                        tracks['AtT_TX'])

    fig = plt.figure(figsize=(14, 7))
    ax = fig.gca()

    tx = tracks['EndVelo_TX']

    # Range to fit and plot
    tx_range = (-0.3, +0.3)
    zf_range = (5000, 5600)

    # Only consider values in the range of plotting
    tx_cond = numpy.logical_and(tx >= tx_range[0], tx <= tx_range[1])
    zf_cond = numpy.logical_and(zf >= zf_range[0], zf <= zf_range[1])

    cond = numpy.logical_and(tx_cond, zf_cond)

    tx = tx[cond]
    zf = zf[cond]

    # Get the magent focal plane parameters
    p0, sp0, p2, sp2 = _fit_magnet_focal_plane(tx, zf)

    centers, profile, ex, ey = _make_profile(tx, zf, tx_range, 11)

    # Plot "zf" over "tx"
    ax.plot(tx, zf, 'k.', markersize=0.3)
    ax.set_xlabel(r'$t_x$ at end of VELO (from Forward track)', fontsize=16)
    ax.set_ylabel(r'$z_f$ (mm)', fontsize=16)
    ax.tick_params(labelsize=15)
    ax.set_ylim(*zf_range)
    ax.set_xlim(*tx_range)

    ax.errorbar(
        centers,
        profile,
        yerr=ey,
        xerr=ex,
        linestyle='none',
        color='red',
        marker='+',
        capsize=3,
        markersize=0.5)

    logger.info('Coefficients of fit to polynomial:\n' +
                ' - p0: {:+.2f} +- {:.2f}\n'.format(p0, sp0) +
                ' - p2: {:+.2f} +- {:.2f}'.format(p2, sp2))

    x = numpy.linspace(*tx_range, num=101)

    ax.plot(
        x,
        _zf_parametrization(x, p0, p2),
        'b-',
        label=r'$z_f = {:+.2f}{:+.2f} \times t_x^2$'.format(p0, p2))

    ax.legend(fontsize=16)

    # Plot the resolution in the magnet focal plane position
    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(14, 7))

    v_x = unumpy.uarray(tracks['EndVelo_X'],
                        numpy.sqrt(tracks['EndVelo_ERR_X2']))
    v_y = unumpy.uarray(tracks['EndVelo_Y'],
                        numpy.sqrt(tracks['EndVelo_ERR_Y2']))
    v_z = tracks['EndVelo_Z']

    v_tx = unumpy.uarray(tracks['EndVelo_TX'],
                         numpy.sqrt(tracks['EndVelo_ERR_TX2']))
    v_ty = unumpy.uarray(tracks['EndVelo_TY'],
                         numpy.sqrt(tracks['EndVelo_ERR_TY2']))

    t_x = unumpy.uarray(tracks['AtT_X'], numpy.sqrt(tracks['AtT_ERR_X2']))
    t_y = unumpy.uarray(tracks['AtT_Y'], numpy.sqrt(tracks['AtT_ERR_Y2']))
    t_z = tracks['AtT_Z']

    t_tx = unumpy.uarray(tracks['AtT_TX'], numpy.sqrt(tracks['AtT_ERR_TX2']))
    t_ty = unumpy.uarray(tracks['AtT_TY'], numpy.sqrt(tracks['AtT_ERR_TY2']))

    zf = _focal_plane_z(v_x, v_z, v_tx, t_x, t_z, t_tx)
    xf = v_x + v_tx * (zf - v_z)
    yf = v_y + v_ty * (zf - v_z)

    common = {
        'range': (-1200, +1200),
        'bins': 40,
        'histtype': 'step',
        'linewidth': 1.2
    }

    x = unumpy.nominal_values(xf)
    ax0.hist(
        x, label=r'$x_f$ ($\sigma = {:.2f}$)'.format(numpy.std(x)), **common)
    y = unumpy.nominal_values(yf)
    ax0.hist(
        y, label=r'$y_f$ ($\sigma = {:.2f}$)'.format(numpy.std(y)), **common)
    ax0.set_xlabel(r'Position (mm)')
    ax0.set_ylabel(r'Arbitrary units')
    ax0.legend()

    common = {
        'range': (0, 10),
        'bins': 30,
        'histtype': 'step',
        'linewidth': 1.2
    }

    x = unumpy.std_devs(xf)
    ax1.hist(
        x,
        label=r'$s_{{y_f}}$ ($\sigma = {:.2f}$)'.format(numpy.std(x)),
        **common)
    y = unumpy.std_devs(yf)
    ax1.hist(
        y,
        label=r'$s_{{y_f}}$ ($\sigma = {:.2f}$)'.format(numpy.std(y)),
        **common)
    ax1.set_xlabel(r'Uncertainty on position (mm)')
    ax1.set_ylabel(r'Arbitrary units')
    ax1.legend()

    # Show the plots
    plt.show()


def momentum_dependency(input_file):
    '''
    Study the dependence of the magnet focal plane parametrization with the
    momentum.
    '''
    # Requirements imposed by reconstruction and muon penetrability
    requirements = '(MCMATCH_PID == -13 || MCMATCH_PID == +13) && MCMATCH_WGT > 0.7'

    tracks = access_tracks_sample(
        input_file, 'ForwardFastTracksTuple', selection=requirements)

    logger.info('Applied the following requirements to the tracks: {}'.format(
        requirements))

    fig, axs = plt.subplots(2, 2, figsize=(14, 8))

    # Iterate over the momentum ranges defining the different track categories
    params = []
    edges = numpy.array([0., 3e3, 6e3, 10e3, 60e3], dtype=float)
    for ax, (pmin, pmax) in zip(axs.flatten(), zip(edges[:-1], edges[1:])):

        filtered_tracks = tracks[numpy.logical_and(tracks['P'] >= pmin,
                                                   tracks['P'] < pmax)]

        tx = filtered_tracks['EndVelo_TX']
        zf = _focal_plane_z(
            filtered_tracks['EndVelo_X'], filtered_tracks['EndVelo_Z'],
            filtered_tracks['EndVelo_TX'], filtered_tracks['AtT_X'],
            filtered_tracks['AtT_Z'], filtered_tracks['AtT_TX'])

        # Range to fit and plot
        tx_range = (-0.25, +0.25)
        zf_range = (5000, 5600)

        # Only consider values in the range of plotting
        tx_cond = numpy.logical_and(tx >= tx_range[0], tx <= tx_range[1])
        zf_cond = numpy.logical_and(zf >= zf_range[0], zf <= zf_range[1])

        cond = numpy.logical_and(tx_cond, zf_cond)

        tx = tx[cond]
        zf = zf[cond]

        # Get the magent focal plane parameters
        p0, sp0, p2, sp2 = _fit_magnet_focal_plane(tx, zf)

        centers, profile, ex, ey = _make_profile(tx, zf, tx_range, 11)

        params.append((p0, sp0, p2, sp2))

        # Plot "zf" over "tx"
        ax.plot(tx, zf, 'k.', markersize=0.3)
        ax.set_xlabel(r'$t_x$ at end of VELO (from Forward track)')
        ax.set_ylabel(r'$z_f$ (mm)')
        ax.tick_params(labelsize=15)
        ax.set_ylim(*zf_range)
        ax.set_xlim(*tx_range)

        ax.errorbar(
            centers,
            profile,
            yerr=ey,
            xerr=ex,
            linestyle='none',
            color='red',
            marker='+',
            capsize=3,
            markersize=0.5)

        logger.info(
            'Coefficients of fit to polynomial (p >= {} MeV/c && p < {} MeV/c):\n'
            .format(pmin, pmax) +
            ' - p0: {:+.2f} +- {:.2f}\n'.format(p0, sp0) +
            ' - p2: {:+.2f} +- {:.2f}'.format(p2, sp2))

        x = numpy.linspace(*tx_range, num=101)

        ax.plot(
            x,
            _zf_parametrization(x, p0, p2),
            'b-',
            label=r'$z_f = {:+.2f}{:+.2f} \times t_x^2$'.format(p0, p2))

        ax.set_title(
            r'$p \geq {}~\text{{MeV}}/c~\text{{and}}~p < {}~\text{{MeV}}/c$'.
            format(pmin, pmax))
        ax.legend()

    fig.tight_layout()

    # Plot the variation of the parameters as a function of the momentum
    centers = (edges[1:] + edges[:-1]) / 2.

    xerr = (edges[1:] - edges[:-1]) / 2.

    p0, sp0, p2, sp2 = list(zip(*params))

    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(14, 7))
    ax0.errorbar(
        centers, p0, yerr=sp0, xerr=xerr, ls='none', color='b', marker='o')
    ax0.set_ylabel(r'p0 (mm)')
    ax1.errorbar(
        centers, p2, yerr=sp2, xerr=xerr, ls='none', color='b', marker='o')
    ax1.set_ylabel(r'p2 (mm)')

    for ax in (ax0, ax1):
        ax.set_xlabel(r'momentum (MeV/$c$)')

    # Show the plots
    plt.show()


def muon_hits(input_file):
    '''
    Plot the distributions of MC-matched muon hits.
    '''
    requirements = 'abs(MCMATCH_PID) == 13 && MCMATCH_SQUARED_DISTANCE < 1000'

    muonhits = access_hits_sample(
        input_file, 'MuonMatchHitsTuple', selection=requirements)

    edges = numpy.linspace(0., 1e5, 100)

    muonhits.loc[:, 'P_BIN'] = numpy.digitize(muonhits['MCMATCH_P'], edges)

    # We will work per event number and MC-match key, so we must create a new
    # frame removing the duplicated entries by MC-match key
    df = muonhits.index.to_frame(index=False).drop_duplicates('MCMATCH_KEY')

    idx = pandas.MultiIndex.from_arrays(
        [df['RunNumber'], df['EvtNumber'], df['MCMATCH_KEY']],
        names=['RunNumber', 'EvtNumber', 'MCMATCH_KEY'])

    muons = pandas.DataFrame(
        columns=['P', 'Q', 'M2', 'M3', 'M4', 'M5', 'CONSECUTIVE'], index=idx)

    logger.info('Calculating quantities by MC-matched particle key')

    group = muonhits.groupby(['RunNumber', 'EvtNumber', 'MCMATCH_KEY'])
    for i in range(4):
        muons['M{}'.format(i + 2)] = group['STATION'].apply(
            lambda x: numpy.any(x == i))

    # All values in the group are the same for these quantities, and equal
    # to the mean
    muons['Q'] = group['MCMATCH_Q'].apply(lambda x: x.mean())
    muons['P'] = group['MCMATCH_P'].apply(lambda x: x.mean())

    # If there are missing hits in the intermediate muon chambers, the
    # difference between consecutive elements in the sorting array will be
    # greater than zero for at least one index.
    muons['CONSECUTIVE'] = numpy.all(
        numpy.diff(muons[['M2', 'M3', 'M4', 'M5']].values.astype(int), axis=1)
        <= 0,
        axis=1)

    def _plot_by_momentum(muons, title=None):
        '''
        Plot the given muons in four different axes
        '''
        fig, axs = plt.subplots(2, 2, figsize=(8, 5))

        for i, ax in enumerate(axs.flatten()):
            ax.hist(
                muons[muons['M{}'.format(i + 2)]]['P'],
                bins=edges,
                histtype='step')
            ax.set_xlabel(r'Momentum $(\text{MeV}/c)$')
            ax.set_ylabel(r'Arbitrary units')
            ax.set_title(r'Muons with hits in M{}'.format(i + 2))
            ax.set_yscale('log', nonposy='clip')

        if title is not None:
            fig.suptitle(title)
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        else:
            fig.tight_layout()

    logger.info('Making plots by momentum')

    _plot_by_momentum(muons)
    _plot_by_momentum(
        muons[muons['CONSECUTIVE']], title='Muons with consecutive hits')
    _plot_by_momentum(
        muons[~muons['CONSECUTIVE']], title='Muons with no consecutive hits')

    logger.info('Making correlation plot')

    fig = plt.figure(figsize=(8, 5))
    ax = fig.gca()

    labels = ['M2', 'M3', 'M4', 'M5']

    corr = pandas.DataFrame(index=labels[1:], columns=labels[:-1])

    corr.loc[:, :] = 0.

    for i, curr in enumerate(labels[1:]):

        fm = muons[muons[curr] == True]

        for prev in labels[:i + 1]:

            c = numpy.count_nonzero(fm[prev]) / float(len(fm))

            corr.loc[curr, prev] = c

    im = ax.matshow(corr, vmin=0, vmax=+1, cmap='Purples')

    ax.set_xticks(numpy.arange(3))
    ax.set_xticklabels(labels[:-1])

    ax.set_yticks(numpy.arange(3))
    ax.set_yticklabels(labels[1:])

    ax.set_title(r'Probability of having hits in previous muon chambers')

    fig.colorbar(im)

    fig.tight_layout()

    plt.show()


def muon_occupancy(input_file):
    '''
    Plot distributions about the occupancy of the muon chambers.
    '''
    requirements = 'abs(MCMATCH_PID) == 13 && MCMATCH_SQUARED_DISTANCE < 1000 && abs(MCMATCH_MOTHER_PID) != 211'

    muonhits = access_hits_sample(
        input_file, 'MuonMatchHitsTuple', selection=requirements)

    # Draw variables per crossing category and for each muon chamber overlayed
    categories = OrderedDict([
        ('all', numpy.ones(len(muonhits), dtype=numpy.bool)),
        ('crossed', muonhits['UNCROSSED'] == False),
        ('uncrossed', muonhits['UNCROSSED'] == True),
    ])

    variables = [
        PlotVar('X', r'$x~(\text{mm})$', 100, (-6e3, 6e3)),
        PlotVar('Y', r'$y~(\text{mm})$', 100, (-6e3, 6e3)),
        PlotVar('Z', r'$z~(\text{mm})$', 100, (15e3, 20e3)),
        PlotVar('ERR_X2', r'$\sigma_x^2~(\text{mm}^2)$', 100, (0, 1.2e3)),
        PlotVar('ERR_Y2', r'$\sigma_y^2~(\text{mm}^2)$', 100, (0, 1.2e3)),
        PlotVar('ERR_Z2', r'$\sigma_z^2~(\text{mm}^2)$', 100, (1120, 1160)),
    ]

    for name, cuts in categories.items():

        logger.info('Making plots for category "{}"'.format(name))

        fig, axs = plt.subplots(2, 3, figsize=(14, 8))

        smp = muonhits[cuts]
        for ax, v in zip(axs.flatten(), variables):
            for i in range(4):
                values = smp[smp['STATION'] == i][v.name]
                ax.hist(
                    values,
                    bins=v.bins,
                    range=v.range,
                    histtype='step',
                    label='M{}'.format(i + 2))

            ax.set_xlabel(v.title)
            ax.set_ylabel(r'Entries')
            ax.legend()

        fig.suptitle(r'Muon hit information for category "{}"'.format(name))

        fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    # Plot the occupancies of the muon chambers
    group = muonhits.groupby(['RunNumber', 'EvtNumber', 'STATION'])

    logger.info('Calculating occupancies')

    occupancies = group['STATION'].count()

    means = occupancies.groupby('STATION').mean()

    fig = plt.figure()
    ax = fig.gca()

    centers = numpy.arange(4)

    ax.bar(centers, means, width=0.5)
    ax.set_title(r'Occupancies per muon chamber')
    ax.set_xlabel(r'Muon chamber')
    ax.set_ylabel(r'Average occupancy')
    ax.tick_params(axis='x', which='minor', bottom=False)
    ax.set_xticks(centers)
    ax.set_xticklabels(['M{}'.format(i + 2) for i in range(4)])

    fig.tight_layout()

    # Show the figures
    plt.show()


def performance(input_file):
    '''
    Study the performance of the tracks in the given input file.
    '''
    requirements = 'PT > 80 && P > 2500'

    logger.info('The following cuts will be applied to the tracks: "{}"'.
                format(requirements))

    raw_upstream = access_tracks_sample(
        input_file, 'UpstreamTracksTuple', selection=requirements)
    raw_muonmatch = access_tracks_sample(
        input_file, 'MuonMatchVeloUTTracksTuple', selection=requirements)
    raw_forward = access_tracks_sample(
        input_file, 'ForwardFastTracksTuple', selection=requirements)

    # PDG convention from 2018 for quark IDs
    b_quark = 5
    c_quark = 4
    s_quark = 3

    def with_mother_quark(qid):
        def func(muons):
            n = numpy.count_nonzero(muons['MCMATCH_MOTHER_PID'].apply(
                lambda c: str(qid) in str(c)[-4:-1])
                                    )  # Check in the quark content
            return n, float(n) / len(muons)

        return func

    def is_ghost(df):
        return df.eval('MCMATCH_WGT < 0.7')

    def is_muon(df):
        return df.eval(
            'MCMATCH_WGT > 0.7 and (MCMATCH_PID == -13 or MCMATCH_PID == +13)')

    def wrong_sign(df):
        return df.eval('MCMATCH_Q != Q')

    for use_common_keys, title in ((True, 'Muons only from previous step'),
                                   (False, 'General muons')):

        if use_common_keys:
            upstream, muonmatch, forward = samples_with_common_keys(
                raw_upstream, raw_muonmatch, raw_forward)
        else:
            upstream, muonmatch, forward = raw_upstream, raw_muonmatch, raw_forward

        upstream_muons = upstream[is_muon(upstream)]
        muonmatch_muons = muonmatch[is_muon(muonmatch)]
        forward_muons = forward[is_muon(forward)]

        # Must be in processing order
        track_types = (('Upstream', upstream, upstream_muons),
                       ('MuonMatchVeloUT', muonmatch,
                        muonmatch_muons), ('Forward', forward, forward_muons))

        # Plot the number of tracks, muons ghosts and purity
        fig, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=(16, 6))
        fig.suptitle(title, fontsize=16)

        ntotal = numpy.array([len(whole) for (_, whole, _) in track_types],
                             dtype=int)
        nmuons = numpy.array([len(muons) for (_, _, muons) in track_types],
                             dtype=int)
        nghosts = numpy.array([
            numpy.count_nonzero(is_ghost(whole))
            for (_, whole, _) in track_types
        ],
                              dtype=int)
        nwrongq = numpy.array([
            numpy.count_nonzero(wrong_sign(muons))
            for (_, _, muons) in track_types
        ],
                              dtype=int)
        purity = nmuons.astype(float) / ntotal

        x = numpy.arange(len(track_types))

        for i, (name, color, values) in enumerate(
            [(r'total', 'royalblue', ntotal), (r'muons', 'seagreen', nmuons),
             (r'ghosts', 'tomato', nghosts),
             (r'wrong sign muons', 'orange', nwrongq)]):

            pos = x - 0.3 + i * 0.2

            ax0.bar(
                pos,
                values,
                width=0.2,
                color=color,
                align='center',
                label=name)

            for p, v in zip(pos, values):
                ax0.text(
                    p,
                    v,
                    v,
                    color='k',
                    fontweight='bold',
                    horizontalalignment='center',
                    verticalalignment='top',
                    rotation='vertical')

        ax0.set_xticks(x)
        ax0.set_xticklabels([name for name, _, _ in track_types])
        ax0.set_yscale('log', nonposy='clip')
        ax0.set_ylim(top=10 * ntotal.max())
        ax0.tick_params(axis='x', which='minor', bottom=False)
        ax0.set_ylabel(r'Number of tracks')
        ax0.legend()

        ax1.bar(x, purity, width=0.5, color='royalblue', align='center')
        ax1.set_xticks(x)
        ax1.set_xticklabels([name for name, _, _ in track_types])
        ax1.tick_params(axis='x', which='minor', bottom=False)
        ax1.set_ylabel(r'Purity')

        # Annotate the efficiency
        effs = r''
        ms = list(zip(*track_types))
        names, muons = ms[0], ms[2]
        for prev, name, next in zip(muons[:-1], names[1:], muons[1:]):

            num = ufloat(len(next), numpy.sqrt(len(next)))
            den = ufloat(len(prev), numpy.sqrt(len(prev)))

            effs += r'\item {} efficiency: ${}$'.format(
                name, (num / den).format('.2L'))

        text = r'\begin{itemize}\setlength\itemsep{4pt}' + effs + r'\end{itemize}'

        ax1.add_artist(AnchoredText(text, loc='upper left'))

        # Calculate the provenance of the muons in the samples
        from_b = [
            with_mother_quark(b_quark)(muons) for _, _, muons in track_types
        ]
        from_c = [
            with_mother_quark(c_quark)(muons) for _, _, muons in track_types
        ]
        from_s = [
            with_mother_quark(s_quark)(muons) for _, _, muons in track_types
        ]

        for i, (name, color, values) in enumerate(
            [(r'from \textit{b}', 'royalblue', from_b),
             (r'from \textit{c}', 'tomato', from_c),
             (r'from \textit{s}', 'seagreen', from_s)]):

            pos = x - 0.25 + i * 0.25

            numbers, fractions = list(zip(*values))

            ax2.bar(
                pos,
                fractions,
                width=0.25,
                color=color,
                align='center',
                label=name)

            for p, f, v in zip(pos, fractions, numbers):
                ax2.text(
                    p,
                    f,
                    v,
                    color='k',
                    fontweight='bold',
                    horizontalalignment='center',
                    verticalalignment='top',
                    rotation='vertical')

        ax2.set_xticks(x)
        ax2.set_xticklabels([name for name, _, _ in track_types])
        ax2.tick_params(axis='x', which='minor', bottom=False)
        ax2.set_ylabel(r'Fraction of muons')
        ax2.legend()

        fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    plt.show()


def resolutions(input_file):
    '''
    Plot the resolution of the Velo-UT tracks with respect to the
    true MC particle.
    '''
    categories = OrderedDict(
        nocuts=None,
        min_pt='PT > 80',
        muons=
        'PT > 80 and P > 3000 and (MCMATCH_PID == -13 or MCMATCH_PID == +13) and MCMATCH_WGT > 0.7',
        detached_muons=
        'PT > 80 and P > 3000 and (MCMATCH_PID == -13 or MCMATCH_PID == +13) and MCMATCH_WGT > 0.7 and IP > 1',
        prompt_muons=
        'PT > 80 and P > 3000 and (MCMATCH_PID == -13 or MCMATCH_PID == +13) and MCMATCH_WGT > 0.7 and IP < 1',
    )

    upstream = access_tracks_sample(input_file, 'UpstreamTracksTuple')

    variables = [
        PlotVar('P', r'$p~(\text{MeV}/c)$', 100, (0, 4e4)),
        PlotVar('PT', r'$p_t~(\text{MeV}/c)$', 100, (0, 2.5e3)),
    ]

    for cuts in categories.values():

        if cuts is not None:
            sample = upstream[upstream.eval(cuts)]
        else:
            sample = upstream

        fig, axs = plt.subplots(2, 2, figsize=(14, 8))

        fig.suptitle(r'Cuts on tracks: \verb|{}|'.format(cuts), fontsize=16)

        for v, ax in zip(variables, axs):

            ax0, ax1 = ax

            # Calculate the resolution
            true = sample['MCMATCH_' + v.name]
            reco = sample[v.name]
            res = (reco - true) / true

            # Plot the resolution
            rr = (-2., +3.)
            ax0.hist(
                res,
                bins=v.bins,
                range=rr,
                histtype='step',
                color='blue',
                linewidth=1.5)
            ax0.set_ylabel(r'Entries')
            ax0.set_xlabel(v.title)

            # Plot the resolution versus the momentum
            ax1.hist2d(
                true,
                res,
                bins=100,
                range=(v.range, rr),
                cmap='Blues',
                norm=matplotlib.colors.LogNorm())
            ax1.set_xlabel(r'True ' + v.title)
            ax1.set_ylabel(r'Resolution in ' + v.name)

        fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    plt.show()


def set_qoverp_effect(default, setqop, no_decays_in_flight):
    '''
    Make plots to illustrate the difference between asking the algorithm to
    set the value of q/p or not.
    '''
    requirements = 'abs(MCMATCH_PID) == 13 && MCMATCH_PID > 0.7'

    if no_decays_in_flight:
        logger.info('Will remove muons coming from pions')
        requirements += ' && abs(MCMATCH_MOTHER_PID) != 211'

    muonmatch_default = access_tracks_sample(
        default, 'MuonMatchVeloUTTracksTuple', selection=requirements)
    muonmatch_setqop = access_tracks_sample(
        setqop, 'MuonMatchVeloUTTracksTuple', selection=requirements)
    forward_default = access_tracks_sample(
        default, 'ForwardFastTracksTuple', selection=requirements)
    forward_setqop = access_tracks_sample(
        setqop, 'ForwardFastTracksTuple', selection=requirements)

    muonmatch_default, forward_default = samples_with_common_keys(
        muonmatch_default, forward_default)
    muonmatch_setqop, forward_setqop = samples_with_common_keys(
        muonmatch_setqop, forward_setqop)

    fig, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=(16, 6))

    common = {
        'range': (0, 5e4),
        'bins': 40,
        'density': True,
        'histtype': 'step'
    }

    ax0.hist(muonmatch_default['P'], label=r'Default', **common)
    ax0.hist(muonmatch_setqop['P'], label=r'Setting $q/p$', **common)
    ax0.set_xlabel(r'$p~\text{MeV}/c$')
    ax0.set_ylabel(r'Arbitrary units')
    ax0.set_title(r'MuonMatchVeloUT')
    ax0.legend()

    res_muonmatch_setqop = (
        muonmatch_setqop['P'] -
        muonmatch_setqop['MCMATCH_P']) / muonmatch_setqop['MCMATCH_P']
    res_muonmatch_default = (
        muonmatch_default['P'] -
        muonmatch_default['MCMATCH_P']) / muonmatch_default['MCMATCH_P']
    res_forward_setqop = (forward_setqop['P'] - forward_setqop['MCMATCH_P']
                          ) / forward_setqop['MCMATCH_P']
    res_forward_default = (forward_default['P'] - forward_default['MCMATCH_P']
                           ) / forward_default['MCMATCH_P']

    common = {
        'range': (-1., +1.),
        'bins': 80,
        'density': True,
        'histtype': 'step'
    }

    logger.info(
        'Standard deviation is only considered for the resolution in the range {}'
        .format(common['range']))

    res_muonmatch_default = res_muonmatch_default[numpy.logical_and(
        res_muonmatch_default >= common['range'][0],
        res_muonmatch_default <= common['range'][1])]
    res_muonmatch_setqop = res_muonmatch_setqop[numpy.logical_and(
        res_muonmatch_setqop >= common['range'][0],
        res_muonmatch_setqop <= common['range'][1])]
    res_forward_default = res_forward_default[numpy.logical_and(
        res_forward_default >= common['range'][0],
        res_forward_default <= common['range'][1])]
    res_forward_setqop = res_forward_setqop[numpy.logical_and(
        res_forward_setqop >= common['range'][0],
        res_forward_setqop <= common['range'][1])]

    ax1.hist(res_muonmatch_default, label=r'Default', **common)
    ax1.hist(res_muonmatch_setqop, label=r'Setting $q/p$', **common)
    ax1.set_xlabel(r'$\delta p/p$')
    ax1.set_ylabel(r'Arbitrary units')
    ax1.set_title(r'MuonMatchVeloUT')
    ax1.add_artist(
        AnchoredText(
            r'\begin{itemize}' +
            r'\item$\sigma = {:.2f}$ (no $q/p$)\item$\sigma = {:.2f}$ (setting $q/p$)'
            .format(
                numpy.std(res_muonmatch_default),
                numpy.std(res_muonmatch_setqop)) + r'\end{itemize}',
            loc='upper left'))
    ax1.legend()

    ax2.hist(res_forward_default, label=r'Default', **common)
    ax2.hist(res_forward_setqop, label=r'Setting $q/p$', **common)
    ax2.set_xlabel(r'$\delta p/p$')
    ax2.set_ylabel(r'Arbitrary units')
    ax2.set_title(r'ForwardFast')
    ax2.add_artist(
        AnchoredText(
            r'\begin{itemize}' +
            r'\item$\sigma = {:.2f}$ (no $q/p$)\item$\sigma = {:.2f}$ (setting $q/p$)'
            .format(
                numpy.std(res_forward_default),
                numpy.std(res_forward_setqop)) + r'\end{itemize}',
            loc='upper left'))
    ax2.legend()

    fig.tight_layout()

    plt.show()


def station_windows(input_file):
    '''
    Study the distribution of hits in each of the muon stations for each muon.
    '''
    requirements = 'abs(MCMATCH_PID) == 13 && MCMATCH_SQUARED_DISTANCE < 1000'

    muonhits = access_hits_sample(
        input_file, 'MuonMatchHitsTuple', selection=requirements)

    requirements = 'abs(MCMATCH_PID) == 13 && MCMATCH_WGT > 0.7 && PT > 80 && P > 2500'

    forward = access_tracks_sample(
        input_file, 'ForwardFastTracksTuple', selection=requirements)

    logger.info('Getting only tracks with muon hits')

    valid = forward.index.intersection(muonhits.index)

    logger.info('The {:.2f}% of the muon tracks have muon hits'.format(
        len(valid) * 100. / len(forward)))

    forward = forward.loc[valid]
    muonhits = muonhits.loc[valid]

    logger.info('Extrapolating tracks to muon chambers')

    distances = pandas.DataFrame(
        columns=[
            'M{}_{}'.format(i + 2, p) for i in range(4) for p in ('X', 'Y')
        ],
        index=valid)

    for i in range(4):

        m = muonhits[muonhits['STATION'] == i]

        dz = m['Z'] - forward['AtT_Z']

        # Here we average per muon chamber and for MC particle
        dx = (forward['AtT_X'] + dz * forward['AtT_TX'] - m['X']).groupby(
            ['RunNumber', 'EvtNumber', 'MCMATCH_KEY']).mean()
        dy = (forward['AtT_Y'] + dz * forward['AtT_TY'] - m['Y']).groupby(
            ['RunNumber', 'EvtNumber', 'MCMATCH_KEY']).mean()

        distances.loc[:, 'M{}_X'.format(i + 2)] = dx
        distances.loc[:, 'M{}_Y'.format(i + 2)] = dy

    # Create the plots per muon chamber
    logger.info('Making plots')

    fig, axs = plt.subplots(2, 2, figsize=(15, 9))
    axs = axs.flatten()

    fig.suptitle(r'Distances from extrapolation to true position', fontsize=16)

    for i, ax in enumerate(axs):

        ax.set_title(r'M{}'.format(i + 2), fontsize=14)

        for p in ('X', 'Y'):

            ax.hist(
                distances['M{}_{}'.format(i + 2, p)].dropna(),
                bins=100,
                range=(-1000, 1000),
                histtype='step',
                linewidth=1.2,
                label=r'$\delta {}$'.format(p.lower()))

        ax.set_xlabel(r'$\delta$ (mm)')
        ax.set_ylabel(r'Arbitrary units')
        ax.set_title(r'M{}'.format(i + 2))
        ax.legend()

    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    plt.show()


def track_variables(input_file):
    '''
    Plot the variables of interest for the tracks and different states.
    '''
    forward = access_tracks_sample(input_file, 'ForwardFastTracksTuple')
    upstream = access_tracks_sample(input_file, 'UpstreamTracksTuple')

    for smp in (forward, upstream):
        smp['WRONG_Q'] = (smp['Q'] != smp['MCMATCH_Q'])

    pvar = PlotVar('P', r'$p (\text{MeV}/c)$', 100, (0, 4e4))
    ptvar = PlotVar('PT', r'$p_T (\text{MeV}/c)$', 100, (0, 2.5e3))

    variables = [
        pvar,
        ptvar,
        PlotVar('ETA', r'$\eta$', 100, (1., 6.)),
        PlotVar('IP', r'Minimum IP (mm)', 100, (0, 0.5)),
        PlotVar('PHI', r'$\phi$ (rad)', 100, (0., numpy.pi)),
        PlotVar('WRONG_Q', r'Wrong charge', 2, (0, 2)),
    ]

    fig, axs = plt.subplots(2, 3, figsize=(16, 8))
    for v, ax in zip(variables, axs.flatten()):
        for (name, smp) in (('forward', forward), ('upstream', upstream)):
            ax.hist(
                smp[v.name],
                bins=v.bins,
                range=v.range,
                histtype='step',
                density=True,
                label=name)
            ax.set_xlabel(v.title)
            ax.set_ylabel(r'Arbitrary units')
            ax.legend()

    fig.suptitle(r'Track parameters', fontsize=16)

    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    # Plot the P and PT for tracks with wrong assigned charge
    left, width = 0.10, 0.5
    bottom, height = 0.10, 0.5
    upper = bottom + height + 0.05
    right = left + width + 0.05
    upper_height = 1. - 0.07 - upper
    right_width = 1. - 0.05 - right

    for (name, smp) in (('Forward', forward), ('Upstream', upstream)):

        smp = smp[smp['WRONG_Q']]

        fig = plt.figure(figsize=(12, 8))

        scatter = fig.add_axes([left, bottom, width, height])
        xaxes = fig.add_axes([left, upper, width, upper_height])
        yaxes = fig.add_axes([right, bottom, right_width, height])

        scatter.hist2d(
            smp['P'],
            smp['PT'],
            range=(pvar.range, ptvar.range),
            bins=(pvar.bins, ptvar.bins),
            norm=matplotlib.colors.LogNorm())
        scatter.set_xlabel(r'$p~(\text{MeV}/c)$')
        scatter.set_ylabel(r'$p_T~(\text{MeV}/c)$')
        scatter.set_xlim(pvar.range)
        scatter.set_ylim(ptvar.range)

        xaxes.hist(smp['P'], range=pvar.range, bins=pvar.bins)
        xaxes.set_ylabel(r'Number of tracks')
        xaxes.set_xlim(scatter.get_xlim())
        xaxes.set_xticklabels([])

        yaxes.hist(
            smp['PT'],
            range=ptvar.range,
            bins=ptvar.bins,
            orientation='horizontal')
        yaxes.set_xlabel(r'Number of tracks')
        yaxes.set_ylim(scatter.get_ylim())
        yaxes.set_yticklabels([])

        fig.suptitle(r'{} wrong-sign tracks'.format(name), fontsize=16)

    plt.show()


if __name__ == '__main__':

    logging.basicConfig(
        format='%(levelname)s: %(message)s', level=logging.INFO)

    parser = argparse.ArgumentParser(description=__doc__)
    subparsers = parser.add_subparsers(help='Mode to run')

    mo = subparsers.add_parser('muon-occupancy', help=muon_occupancy.__doc__)
    mo.set_defaults(function=muon_occupancy)

    mp = subparsers.add_parser('muon-hits', help=muon_hits.__doc__)
    mp.set_defaults(function=muon_hits)

    rp = subparsers.add_parser('resolutions', help=resolutions.__doc__)
    rp.set_defaults(function=resolutions)

    tp = subparsers.add_parser('track-variables', help=track_variables.__doc__)
    tp.set_defaults(function=track_variables)

    kp = subparsers.add_parser(
        'kick-parametrization', help=kick_parametrization.__doc__)
    kp.set_defaults(function=kick_parametrization)

    zp = subparsers.add_parser(
        'magnet-focal-plane', help=magnet_focal_plane.__doc__)
    zp.set_defaults(function=magnet_focal_plane)

    pp = subparsers.add_parser(
        'momentum-dependency', help=momentum_dependency.__doc__)
    pp.set_defaults(function=momentum_dependency)

    ep = subparsers.add_parser('performance', help=performance.__doc__)
    ep.set_defaults(function=performance)

    sp = subparsers.add_parser('station-windows', help=station_windows.__doc__)
    sp.set_defaults(function=station_windows)

    for p in subparsers.choices.values():
        p.add_argument(
            'input_file',
            type=str,
            default='muonmatch-ntuple.root',
            help='Input file to process')

    op = subparsers.add_parser(
        'set-qoverp-effect', help=set_qoverp_effect.__doc__)
    op.set_defaults(function=set_qoverp_effect)

    op.add_argument(
        '--no-decays-in-flight',
        action='store_true',
        help=
        'Whether to do not show the results with muons from decays in flight')
    op.add_argument('default', type=str,
                    help='File storing tracks comming from the '\
                        'MuonMatchVeloUT algorithm.')
    op.add_argument('setqop', type=str,
                    help='File storing tracks where the '\
                        'MuonMatchVeloUT algorithm ran setting the '\
                        'value of q/p.')

    args = parser.parse_args()

    opts = vars(args)

    function = opts.pop('function')

    function(**opts)
