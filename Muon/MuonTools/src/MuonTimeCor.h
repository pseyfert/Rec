/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONTIMECOR_H
#define MUONTIMECOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "MuonDAQ/IMuonRawBuffer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonInterfaces/IMuonFastHWTool.h"
#include "MuonInterfaces/IMuonTimeCor.h" // Interface

/** @class MuonTimeCor MuonTimeCor.h
 *
 *
 *  @author Alessia Satta
 *  @date   2009-12-22
 */
class MuonTimeCor final : public GaudiTool, virtual public IMuonTimeCor {
public:
  /// Standard constructor
  MuonTimeCor( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;
  StatusCode getCorrection( LHCb::MuonTileID tile, int& cor ) override;
  StatusCode getOutCorrection( LHCb::MuonTileID tile, int& cor ) override;
  StatusCode setOutCorrection( LHCb::MuonTileID tile, int cor ) override;
  StatusCode writeOutCorrection() override;
  StatusCode writeCorrection() override;

private:
  int  getIndex( LHCb::MuonTileID tile ) const;
  int  getLayer( LHCb::MuonTileID tile ) const;
  void setCorrection( LHCb::MuonTileID tile, int cor );

  Gaudi::Property<std::vector<std::string>> m_correctionFiles{this, "TimeCorrectionFiles"};
  Gaudi::Property<std::string>              m_correctionFileOut{this, "OutCorrectionFile"};
  Gaudi::Property<std::vector<int>>         m_correctionSign{this, "SignCorrection"};

  std::vector<int> m_correction[20][2];
  std::vector<int> m_Outcorrection[20][2];

  DeMuonDetector*  m_muonDetector = nullptr;
  IMuonRawBuffer*  m_muonBuffer   = nullptr;
  IMuonFastHWTool* m_muonHWTool   = nullptr;
};
#endif // MUONTIMECOR_H
