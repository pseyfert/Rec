/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IGETARRIVAL_H
#define IGETARRIVAL_H 1

// Include files
// from STL
#include <vector>

// from Gaudi
#include "Event/Track.h"
#include "GaudiKernel/IAlgTool.h"

/** @class IGetArrival ICLTool.h
 *
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2008-07-02
 */
struct IGetArrival : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( IGetArrival, 1, 0 );

  virtual StatusCode getArrivalFromTrack( const LHCb::Track& mutrack, double& parr )             = 0;
  virtual StatusCode clArrival( const LHCb::Track& muTrack, double& clarr )                      = 0;
  virtual StatusCode clArrival( const double p, const std::vector<int>& type_st, double& clarr ) = 0;
};
#endif // IGETARRIVAL_H
