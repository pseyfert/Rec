/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <tuple>
/** @brief Get the index/position in a tuple of a specified type
 *
 *  index_of_v<B, std::tuple<A, B, C>> yields 1 etc.
 *
 *  Implementation based on
 *  https://stackoverflow.com/questions/18063451/get-index-of-a-tuple-elements-type
 */
template <typename T, typename Tuple>
struct index_of;

template <typename... Ts>
inline constexpr std::size_t index_of_v = index_of<Ts...>::value;

template <typename T, typename... Ts>
struct index_of<T, std::tuple<T, Ts...>> {
  static constexpr std::size_t value{0};
};

template <typename T, typename U, typename... Ts>
struct index_of<T, std::tuple<U, Ts...>> {
  static constexpr std::size_t value{1 + index_of_v<T, std::tuple<Ts...>>};
};