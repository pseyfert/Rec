###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""@package Functors
Pure-python classes used as building blocks of functor expressions.
"""
from Functors.common import header_prefix, top_level_namespace
from itertools import chain


def bind_if_needed(functor):
    """Return a BoundFunctor constructed from the argument.

    If the argument is an arithmetic constant, do this by hand. Otherwise let
    the argument try and bind itself. This will result in an error if you try
    and pass an unbound functor that needs arguments.
    """
    if type(functor) == str:
        return functor
    if type(functor) in [int, float]:
        code, headers = python_to_cpp_str(functor)
        assert len(headers) == 0
        return BoundFunctor(code=code, nice_code=str(functor))
    else:
        return functor()


def python_to_cpp_str(obj):
    if type(obj) == int:
        return repr(obj), []
    elif type(obj) == float:
        # map python float onto C++ float, not double
        return repr(obj) + 'f', []
    elif type(obj) == str:
        # map python strings explicitly to std::string
        return 'std::string{"' + obj.replace('"', '\\"') + '"}', ['<string>']
    elif type(obj) == dict:
        items = []
        headers = []
        for key, val in obj.items():
            key_code, key_headers = python_to_cpp_str(key)
            val_code, val_headers = python_to_cpp_str(val)
            items.append('{ ' + key_code + ', ' + val_code + ' }')
            headers += key_headers + val_headers
        return '{' + ', '.join(items) + '}', list(set(headers))
    elif (type(obj) == list or type(obj) == tuple):
        items, headers = zip(*[python_to_cpp_str(item) for item in obj])
        return 'std::tuple{' + ', '.join(items) + '}', list(
            set(chain(*headers)).union({'<tuple>'}))
    elif isinstance(obj, BoundFunctor):
        return obj.code(), obj.headers()
    else:
        # one last thing to try: if the argument is an unbound functor, but it
        # doesn't take any arguments, then we can convert it to a bound functor
        try:
            bound = obj()
            if isinstance(bound, BoundFunctor):
                return bound.code(), bound.headers()
        except:
            pass
        raise TypeError("Unsupported type: " + str(type(obj)))


class FunctorBase(object):
    """Base class for all Python-side functors.

    This implements the various operator overloads.
    """

    def __call__(self):
        """Dummy implementation to keep pylint happy"""
        raise Exception("FunctorBase.__call__ should not be called!")

    def __invert__(self):
        """~FUNCTOR, negation"""
        return InvertedBoundFunctor(self())

    def __and__(self, other):
        """F1 & F2, logical and"""
        # bind_if_needed not used because we don't need to support applying the
        # operator to a functor and an arithmetic constant.
        return ComposedBoundFunctor('&', self(), other())

    def __or__(self, other):
        """F1 | F2, logical or"""
        # bind_if_needed not used because we don't need to support applying the
        # operator to a functor and an arithmetic constant.
        return ComposedBoundFunctor('|', self(), other())

    def __lt__(self, other):
        """F1 < F2"""
        return ComposedBoundFunctor('<', self(), bind_if_needed(other))

    def __gt__(self, other):
        """F1 > F2"""
        return ComposedBoundFunctor('>', self(), bind_if_needed(other))

    def __le__(self, other):
        """F1 <= F2"""
        return ComposedBoundFunctor('<=', self(), bind_if_needed(other))

    def __ge__(self, other):
        """F1 >= F2"""
        return ComposedBoundFunctor('>=', self(), bind_if_needed(other))

    def __eq__(self, other):
        """F1 == F2"""
        return ComposedBoundFunctor('==', self(), bind_if_needed(other))

    def __ne__(self, other):
        """F1 != F2"""
        return ComposedBoundFunctor('!=', self(), bind_if_needed(other))

    def __add__(self, other):
        """F1 + F2"""
        return ComposedBoundFunctor('+', self(), bind_if_needed(other))

    def __radd__(self, other):
        """F1 + F2"""
        return ComposedBoundFunctor('+', bind_if_needed(other), self())

    def __sub__(self, other):
        """F1 - F2"""
        return ComposedBoundFunctor('-', self(), bind_if_needed(other))

    def __rsub__(self, other):
        """F1 - F2"""
        return ComposedBoundFunctor('-', bind_if_needed(other), self())

    def __mul__(self, other):
        """F1 * F2"""
        return ComposedBoundFunctor('*', self(), bind_if_needed(other))

    def __rmul__(self, other):
        """F1 * F2"""
        return ComposedBoundFunctor('*', bind_if_needed(other), self())

    def __div__(self, other):
        """F1 / F2, python 2"""
        return ComposedBoundFunctor('/', self(), bind_if_needed(other))

    def __rdiv__(self, other):
        """F1 / F2, python 2"""
        return ComposedBoundFunctor('/', bind_if_needed(other), self())

    def __truediv__(self, other):
        """F1 / F2, python 3"""
        return ComposedBoundFunctor('/', self(), bind_if_needed(other))

    def __rtruediv__(self, other):
        """F1 / F2, python 3"""
        return ComposedBoundFunctor('/', bind_if_needed(other), self())

    def __pow__(self, other):
        """F1 ** F2"""
        return WrappedBoundFunctor(top_level_namespace + 'math::pow',
                                   header_prefix + 'Function.h', self, other)

    def __rpow__(self, other):
        """F1 ** F2"""
        return WrappedBoundFunctor(top_level_namespace + 'math::pow',
                                   header_prefix + 'Function.h', other, self)


class BoundFunctor(FunctorBase, type(
        'Configurable', (),
    {})):  #hack to ensure that the BoundFunctor is parsed correctly
    """A functor that has had all its parameters fully specified."""

    def __init__(self, code=None, nice_code=None, headers=[]):
        self._strrep = nice_code
        self._code = code
        self._headers = headers

    def code(self):
        return self._code

    def code_repr(self):
        return self._strrep

    def headers(self):
        assert not any([len(h) == 0 for h in self._headers])
        return self._headers

    def __str__(self):
        return str((self.code(), self.headers(), self.code_repr()))

    def __repr__(self):
        import json
        return json.dumps(str(self))
        # return "\"{}\"".format(self.code_repr())  # TODO maybe only nice?

    def to_json(self):
        return str(self)

    def __call__(self):
        """Return ourself, for compatibility with a 0-argument Functor.

        The function call operator is used to bake in parameter values. This
        has already been done for a BoundFunctor, so this is trivial.
        """
        return self


class ComposedBoundFunctor(BoundFunctor):
    """A (bound) functor made from two other (bound) functors.

    This is the return type of the various binary operators.
    """

    def __init__(self, operator, func1, func2):
        assert isinstance(func1, BoundFunctor)
        assert isinstance(func2, BoundFunctor)

        code = '( {f1} {op} {f2} )'.format(
            f1=func1.code(), f2=func2.code(), op=operator)
        nice_code = '( {f1} {op} {f2} )'.format(
            f1=func1.code_repr(), f2=func2.code_repr(), op=operator)

        BoundFunctor.__init__(
            self,
            code=code,
            nice_code=nice_code,
            headers=list(set(func1.headers() + func2.headers())))


class InvertedBoundFunctor(BoundFunctor):
    """The logical negation of another (bound) functor.

    This is the return type of the unary negation operator.
    """

    def __init__(self, bound_functor):
        assert isinstance(bound_functor, BoundFunctor)
        BoundFunctor.__init__(
            self,
            code='~( {f1} )'.format(f1=bound_functor.code()),
            nice_code='~{f1}'.format(f1=bound_functor.code_repr()),
            headers=bound_functor.headers())


class WrappedBoundFunctor(BoundFunctor):
    """A (bound) functor that's fed through a transforming function.

    This is used to represent a transformation such as math::log(PT)
    or math::pow( PT, 2 ).
    """

    def __init__(self, wrapping_function, wrapping_function_header,
                 *arguments):
        bound_functors = [bind_if_needed(arg) for arg in arguments]
        headers = [wrapping_function_header]
        for bound_functor in bound_functors:
            headers += bound_functor.headers()
        code_arguments = ', '.join(
            [bound_functor.code() for bound_functor in bound_functors])
        code = '{wrap}( {args} )'.format(
            wrap=wrapping_function, args=code_arguments)
        nice_code_arguments = ', '.join(
            [bound_functor.code_repr() for bound_functor in bound_functors])
        nice_code = '{wrap}( {args} )'.format(
            wrap=wrapping_function, args=nice_code_arguments)
        headers = list(set(headers))

        BoundFunctor.__init__(
            self, code=code, nice_code=nice_code, headers=headers)


def Functor(representation,
            cppname,
            header,
            brief_docs,
            Params=[],
            TemplateParams=[]):
    """Create a new Functor class and return an instance of it.

    This takes care of generating a new class inheriting from FunctorBase and
    adding some appropriate methods and doc-strings to it.
    """

    def call_without_args(self):
        """Trivial conversion to a BoundFunctor"""
        return BoundFunctor(
            code=self.code(), nice_code=representation, headers=[self._header])

    def call_with_args(self, *args, **kwargs):
        """Convert this Functor object to a BoundFunctor, using the given
        parameter values. To encourage explicit code, all parameters must
        be specified by name, unless there is only one parameter in which
        case it can be passed as a positional argument. Passing additional
        named arguments also causes an error."""

        if len(args) > 1 and len(self._arguments) > 1:
            raise Exception(
                "Functors that take more >1 argument must have those arguments specified by keyword."
            )
        elif len(args) == 1:
            if len(kwargs):
                raise Exception(
                    "You may not mix keyword and positional arguments to functors."
                )
            # we got exactly one positional argument and exactly zero keyword arguments
            if len(self._arguments) == 0:
                raise Exception(
                    "You passed a positional argument to a functor that doesn't take arguments."
                )
            # there is exactly one argument expected and no ambiguity
            kwargs[self._arguments[0][0]] = args[0]

        cpp_headers = [self._header]
        cpp_arguments = []
        repr_arguments = []
        for argtuple in self._arguments:
            argname, argdocs, argtype = argtuple[:3]
            formatter = argtuple[3] if len(argtuple) > 3 else None
            if argname in kwargs:
                provided_value = kwargs.pop(argname)
                value_repr = provided_value
                doc_comment = '/* {comment} */ '.format(
                    comment=argdocs.replace('*', '\\*'))
                if argtype == BoundFunctor:
                    # We want a BoundFunctor. We can try to get one by binding
                    # with no arguments (a no-op if the object was already a
                    # BoundFunctor). This will work if we were given an unbound
                    # functor that doesn't expect arguments (e.g. PT)...
                    provided_value = provided_value()
                    value_repr = provided_value.code_repr()
                if isinstance(provided_value, argtype):
                    # all good
                    repr_arguments.append((argname, value_repr))
                    if formatter is not None:
                        code, headers = formatter(provided_value)
                        cpp_arguments.append(doc_comment + code)
                        cpp_headers += headers
                        #TODO also format the repr
                    else:
                        try:
                            code, headers = python_to_cpp_str(provided_value)
                            cpp_arguments.append(doc_comment + code)
                            cpp_headers += headers
                        except:
                            raise TypeError(
                                "Not sure how to express argument '{arg}' of type {type}"
                                .format(
                                    arg=provided_value,
                                    type=type(provided_value)))
                else:
                    raise Exception(
                        "Incompatible type {provided} given, expected {expected}"
                        .format(
                            provided=str(type(provided_value)),
                            expected=str(argtype)))
            else:
                raise Exception(
                    "No value provided for compulsory argument {argname}".
                    format(argname=argname))

        if len(self._template_arguments):
            targs = []
            # Now handle the template arguments
            for n, argtuple in enumerate(self._template_arguments):
                targ, tdoc = argtuple[:2]
                formatter = argtuple[2] if len(argtuple) > 2 else None
                if targ in kwargs:
                    # The template parameter was actually specified
                    provided_value = kwargs.pop(targ)
                    repr_arguments.append((targ, provided_value))
                    if formatter:
                        code, headers = formatter(provided_value)
                        targs.append(code)
                        cpp_headers += headers
                    else:
                        assert type(provided_value) == str
                        targs.append(provided_value)
                else:
                    # The parameter was not specified. This is fine as long as
                    # the rest of the parameters are also unspecified.
                    assert not any([
                        targ in kwargs
                        for targ in self._template_arguments[n + 1:]
                    ])
                    break
            template_arguments = '<' + ', '.join(targs) + '>'
        else:
            template_arguments = ''
        if len(kwargs):
            raise Exception("Unknown parameters were provided: " +
                            repr(kwargs))
        cpp_str = '{name}{template_params}( {args} )'.format(
            name=self._cppname,
            template_params=template_arguments,
            args=', '.join(cpp_arguments))
        repr_str = '{name}({args})'.format(
            name=representation,
            args=', '.join(["{}={}".format(n, v) for n, v in repr_arguments]))
        return BoundFunctor(
            code=cpp_str, nice_code=repr_str, headers=list(set(cpp_headers)))

    call_doc_lines = ["Keyword arguments:"]
    for argtuple in Params:
        argname, argdocs, argtype = argtuple[:3]
        call_doc_lines.append(' -- '.join([argname, argdocs,
                                           argtype.__name__]))
    call_with_args.__doc__ = '\n'.join(call_doc_lines)

    def code_without_args(self):
        """This is needed to handle the case that the entire functor is
        something like "PT" -- i.e. an unbound functor that does not take any
        parameters. If we did not implement this method, this functor would
        have to be written as PT(), which would be inconsistent.
        """
        return self._cppname + '{}'

    def code_with_args(self):
        """We cannot produce C++ directly from an unbound functor with arguments.
        This is a dummy method that exists only to throw an informative error.
        """
        raise Exception(
            "This functor expects arguments, so you must provide them. Try: FUNCTOR -> FUNCTOR(a=b, ...)"
        )

    def headers(self):
        """Return the list of headers needed to compile this functor."""
        return [self._header]

    has_args = (len(Params) > 0)
    cdict = {
        '__doc__': brief_docs,
        '__call__': call_with_args if has_args else call_without_args,
        '_arguments': Params,
        '_template_arguments': TemplateParams,
        '_cppname': top_level_namespace + cppname,
        '_header': header_prefix + header,
        'code': code_with_args if has_args else code_without_args,
        'headers': headers,
    }
    new_type_name = cppname.replace('::', '__')
    t = type(new_type_name, (FunctorBase, ), cdict)()
    return t if has_args else t()
