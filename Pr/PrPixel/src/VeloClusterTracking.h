/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <array>
#include <tuple>
#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/RawEvent.h"
#include "Event/VPLightCluster.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/VPChannelID.h"
#include "Kernel/VPConstants.h"
#include "PrKernel/PrPixelModule.h"
#include "PrKernel/VeloPixelInfo.h"
#include "VPDet/DeVP.h"

#include "Event/PrVeloHits.h"
#include "Event/PrVeloTracks.h"

namespace LHCb::Pr::Velo {

  namespace VPInfos {
    constexpr int NPlanes           = 26;
    constexpr int NModulesPerPlane  = 2;
    constexpr int NModules          = NPlanes * NModulesPerPlane;
    constexpr int NSensorsPerModule = 4;
    constexpr int NSensors          = NModules * NSensorsPerModule;
    constexpr int NSensorsPerPlane  = NModulesPerPlane * NSensorsPerModule;
  } // namespace VPInfos

  namespace TrackParams {
    constexpr float max_slope              = 0.4f;
    constexpr float tolerance              = 0.6f;
    constexpr float max_scatter_seeding    = 0.004f;
    constexpr float max_scatter_forwarding = 0.004f;
    constexpr float max_scatter_3hits      = 0.02f;
    constexpr float phi_window             = 0.06f;
    constexpr int   min_hit_per_track      = 4;
    constexpr int   max_allowed_skip       = 1;
    constexpr bool  save_LHCbIds           = true;

    constexpr float phi_window_param( float z ) {
      const float min_window = 0.03f;
      const float max_window = 0.12f;
      return min_window + std::abs( z ) * ( ( max_window - min_window ) / 751.f );
    }
  } // namespace TrackParams

  /**
   * @class ClusterTracking ClusterTracking.h
   * Experimental Clustering and Tracking algorithm using avx512
   *
   * @author Arthur Hennequin (CERN, LIP6)
   */

  class ClusterTracking
      : public Gaudi::Functional::MultiTransformer<std::tuple<Hits, Tracks, Tracks>( const RawEvent& )> {

  public:
    /// Standard constructor
    ClusterTracking( const std::string& name, ISvcLocator* pSvcLocator );

    /// Algorithm initialization
    StatusCode initialize() override;

    /// Algorithm execution
    std::tuple<Hits, Tracks, Tracks> operator()( const RawEvent& ) const override;

  private:
    /// Recompute the geometry in case of change
    StatusCode rebuildGeometry();
    /// Detector element
    DeVP* m_vp{nullptr};

    /// List of pointers to modules (which contain pointers to their hits)
    /// FIXME This is not thread safe on a change on geometry.
    /// FIXME These vectors should be stored as a derived condition
    std::vector<PrPixelModule*> m_modules;
    std::vector<PrPixelModule>  m_module_pool;

    /// Indices of first and last module
    unsigned int m_firstModule;
    unsigned int m_lastModule;

    /// Maximum allowed cluster size (no effect when running on lite clusters).
    unsigned int m_maxClusterSize = 999999999;

    std::array<std::array<float, 12>, VP::NSensors> m_ltg;
    std::array<float, VP::NSensorColumns>           m_local_x{};
    std::array<float, VP::NSensorColumns>           m_x_pitch{};
    float                                           m_pixel_size;

    Gaudi::Property<std::vector<unsigned int>> m_modulesToSkip{
        this, "ModulesToSkip", {}, "List of modules that should be skipped in decoding"};
    std::bitset<VP::NModules> m_modulesToSkipMask;

    mutable Gaudi::Accumulators::SummingCounter<> m_nbClustersCounter{this, "Nb of Produced Clusters"};
    mutable Gaudi::Accumulators::SummingCounter<> m_nbTracksCounter{this, "Nb of Produced Tracks"};
  };
} // namespace LHCb::Pr::Velo
