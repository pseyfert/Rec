/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// @Author: Arthur Hennequin (LIP6, CERN)

#pragma once

#include "VeloClusterTracking.h"
#include "fastAtan2.h"

namespace VeloTracking {
  inline __attribute__( ( always_inline ) ) void copy_track( const LightTracksSoA& from, LightTracksSoA& to,
                                                             const int from_n, int& to_n ) {
    to.sum_scatter[to_n] = from.sum_scatter[from_n];

    to.x0[to_n] = from.x0[from_n];
    to.y0[to_n] = from.y0[from_n];
    to.z0[to_n] = from.z0[from_n];

    to.x1[to_n] = from.x1[from_n];
    to.y1[to_n] = from.y1[from_n];
    to.z1[to_n] = from.z1[from_n];

    to.x2[to_n] = from.x2[from_n];
    to.y2[to_n] = from.y2[from_n];
    to.z2[to_n] = from.z2[from_n];

    to.phi2[to_n] = from.phi2[from_n];

    if ( TrackParams::save_LHCbIds ) {
      for ( int i = 0; i < from.n_hits[from_n]; i++ ) {
        to.cId[i * MAX_TRACKS_PER_EVENT + to_n] = from.cId[i * MAX_TRACKS_PER_EVENT + from_n];
      }
    }
    to.n_hits[to_n]  = from.n_hits[from_n];
    to.skipped[to_n] = from.skipped[from_n];

    to_n++;
  }

  inline __attribute__( ( always_inline ) ) void sort_by_phi( PlaneSoA* Pin, PlaneSoA* Pout, int* perms, int n_hits ) {
    for ( int i = 0; i < n_hits; i++ ) {
      Pin->phi[i] = faster_atan2( Pin->Gy[i], Pin->Gx[i] );
      perms[i]    = i;
    }
    std::sort( perms, perms + n_hits, [&Pin]( int a, int b ) { return Pin->phi[a] < Pin->phi[b]; } );
    for ( int i = 0; i < n_hits; i++ ) {
      int j        = perms[i];
      Pout->Gx[i]  = Pin->Gx[j];
      Pout->Gy[i]  = Pin->Gy[j];
      Pout->Gz[i]  = Pin->Gz[j];
      Pout->phi[i] = Pin->phi[j];
      Pout->cId[i] = Pin->cId[j];
    }
  }

  inline __attribute__( ( always_inline ) ) int TrackSeeding( PlaneSoA* P0, PlaneSoA* P1, PlaneSoA* P2,
                                                              LightTracksSoA* tracks, // Output track candidates
                                                              const int       t_insert_start ) {
    int n_tracks = 0;
    int start0   = 0;
    for ( int h1 = 0; h1 < P1->n_hits; h1++ ) {
      if ( is_used( P1, h1 ) ) continue;

      const float x1   = P1->Gx[h1];
      const float y1   = P1->Gy[h1];
      const float z1   = P1->Gz[h1];
      const float phi1 = P1->phi[h1];

      float bestFit = TrackParams::max_scatter_seeding;
      int   bestH0  = -1;
      int   bestH2  = -1;

      int start2 = P2->n_hits;

      for ( int h0 = start0; h0 < P0->n_hits; h0++ ) {
        if ( is_used( P0, h0 ) ) continue;

        const float x0 = P0->Gx[h0];
        const float y0 = P0->Gy[h0];
        const float z0 = P0->Gz[h0];

        const float phi0 = P0->phi[h0];
        if ( fabsf( phi1 - phi0 ) > TrackParams::phi_window ) {
          if ( phi0 > phi1 ) break;
          start0 = h0 + 1;
          continue;
        }

        for ( int h2 = start2 - 1; h2 >= 0; h2-- ) {
          const float phi2 = P2->phi[h2];
          if ( fabsf( phi1 - phi2 ) > TrackParams::phi_window ) {
            if ( phi2 < phi1 ) break;
            start2 = h2;
            continue;
          }

          const float x2 = P2->Gx[h2];
          const float y2 = P2->Gy[h2];
          const float z2 = P2->Gz[h2];

          float r = ( z2 - z0 ) / ( z1 - z0 );
          float x = x0 + ( x1 - x0 ) * r;
          float y = y0 + ( y1 - y0 ) * r;

          float dx      = x - x2;
          float dy      = y - y2;
          float dz      = z2 - z1;
          float scatter = ( dx * dx + dy * dy ) / ( dz * dz );

          if ( fabsf( dx ) < TrackParams::tolerance && fabsf( dy ) < TrackParams::tolerance && scatter < bestFit ) {
            bestFit = scatter;
            bestH0  = h0;
            bestH2  = h2;
          }
        } // h2
      }   // h0

      if ( bestH0 != -1 ) { // Add triplet to tracks
        float x0 = P0->Gx[bestH0];
        float y0 = P0->Gy[bestH0];
        float z0 = P0->Gz[bestH0];

        float x2 = P2->Gx[bestH2];
        float y2 = P2->Gy[bestH2];
        float z2 = P2->Gz[bestH2];

        int i = t_insert_start + n_tracks;

        tracks->sum_scatter[i] = bestFit; // only h2 has scattering

        tracks->x0[i] = x0;
        tracks->y0[i] = y0;
        tracks->z0[i] = z0;

        tracks->x1[i] = x1;
        tracks->y1[i] = y1;
        tracks->z1[i] = z1;

        tracks->x2[i] = x2;
        tracks->y2[i] = y2;
        tracks->z2[i] = z2;

        tracks->phi2[i] = phi1; // Cheat a little: phi1 ~= phi2

        if ( TrackParams::save_LHCbIds ) {
          tracks->cId[i]                            = P0->offset + bestH0;
          tracks->cId[MAX_TRACKS_PER_EVENT + i]     = P1->offset + h1;
          tracks->cId[2 * MAX_TRACKS_PER_EVENT + i] = P2->offset + bestH2;
        }

        tracks->n_hits[i]  = 3;
        tracks->skipped[i] = 0;
        n_tracks++;

        // no need to mark h0
        set_used( P1, h1 );
        set_used( P2, bestH2 );
      }

    } // h1

    return n_tracks;
  }

  inline __attribute__( ( always_inline ) ) void TrackForwarding( LightTracksSoA* tracks, // Track candidates
                                                                  const int n_tracks, PlaneSoA* P2,
                                                                  LightTracksSoA* tracks_forwarded, int& n_forwarded,
                                                                  LightTracksSoA* tracks_finalized, int& n_finalized ) {
    for ( int t = 0; t < n_tracks; t++ ) {
      float x0 = tracks->x1[t];
      float y0 = tracks->y1[t];
      float z0 = tracks->z1[t];

      float x1 = tracks->x2[t];
      float y1 = tracks->y2[t];
      float z1 = tracks->z2[t];

      float phi1 = tracks->phi2[t];

      float bestFit = TrackParams::max_scatter_forwarding;
      int   bestH2  = -1;

      for ( int h2 = 0; h2 < P2->n_hits; h2++ ) {
        if ( is_used( P2, h2 ) ) continue;

        const float phi2 = P2->phi[h2];
        if ( fabsf( phi1 - phi2 ) > TrackParams::phi_window ) {
          if ( phi2 > phi1 ) break;
          continue; // cannot restart from start2, because tracks not ordered
        }

        const float x2 = P2->Gx[h2];
        const float y2 = P2->Gy[h2];
        const float z2 = P2->Gz[h2];

        float r = ( z2 - z0 ) / ( z1 - z0 );
        float x = x0 + ( x1 - x0 ) * r;
        float y = y0 + ( y1 - y0 ) * r;

        float dx      = x - x2;
        float dy      = y - y2;
        float dz      = z2 - z1;
        float scDen2  = dz * dz;
        float scatter = ( dx * dx + dy * dy ) / scDen2;

        if ( fabsf( dx ) < TrackParams::tolerance && fabsf( dy ) < TrackParams::tolerance && scatter < bestFit ) {
          bestFit = scatter;
          bestH2  = h2;
        }
      }

      if ( tracks->skipped[t] <= TrackParams::max_allowed_skip ) {
        if ( bestH2 != -1 ) {
          float x2 = P2->Gx[bestH2];
          float y2 = P2->Gy[bestH2];
          float z2 = P2->Gz[bestH2];

          tracks->sum_scatter[t] += bestFit;

          if ( TrackParams::save_LHCbIds ) {
            tracks->cId[tracks->n_hits[t] * MAX_TRACKS_PER_EVENT + t] = P2->offset + bestH2;
          }
          tracks->n_hits[t]++;
          tracks->skipped[t] = 0;

          tracks->x1[t] = x1;
          tracks->y1[t] = y1;
          tracks->z1[t] = z1;

          tracks->x2[t] = x2;
          tracks->y2[t] = y2;
          tracks->z2[t] = z2;

          tracks->phi2[t] = P2->phi[bestH2];

          set_used( P2, bestH2 );
        } else {
          tracks->skipped[t]++;
        }
        copy_track( *tracks, *tracks_forwarded, t, n_forwarded );
      } else if ( tracks->n_hits[t] >= TrackParams::min_hit_per_track ||
                  tracks->sum_scatter[t] < TrackParams::max_scatter_3hits ) {
        copy_track( *tracks, *tracks_finalized, t, n_finalized );
      }
    }
  }

  inline __attribute__( ( always_inline ) ) void copy_remaining( LightTracksSoA* tracks_candidates, int n_candidates,
                                                                 LightTracksSoA* tracks, int& n_tracks_output ) {
    for ( int t = 0; t < n_candidates; t++ ) {
      if ( tracks_candidates->n_hits[t] >= TrackParams::min_hit_per_track ||
           tracks_candidates->sum_scatter[t] < TrackParams::max_scatter_3hits ) {
        copy_track( *tracks_candidates, *tracks, t, n_tracks_output );
      }
    }
  }
} // namespace VeloTracking
