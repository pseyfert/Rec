/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// @Author: Arthur Hennequin (LIP6, CERN)

#pragma once

#include "VeloClusterTracking.h"
#include <immintrin.h>

#include "VPSortByPhi_avx512.h"

namespace VeloTracking {
  inline __attribute__( ( always_inline ) ) void sort_by_phi( PlaneSoA* Pin, PlaneSoA* Pout, int*, int n_hits ) {
    for ( int i = 0; i < n_hits; i += 16 ) {
      __m512 key = compute_key( Pin, i );
      _mm512_store_ps( &( Pout->phi[i] ), key );
    }

    quicksort_avx512( Pout->phi, 0, n_hits - 1 );

    for ( int i = 0; i < n_hits; i += 16 ) {
      __m512 key = _mm512_load_ps( &( Pout->phi[i] ) );
      permute_hits( Pin, Pout, key, i );
    }
  }

  // Assume all indices in h are < 512
  inline void set_used_512( PlaneSoA* P, __mmask16 m, __m512i h ) {
    __m512i val = _mm512_load_si512( &( P->used[0] ) );

    __m512i mask = _mm512_maskz_sllv_epi32( m, _mm512_set1_epi32( 1 ), _mm512_and_epi32( h, _mm512_set1_epi32( 31 ) ) );
    __m512i perm = _mm512_maskz_srli_epi32( m, h, 5 );

    __m512i vi = _mm512_setzero_epi32();
    for ( int i = 0; i < 16; i++ ) {
      __mmask16 k = _mm512_cmpeq_epi32_mask( vi, perm );
      __m512i   v = _mm512_maskz_set1_epi32( 1 << i, _mm512_mask_reduce_or_epi32( k, mask ) );
      val         = _mm512_or_epi32( val, v );
      vi          = _mm512_add_epi32( vi, _mm512_set1_epi32( 1 ) );
    }
    _mm512_store_si512( &( P->used[0] ), val );
  }

  inline __m512 diff_phi( __m512 phi1, __m512 phi2 ) { return _mm512_abs_ps( _mm512_sub_ps( phi1, phi2 ) ); }

  inline __attribute__( ( always_inline ) ) int TrackSeeding( PlaneSoA* P0, PlaneSoA* P1, PlaneSoA* P2,
                                                              LightTracksSoA* tracks, // Output track candidates
                                                              const int       t_insert_start ) {
    int n_tracks = 0;
    int start0   = 0;
    int start2   = 0;

    const int MAX_H0_CANDIDATES_PER_H1 = 4;
    int16_t   insert_mask[MAX_H0_CANDIDATES_PER_H1];
    __m512    x0_candidates[MAX_H0_CANDIDATES_PER_H1];
    __m512    y0_candidates[MAX_H0_CANDIDATES_PER_H1];
    __m512    z0_candidates[MAX_H0_CANDIDATES_PER_H1];
    __m512i   h0_candidates[MAX_H0_CANDIDATES_PER_H1];

    __m512i inc = _mm512_set1_epi32( 16 );
    __m512i vh1 =
        _mm512_add_epi32( _mm512_set1_epi32( P1->offset ),
                          _mm512_setr_epi32( -16, -15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1 ) );
    for ( int h1 = 0; h1 < P1->n_hits; h1 += 16 ) {
      vh1              = _mm512_add_epi32( vh1, inc );
      __mmask16 used_1 = P1->used[h1 / 16];
      if ( ( h1 + 16 ) > P1->n_hits ) used_1 |= 0xFFFF << ( P1->n_hits & 15 );

      if ( used_1 == 0xFFFF ) continue;

      const __m512 x1   = _mm512_load_ps( &( P1->Gx[h1] ) );
      const __m512 y1   = _mm512_load_ps( &( P1->Gy[h1] ) );
      const __m512 z1   = _mm512_load_ps( &( P1->Gz[h1] ) );
      const __m512 phi1 = _mm512_load_ps( &( P1->phi[h1] ) );
      const float  phi1f =
          ( ( h1 + 16 ) > P1->n_hits ) ? P1->phi[P1->n_hits - 1] : P1->phi[h1 + 15]; // largest phi in h1

      __m512    bestFit = _mm512_set1_ps( TrackParams::max_scatter_seeding );
      __mmask16 bestH0  = 0;
      __m512i   bestH2  = _mm512_setzero_epi32();
      __m512i   vh0     = _mm512_setzero_epi32();
      __m512i   vh2     = _mm512_setzero_epi32();

      __m512 bestX0 = _mm512_setzero_ps();
      __m512 bestY0 = _mm512_setzero_ps();
      __m512 bestZ0 = _mm512_setzero_ps();
      __m512 bestX2 = _mm512_setzero_ps();
      __m512 bestY2 = _mm512_setzero_ps();
      __m512 bestZ2 = _mm512_setzero_ps();

      for ( int i = 0; i < MAX_H0_CANDIDATES_PER_H1; i++ ) insert_mask[i] = 0; // reset candidates masks

      // Fill h0 candidates TETRIS-style
      for ( int h0 = start0; h0 < P0->n_hits; h0++ ) {
        if ( is_used( P0, h0 ) ) continue;

        const __m512i cid0 = _mm512_set1_epi32( P0->offset + h0 );
        const __m512  x0   = _mm512_set1_ps( P0->Gx[h0] );
        const __m512  y0   = _mm512_set1_ps( P0->Gy[h0] );
        const __m512  z0   = _mm512_set1_ps( P0->Gz[h0] );

        const float  phi0f = P0->phi[h0];
        const __m512 phi0  = _mm512_set1_ps( phi0f );
        const __m512 dphi  = diff_phi( phi1, phi0 );
        __mmask16    m_phi = _mm512_mask_cmple_ps_mask( ~used_1, dphi, _mm512_set1_ps( TrackParams::phi_window ) );
        if ( m_phi == 0 ) {
          if ( phi1f < phi0f ) break;
          start0 = h0 + 1;
          continue;
        }

        // h0 is a candidate
        for ( int i = 0; i < MAX_H0_CANDIDATES_PER_H1; i++ ) {
          __mmask16 can_store = m_phi & ( ~insert_mask[i] );
          insert_mask[i] |= m_phi;

          x0_candidates[i] = _mm512_mask_mov_ps( x0_candidates[i], can_store, x0 );
          y0_candidates[i] = _mm512_mask_mov_ps( y0_candidates[i], can_store, y0 );
          z0_candidates[i] = _mm512_mask_mov_ps( z0_candidates[i], can_store, z0 );
          h0_candidates[i] = _mm512_mask_mov_epi32( h0_candidates[i], can_store, cid0 );

          m_phi &= ~can_store;
          if ( m_phi == 0 ) break;
        }
      }

      int next_start2 = start2; // start2 cannot be updated while all h0 candidates havent been processed
      for ( int i = 0; i < MAX_H0_CANDIDATES_PER_H1; i++ ) {
        __mmask16 m_phi = insert_mask[i];
        if ( m_phi == 0 ) break;

        const __m512  x0 = x0_candidates[i];
        const __m512  y0 = y0_candidates[i];
        const __m512  z0 = z0_candidates[i];
        const __m512i h0 = h0_candidates[i];

        for ( int h2 = start2; h2 < P2->n_hits; h2++ ) {
          const float  phi2f  = P2->phi[h2];
          const __m512 phi2   = _mm512_set1_ps( phi2f );
          const __m512 dphi   = diff_phi( phi1, phi2 );
          __mmask16    m_phi2 = _mm512_mask_cmple_ps_mask( ~used_1, dphi, _mm512_set1_ps( TrackParams::phi_window ) );
          if ( m_phi2 == 0 ) {
            if ( phi1f < phi2f ) break;
            next_start2 = h2 + 1;
            continue;
          }

          m_phi2 &= m_phi;
          if ( m_phi2 == 0 ) continue;

          const __m512i cid2 = _mm512_set1_epi32( P2->offset + h2 );
          const __m512  x2   = _mm512_set1_ps( P2->Gx[h2] );
          const __m512  y2   = _mm512_set1_ps( P2->Gy[h2] );
          const __m512  z2   = _mm512_set1_ps( P2->Gz[h2] );

          const __m512 vr = _mm512_div_ps( _mm512_sub_ps( z2, z0 ), _mm512_sub_ps( z1, z0 ) );
          const __m512 x  = _mm512_fmadd_ps( _mm512_sub_ps( x1, x0 ), vr, x0 );
          const __m512 y  = _mm512_fmadd_ps( _mm512_sub_ps( y1, y0 ), vr, y0 );

          const __m512 dx      = _mm512_sub_ps( x, x2 );
          const __m512 dy      = _mm512_sub_ps( y, y2 );
          const __m512 dz      = _mm512_sub_ps( z2, z1 );
          const __m512 scDen2  = _mm512_mul_ps( dz, dz );
          const __m512 scatter = _mm512_div_ps( _mm512_fmadd_ps( dx, dx, _mm512_mul_ps( dy, dy ) ), scDen2 );

          __mmask16 m_tol =
              _mm512_mask_cmplt_ps_mask( m_phi2, _mm512_abs_ps( dx ), _mm512_set1_ps( TrackParams::tolerance ) );
          m_tol = _mm512_mask_cmplt_ps_mask( m_tol, _mm512_abs_ps( dy ), _mm512_set1_ps( TrackParams::tolerance ) );

          __mmask16 m_bestfit = _mm512_mask_cmplt_ps_mask( m_tol, scatter, bestFit );

          if ( m_bestfit == 0 ) continue;

          bestFit = _mm512_mask_mov_ps( bestFit, m_bestfit, scatter );
          bestX0  = _mm512_mask_mov_ps( bestX0, m_bestfit, x0 );
          bestY0  = _mm512_mask_mov_ps( bestY0, m_bestfit, y0 );
          bestZ0  = _mm512_mask_mov_ps( bestZ0, m_bestfit, z0 );
          bestX2  = _mm512_mask_mov_ps( bestX2, m_bestfit, x2 );
          bestY2  = _mm512_mask_mov_ps( bestY2, m_bestfit, y2 );
          bestZ2  = _mm512_mask_mov_ps( bestZ2, m_bestfit, z2 );
          bestH2  = _mm512_mask_mov_epi32( bestH2, m_bestfit, _mm512_set1_epi32( h2 ) );
          vh0     = _mm512_mask_mov_epi32( vh0, m_bestfit, h0 );
          vh2     = _mm512_mask_mov_epi32( vh2, m_bestfit, cid2 );

          bestH0 |= m_bestfit;
        } // h2
      }   // n_0_candidates
      start2 = next_start2;

      // Clone killing: require that h2 are different
      __m512i ch2 = _mm512_maskz_conflict_epi32( bestH0, vh2 );
      bestH0      = _mm512_mask_cmpeq_epi32_mask( bestH0, ch2, _mm512_setzero_epi32() );

      if ( bestH0 == 0 ) continue;

      int i = t_insert_start + n_tracks;

      // only h2 has scattering
      _mm512_mask_compressstoreu_ps( &( tracks->sum_scatter[i] ), bestH0, bestFit );

      _mm512_mask_compressstoreu_ps( &( tracks->x0[i] ), bestH0, bestX0 );
      _mm512_mask_compressstoreu_ps( &( tracks->y0[i] ), bestH0, bestY0 );
      _mm512_mask_compressstoreu_ps( &( tracks->z0[i] ), bestH0, bestZ0 );

      _mm512_mask_compressstoreu_ps( &( tracks->x1[i] ), bestH0, x1 );
      _mm512_mask_compressstoreu_ps( &( tracks->y1[i] ), bestH0, y1 );
      _mm512_mask_compressstoreu_ps( &( tracks->z1[i] ), bestH0, z1 );

      _mm512_mask_compressstoreu_ps( &( tracks->x2[i] ), bestH0, bestX2 );
      _mm512_mask_compressstoreu_ps( &( tracks->y2[i] ), bestH0, bestY2 );
      _mm512_mask_compressstoreu_ps( &( tracks->z2[i] ), bestH0, bestZ2 );

      _mm512_mask_compressstoreu_ps( &( tracks->phi2[i] ), bestH0, phi1 ); // Cheat a little: phi1 ~= phi2

      _mm512_mask_compressstoreu_epi32( &( tracks->n_hits[i] ), bestH0, _mm512_set1_epi32( 3 ) );
      _mm512_mask_compressstoreu_epi32( &( tracks->skipped[i] ), bestH0, _mm512_setzero_epi32() );

      n_tracks += _mm_popcnt_u32( bestH0 );
      P1->used[h1 / 16] |= bestH0;
      set_used_512( P2, bestH0, bestH2 );

      if ( TrackParams::save_LHCbIds ) {
        _mm512_mask_compressstoreu_epi32( &( tracks->cId[i] ), bestH0, vh0 );
        _mm512_mask_compressstoreu_epi32( &( tracks->cId[MAX_TRACKS_PER_EVENT + i] ), bestH0, vh1 );
        _mm512_mask_compressstoreu_epi32( &( tracks->cId[2 * MAX_TRACKS_PER_EVENT + i] ), bestH0, vh2 );
      }
    } // h1

    return n_tracks;
  }

  inline __attribute__( ( always_inline ) ) void TrackForwarding( LightTracksSoA* tracks, // Track candidates
                                                                  const int n_tracks, PlaneSoA* P2,
                                                                  LightTracksSoA* tracks_forwarded, int& n_forwarded,
                                                                  LightTracksSoA* tracks_finalized, int& n_finalized ) {
    for ( int t = 0; t < n_tracks; t += 16 ) {
      __mmask16 mask = ( ( t + 16 ) > n_tracks ) ? ~( 0xFFFF << ( n_tracks & 15 ) ) : 0xFFFF;

      __m512 x0   = _mm512_load_ps( &( tracks->x1[t] ) );
      __m512 y0   = _mm512_load_ps( &( tracks->y1[t] ) );
      __m512 z0   = _mm512_load_ps( &( tracks->z1[t] ) );
      __m512 x1   = _mm512_load_ps( &( tracks->x2[t] ) );
      __m512 y1   = _mm512_load_ps( &( tracks->y2[t] ) );
      __m512 z1   = _mm512_load_ps( &( tracks->z2[t] ) );
      __m512 phi1 = _mm512_load_ps( &( tracks->phi2[t] ) );

      float phi1f = _mm512_reduce_max_ps( phi1 ); // largest phi in tracks

      __m512    bestFit = _mm512_set1_ps( TrackParams::max_scatter_forwarding );
      __mmask16 bestH0  = 0;
      __m512i   bestH2  = _mm512_setzero_epi32();
      __m512    bestX2  = x1;
      __m512    bestY2  = y1;
      __m512    bestZ2  = z1;
      __m512    bestPhi = phi1;
      __m512i   vh2     = _mm512_setzero_epi32();

      for ( int h2 = 0; h2 < P2->n_hits; h2++ ) {
        if ( is_used( P2, h2 ) ) continue;

        float     phi2f  = P2->phi[h2];
        __m512    phi2   = _mm512_set1_ps( phi2f );
        __m512    dphi   = diff_phi( phi1, phi2 );
        __mmask16 m_phi2 = _mm512_mask_cmple_ps_mask( mask, dphi, _mm512_set1_ps( TrackParams::phi_window ) );
        if ( m_phi2 == 0 ) {
          if ( phi1f < phi2f ) break;
          continue; // cannot restart from start2, because tracks not ordered
        }

        __m512i cid2 = _mm512_set1_epi32( P2->offset + h2 );
        __m512  x2   = _mm512_set1_ps( P2->Gx[h2] );
        __m512  y2   = _mm512_set1_ps( P2->Gy[h2] );
        __m512  z2   = _mm512_set1_ps( P2->Gz[h2] );

        __m512 vr = _mm512_div_ps( _mm512_sub_ps( z2, z0 ), _mm512_sub_ps( z1, z0 ) );
        __m512 x  = _mm512_fmadd_ps( _mm512_sub_ps( x1, x0 ), vr, x0 );
        __m512 y  = _mm512_fmadd_ps( _mm512_sub_ps( y1, y0 ), vr, y0 );

        __m512       dx      = _mm512_sub_ps( x, x2 );
        __m512       dy      = _mm512_sub_ps( y, y2 );
        __m512       dz      = _mm512_sub_ps( z2, z1 );
        const __m512 scDen2  = _mm512_mul_ps( dz, dz );
        const __m512 scatter = _mm512_div_ps( _mm512_fmadd_ps( dx, dx, _mm512_mul_ps( dy, dy ) ), scDen2 );

        __mmask16 m_tol =
            _mm512_mask_cmplt_ps_mask( m_phi2, _mm512_abs_ps( dx ), _mm512_set1_ps( TrackParams::tolerance ) );
        m_tol = _mm512_mask_cmplt_ps_mask( m_tol, _mm512_abs_ps( dy ), _mm512_set1_ps( TrackParams::tolerance ) );

        __mmask16 m_bestfit = _mm512_mask_cmplt_ps_mask( m_tol, scatter, bestFit );

        if ( m_bestfit == 0 ) continue;

        bestFit = _mm512_mask_mov_ps( bestFit, m_bestfit, scatter );
        bestX2  = _mm512_mask_mov_ps( bestX2, m_bestfit, x2 );
        bestY2  = _mm512_mask_mov_ps( bestY2, m_bestfit, y2 );
        bestZ2  = _mm512_mask_mov_ps( bestZ2, m_bestfit, z2 );
        bestPhi = _mm512_mask_mov_ps( bestPhi, m_bestfit, phi2 );
        bestH2  = _mm512_mask_mov_epi32( bestH2, m_bestfit, _mm512_set1_epi32( h2 ) );
        vh2     = _mm512_mask_mov_epi32( vh2, m_bestfit, cid2 );

        bestH0 |= m_bestfit;
      }

      // Finish loading tracks
      __m512  tx0     = _mm512_load_ps( &( tracks->x0[t] ) );
      __m512  ty0     = _mm512_load_ps( &( tracks->y0[t] ) );
      __m512  tz0     = _mm512_load_ps( &( tracks->z0[t] ) );
      __m512i n_hits  = _mm512_load_epi32( &( tracks->n_hits[t] ) );
      __m512i skipped = _mm512_load_epi32( &( tracks->skipped[t] ) );

      __m512 ssc = _mm512_load_ps( &( tracks->sum_scatter[t] ) );

      // increment or reset to 0
      skipped = _mm512_maskz_add_epi32( ~bestH0, skipped, _mm512_set1_epi32( 1 ) );

      // increment only if we found a hit
      n_hits = _mm512_mask_add_epi32( n_hits, bestH0, n_hits, _mm512_set1_epi32( 1 ) );
      ssc    = _mm512_mask_add_ps( ssc, bestH0, ssc, bestFit );

      x1 = _mm512_mask_mov_ps( x0, bestH0, x1 );
      y1 = _mm512_mask_mov_ps( y0, bestH0, y1 );
      z1 = _mm512_mask_mov_ps( z0, bestH0, z1 );

      __mmask16 m_forward =
          _mm512_mask_cmple_epi32_mask( mask, skipped, _mm512_set1_epi32( TrackParams::max_allowed_skip ) );

      // Mark used
      set_used_512( P2, bestH0 & m_forward, bestH2 );

      // Forward tracks
      int i = n_forwarded;

      _mm512_mask_compressstoreu_ps( &( tracks_forwarded->sum_scatter[i] ), m_forward, ssc );

      _mm512_mask_compressstoreu_ps( &( tracks_forwarded->x0[i] ), m_forward, tx0 );
      _mm512_mask_compressstoreu_ps( &( tracks_forwarded->y0[i] ), m_forward, ty0 );
      _mm512_mask_compressstoreu_ps( &( tracks_forwarded->z0[i] ), m_forward, tz0 );

      _mm512_mask_compressstoreu_ps( &( tracks_forwarded->x1[i] ), m_forward, x1 );
      _mm512_mask_compressstoreu_ps( &( tracks_forwarded->y1[i] ), m_forward, y1 );
      _mm512_mask_compressstoreu_ps( &( tracks_forwarded->z1[i] ), m_forward, z1 );

      _mm512_mask_compressstoreu_ps( &( tracks_forwarded->x2[i] ), m_forward, bestX2 );
      _mm512_mask_compressstoreu_ps( &( tracks_forwarded->y2[i] ), m_forward, bestY2 );
      _mm512_mask_compressstoreu_ps( &( tracks_forwarded->z2[i] ), m_forward, bestZ2 );

      _mm512_mask_compressstoreu_ps( &( tracks_forwarded->phi2[i] ), m_forward, bestPhi );

      _mm512_mask_compressstoreu_epi32( &( tracks_forwarded->n_hits[i] ), m_forward, n_hits );
      _mm512_mask_compressstoreu_epi32( &( tracks_forwarded->skipped[i] ), m_forward, skipped );

      if ( TrackParams::save_LHCbIds ) {
        int max_n_hits = _mm512_mask_reduce_max_epi32( m_forward, n_hits );
        for ( int j = 0; j < max_n_hits; j++ ) {
          __m512i   cid      = _mm512_load_epi32( &( tracks->cId[j * MAX_TRACKS_PER_EVENT + t] ) );
          __mmask16 push_hit = _mm512_mask_cmpeq_epi32_mask( bestH0, n_hits, _mm512_set1_epi32( j + 1 ) );
          cid                = _mm512_mask_mov_epi32( cid, push_hit, vh2 );
          _mm512_mask_compressstoreu_epi32( &( tracks_forwarded->cId[j * MAX_TRACKS_PER_EVENT + i] ), m_forward, cid );
        }
      }

      n_forwarded += _mm_popcnt_u32( m_forward );

      // Finalize track
      __mmask16 m_final = _mm512_cmpge_epi32_mask( n_hits, _mm512_set1_epi32( TrackParams::min_hit_per_track ) );
      m_final |= _mm512_cmplt_ps_mask( ssc, _mm512_set1_ps( TrackParams::max_scatter_3hits ) );
      m_final &= mask & ~m_forward;

      if ( m_final == 0 ) continue; // Nothing to finalize

      i = n_finalized;

      _mm512_mask_compressstoreu_ps( &( tracks_finalized->sum_scatter[i] ), m_final, ssc );

      _mm512_mask_compressstoreu_ps( &( tracks_finalized->x0[i] ), m_final, tx0 );
      _mm512_mask_compressstoreu_ps( &( tracks_finalized->y0[i] ), m_final, ty0 );
      _mm512_mask_compressstoreu_ps( &( tracks_finalized->z0[i] ), m_final, tz0 );

      _mm512_mask_compressstoreu_ps( &( tracks_finalized->x1[i] ), m_final, x1 );
      _mm512_mask_compressstoreu_ps( &( tracks_finalized->y1[i] ), m_final, y1 );
      _mm512_mask_compressstoreu_ps( &( tracks_finalized->z1[i] ), m_final, z1 );

      _mm512_mask_compressstoreu_ps( &( tracks_finalized->x2[i] ), m_final, bestX2 );
      _mm512_mask_compressstoreu_ps( &( tracks_finalized->y2[i] ), m_final, bestY2 );
      _mm512_mask_compressstoreu_ps( &( tracks_finalized->z2[i] ), m_final, bestZ2 );

      _mm512_mask_compressstoreu_ps( &( tracks_finalized->phi2[i] ), m_final, bestPhi );

      _mm512_mask_compressstoreu_epi32( &( tracks_finalized->n_hits[i] ), m_final, n_hits );
      _mm512_mask_compressstoreu_epi32( &( tracks_finalized->skipped[i] ), m_final, skipped );

      if ( TrackParams::save_LHCbIds ) {
        int max_n_hits = _mm512_mask_reduce_max_epi32( m_final, n_hits );
        for ( int j = 0; j < max_n_hits; j++ ) {
          __m512i cid = _mm512_load_epi32( &( tracks->cId[j * MAX_TRACKS_PER_EVENT + t] ) );
          _mm512_mask_compressstoreu_epi32( &( tracks_finalized->cId[j * MAX_TRACKS_PER_EVENT + i] ), m_final, cid );
        }
      }

      n_finalized += _mm_popcnt_u32( m_final );
    }
  }

  inline __attribute__( ( always_inline ) ) void copy_remaining( LightTracksSoA* tracks_candidates, int n_candidates,
                                                                 LightTracksSoA* tracks, int& n_tracks_output ) {
    __m512i min_hit_per_track = _mm512_set1_epi32( TrackParams::min_hit_per_track );
    __m512  max_scatter_3hits = _mm512_set1_ps( TrackParams::max_scatter_3hits );
    for ( int t = 0; t < n_candidates; t += 16 ) {
      __mmask16 mask = ( ( t + 16 ) > n_candidates ) ? ~( 0xFFFF << ( n_candidates & 15 ) ) : 0xFFFF;

      __m512i n_hits = _mm512_load_epi32( &( tracks_candidates->n_hits[t] ) );
      __m512  ssc    = _mm512_load_ps( &( tracks_candidates->sum_scatter[t] ) );

      __mmask16 m_final = _mm512_mask_cmpge_epi32_mask( mask, n_hits, min_hit_per_track );
      m_final |= _mm512_mask_cmplt_ps_mask( mask, ssc, max_scatter_3hits );

      int i = n_tracks_output;
      _mm512_mask_compressstoreu_ps( &( tracks->sum_scatter[i] ), m_final, ssc );

      _mm512_mask_compressstoreu_ps( &( tracks->x0[i] ), m_final, _mm512_load_ps( &( tracks_candidates->x0[t] ) ) );
      _mm512_mask_compressstoreu_ps( &( tracks->y0[i] ), m_final, _mm512_load_ps( &( tracks_candidates->y0[t] ) ) );
      _mm512_mask_compressstoreu_ps( &( tracks->z0[i] ), m_final, _mm512_load_ps( &( tracks_candidates->z0[t] ) ) );

      _mm512_mask_compressstoreu_ps( &( tracks->x1[i] ), m_final, _mm512_load_ps( &( tracks_candidates->x1[t] ) ) );
      _mm512_mask_compressstoreu_ps( &( tracks->y1[i] ), m_final, _mm512_load_ps( &( tracks_candidates->y1[t] ) ) );
      _mm512_mask_compressstoreu_ps( &( tracks->z1[i] ), m_final, _mm512_load_ps( &( tracks_candidates->z1[t] ) ) );

      _mm512_mask_compressstoreu_ps( &( tracks->x2[i] ), m_final, _mm512_load_ps( &( tracks_candidates->x2[t] ) ) );
      _mm512_mask_compressstoreu_ps( &( tracks->y2[i] ), m_final, _mm512_load_ps( &( tracks_candidates->y2[t] ) ) );
      _mm512_mask_compressstoreu_ps( &( tracks->z2[i] ), m_final, _mm512_load_ps( &( tracks_candidates->z2[t] ) ) );

      _mm512_mask_compressstoreu_ps( &( tracks->phi2[i] ), m_final, _mm512_load_ps( &( tracks_candidates->phi2[t] ) ) );

      _mm512_mask_compressstoreu_epi32( &( tracks->n_hits[i] ), m_final, n_hits );
      _mm512_mask_compressstoreu_epi32( &( tracks->skipped[i] ), m_final,
                                        _mm512_load_epi32( &( tracks_candidates->skipped[t] ) ) );

      if ( TrackParams::save_LHCbIds ) {
        int max_n_hits = _mm512_mask_reduce_max_epi32( m_final, n_hits );
        for ( int j = 0; j < max_n_hits; j++ ) {
          __m512i cid = _mm512_load_epi32( &( tracks_candidates->cId[j * MAX_TRACKS_PER_EVENT + t] ) );
          _mm512_mask_compressstoreu_epi32( &( tracks->cId[j * MAX_TRACKS_PER_EVENT + i] ), m_final, cid );
        }
      }

      n_tracks_output += _mm_popcnt_u32( m_final );
    }
  }
} // namespace VeloTracking
