/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRTRACKRECODUMPER_H
#define PRTRACKRECODUMPER_H 1

// Include files
#include "Event/MCParticle.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCVertex.h"
#include "Event/ODIN.h"
#include "Event/Track.h"
#include "Event/VPLightCluster.h"
#include "GaudiAlg/Consumer.h"
#include "Linker/LinkerWithKey.h"
#include "PrKernel/PrFTHitHandler.h"
#include "PrKernel/UTHitHandler.h"
#include "PrKernel/UTHitInfo.h"

// ROOT
#include <TFile.h>
#include <TTree.h>

/** @class PrTraackRecoDumper PrTrackRecoDumper.h
 *  Dumping of reconstructed tracks and truth of MC particle associated to these tracks
 *
 */
/*

*/

typedef std::vector<LHCb::Track> Tracks;

class PrTrackRecoDumper
    : public Gaudi::Functional::Consumer<void( const Tracks&, const LHCb::VPLightClusters&, const LHCb::ODIN&,
                                               const PrFTHitHandler<PrHit>&, const UT::HitHandler&
                                               // const LHCb::LinksByKey&
                                               )> {
public:
  /// Standard constructor
  PrTrackRecoDumper( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  StatusCode finalize() override;

  void operator()( const Tracks& recTracks, const LHCb::VPLightClusters& VPClusters, const LHCb::ODIN& odin,
                   const PrFTHitHandler<PrHit>& ftHits, const UT::HitHandler& utHits ) const override;

protected:
  TFile* file = nullptr;
  TTree* tree = nullptr;

  int* eventID = nullptr;

  double* p  = nullptr;
  double *px = nullptr, *py = nullptr, *pz = nullptr;
  double* pt  = nullptr;
  double *eta = nullptr, *phi = nullptr;
  bool*   isMatched = nullptr;
  // vertex origin of the particle
  double* ovtx_x                = nullptr;
  double* ovtx_y                = nullptr;
  double* ovtx_z                = nullptr;
  int*    pid                   = nullptr;
  bool*   fromBeautyDecay       = nullptr;
  bool*   fromCharmDecay        = nullptr;
  bool*   fromStrangeDecay      = nullptr;
  int*    DecayOriginMother_pid = nullptr;
  int*    key                   = nullptr;
  double* ghostProb             = nullptr;
  double* chi2                  = nullptr;
  int*    ndof                  = nullptr;

  int*                       nVeloHits    = nullptr;
  std::vector<float>*        Velo_x       = nullptr;
  std::vector<float>*        Velo_y       = nullptr;
  std::vector<float>*        Velo_z       = nullptr;
  std::vector<int>*          Velo_Module  = nullptr;
  std::vector<int>*          Velo_Sensor  = nullptr;
  std::vector<int>*          Velo_Station = nullptr;
  std::vector<unsigned int>* Velo_lhcbID  = nullptr;
  std::vector<unsigned int>* Velo_index   = nullptr;

  std::vector<float>*        FT_hitx         = nullptr;
  std::vector<float>*        FT_hitz         = nullptr;
  std::vector<float>*        FT_hitw         = nullptr;
  std::vector<float>*        FT_hitDXDY      = nullptr;
  std::vector<float>*        FT_hitYMin      = nullptr;
  std::vector<float>*        FT_hitYMax      = nullptr;
  std::vector<int>*          FT_hitPlaneCode = nullptr;
  std::vector<int>*          FT_hitzone      = nullptr;
  std::vector<unsigned int>* FT_lhcbID       = nullptr;

  int*                       nFTHits      = nullptr;
  std::vector<float>*        UT_cos       = nullptr;
  std::vector<float>*        UT_cosT      = nullptr;
  std::vector<float>*        UT_dxDy      = nullptr;
  std::vector<unsigned int>* UT_lhcbID    = nullptr;
  std::vector<int>*          UT_planeCode = nullptr;
  std::vector<float>*        UT_sinT      = nullptr;
  std::vector<int>*          UT_size      = nullptr;
  std::vector<float>*        UT_tanT      = nullptr;
  std::vector<float>*        UT_weight    = nullptr;
  std::vector<float>*        UT_xAtYEq0   = nullptr;
  std::vector<float>*        UT_xAtYMid   = nullptr;
  std::vector<float>*        UT_xMax      = nullptr;
  std::vector<float>*        UT_xMin      = nullptr;
  std::vector<float>*        UT_xT        = nullptr;
  std::vector<float>*        UT_yBegin    = nullptr;
  std::vector<float>*        UT_yEnd      = nullptr;
  std::vector<float>*        UT_yMax      = nullptr;
  std::vector<float>*        UT_yMid      = nullptr;
  std::vector<float>*        UT_yMin      = nullptr;
  std::vector<float>*        UT_zAtYEq0   = nullptr;

  int* nUTHits = nullptr;

  Gaudi::Property<std::string> m_track_location{this, "TrackRecoLocation", "Rec/Track/Keyed/Velo",
                                                "Cointainer in which are sto\
red reconstructed traks linked to MC particles"};
};
#endif // PRTRACKRECODUMPER_H
