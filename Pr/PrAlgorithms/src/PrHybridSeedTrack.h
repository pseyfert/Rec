/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRHYBRIDSEEDTRACK_H
#define PRHYBRIDSEEDTRACK_H 1

// Include files
#include "Event/Track_v2.h"
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrHit.h"

/** @class PrHybridSeedTrack PrHybridSeedTrack.h
 *  This is the working class inside the T station pattern
 *  ---LoH: right now, the x projection is with respect to zRef
 *
 *  @author Renato Quagliani
 *  @date   2015-05-13
 */
// Comment it if you don't want to do truth matching
#include "LHCbMath/BloomFilter.h"

#include <boost/container/small_vector.hpp>
#include <boost/container/static_vector.hpp>

// 64 because we also use that as a container for other things
typedef boost::container::static_vector<ModPrHit, PrFTInfo::Numbers::NFTXLayers> SeedTrackHitsX;
typedef boost::container::static_vector<ModPrHit, 12>                            SeedTrackHits;
typedef SeedTrackHits::iterator                                                  SeedTrackHitsIter;
typedef SeedTrackHits::const_iterator                                            SeedTrackHitsConstIter;
typedef SeedTrackHitsX::iterator                                                 SeedTrackHitsIter;
typedef SeedTrackHitsX::const_iterator                                           SeedTrackHitsConstIter;

constexpr unsigned int maxStereoHits = 100;
using StereoHits                     = boost::container::small_vector<ModPrHit, maxStereoHits>;
using StereoIter                     = boost::container::small_vector<ModPrHit, maxStereoHits>::const_iterator;

namespace Pr {
  namespace Hybrid {
    constexpr static float zReference = StateParameters::ZMidT;
    class AbsSeedTrack {
    public:
      virtual ~AbsSeedTrack(){};

      // Zone (0,1) Up Down
      //====================================
      // SETTERS & Getters  Genearl One
      //====================================
      //***********Track Parameters to compute distances
      inline void setParameters( float ax, float bx, float cx ) noexcept {
        m_ax = ax;
        m_bx = bx;
        m_cx = cx;
      }
      inline float ax() const noexcept { return m_ax; }
      inline float bx() const noexcept { return m_bx; }
      inline float cx() const noexcept { return m_cx; }

      //**********Update the parameters of a track iteratively in the Fit
      inline void updateParameters( float dax, float dbx, float dcx ) {
        m_ax += dax;
        m_bx += dbx;
        m_cx += dcx;
      }
      inline void  setXT1( float value ) { m_xT1 = value; }
      inline float xT1() const noexcept { return m_xT1; }
      inline void  setXT3( float value ) { m_xT3 = value; }
      inline float xT3() const noexcept { return m_xT3; }
      inline void  setXT2( float value ) { m_xT2 = value; }
      inline float xT2() const noexcept { return m_xT2; }

      // validity
      inline void setValid( bool v ) { m_valid = v; }
      inline bool valid() const noexcept { return m_valid; }

      inline void setRecovered( bool v ) { m_recovered = v; }
      inline bool recovered() const noexcept { return m_recovered; }

      // dRatio
      inline void  setdRatio( float dRatio ) { m_dRatio = dRatio; }
      inline float dRatio() const noexcept { return m_dRatio; }

      // chi2
      inline void  setChi2( float chi2, int nDoF ) { m_chi2DoF = chi2 / nDoF; }
      inline float chi2PerDoF() const noexcept { return m_chi2DoF; } // m_chi2 / m_nDoF;}

      // X at a given Z
      inline float x( float z ) const noexcept {
        const float dz = z - zReference;
        return m_ax + dz * ( m_bx + dz * m_cx * ( 1.f + m_dRatio * dz ) );
      }
      // X at zRef
      inline float x0() const noexcept { return m_ax; }
      // Slope X-Z plane track at a given z
      inline float xSlope( float z ) const noexcept {
        const float dz = z - zReference;
        return m_bx + 2.f * dz * m_cx + 3.f * dz * dz * m_cx * m_dRatio; // is it need?
      }
      // Slope X-Z plane track at z reference
      inline float xSlope0() const noexcept {
        return m_bx; // is it need?
      }

    protected:
      // Global Protected variables
      float m_chi2DoF{0.f};
      bool  m_valid{true};
      bool  m_recovered{false};
      float m_ax{0.f};
      float m_bx{0.f};
      float m_cx{0.f};
      float m_dRatio{0.f};
      float m_xT1{0.f};
      float m_xT2{0.f};
      float m_xT3{0.f};
    };
    //    //Specialised class for X candidates
    class SeedTrackX final : public AbsSeedTrack {
    public:
      /// Constructor with the z reference
      //      SeedTrackX() = default;
      SeedTrackX() : AbsSeedTrack() { m_hits = {}; };
      SeedTrackX( SeedTrackHitsX hits ) : SeedTrackX() { m_hits = hits; };
      // Handling the hits on the track ( PrHits is vector<PrHit*> )
      SeedTrackHitsX&       hits() noexcept { return m_hits; };
      const SeedTrackHitsX& hits() const noexcept { return m_hits; }
      inline void           addHit( const ModPrHit& hit ) noexcept { m_hits.push_back( hit ); };
      // distance from Hit of the track
      inline float distance( const PrHit& hit ) const noexcept { return hit.x() - x( hit.z() ); }

      //      inline float distance( const PrHit& hit ) const noexcept {
      //        return hit.x() - x( hit.z() );
      //      }
      inline float chi2( const PrHit& hit ) const noexcept {
        const float d = distance( hit );
        float       w = hit.w();
        return d * d * w;
      }
      template <typename Operator>
      struct CompareBySize {
        bool operator()( const SeedTrackX& lhs, const SeedTrackX& rhs ) const noexcept {
          constexpr auto cmp = Operator{};
          return cmp( std::forward_as_tuple( lhs.hits().size(), rhs.chi2PerDoF() ),
                      std::forward_as_tuple( rhs.hits().size(), lhs.chi2PerDoF() ) );
        }
      };
      static constexpr auto LowerBySize   = CompareBySize<std::greater<>>{};
      static constexpr auto GreaterBySize = CompareBySize<std::less<>>{};

      inline unsigned int size() const noexcept { return m_hits.size(); }

      // Global Private variables
    private:
      SeedTrackHitsX m_hits{};
    };

    typedef std::vector<SeedTrackX> SeedTracksX;

    //    //Specialised class for XY candidates
    class SeedTrack final : public AbsSeedTrack {
    public:
      /// Constructor with the z reference
      SeedTrack() = default;
      /// Constructor with the x seed track. Very costly (3.6M cycles as of now on 100 events)
      SeedTrack( SeedTrackHits hits ) : SeedTrack() { m_hits = hits; };
      inline void setFromXCand( const SeedTrackX& xTrack ) {
        m_ax        = xTrack.ax();
        m_bx        = xTrack.bx();
        m_cx        = xTrack.cx();
        m_dRatio    = xTrack.dRatio();
        m_valid     = xTrack.valid();
        m_recovered = xTrack.recovered();
        m_xT1       = xTrack.xT1();
        m_xT2       = xTrack.xT2();
        m_xT3       = xTrack.xT3();
        m_chi2DoF   = xTrack.chi2PerDoF();
        // No need to initiate chi2, nx, ny, xT1, xT2, xT3 as they will always be after.
        // valid and recover are also always to default
        for ( auto hit : xTrack.hits() ) m_hits.push_back( hit );
      }
      // Handling the hits on the track ( PrHits is vector<PrHit*> )
      SeedTrackHits&       hits() noexcept { return m_hits; };
      const SeedTrackHits& hits() const noexcept { return m_hits; }
      inline void          addHit( const ModPrHit& hit ) noexcept { m_hits.push_back( hit ); };
      inline void          setParameters( float ax, float bx, float cx, float ay, float by ) noexcept {
        m_ax = ax;
        m_bx = bx;
        m_cx = cx;
        m_ay = ay;
        m_by = by;
      }
      inline float ay() const noexcept { return m_ay; }
      inline float by() const noexcept { return m_by; }

      //**********Update the parameters of a track iteratively in the Fit
      inline void updateParameters( float dax, float dbx, float dcx, float day = 0.f, float dby = 0.f ) {
        m_ax += dax;
        m_bx += dbx;
        m_cx += dcx;
        m_ay += day;
        m_by += dby;
      }
      inline void setYParam( float ay = 0.f, float by = 0.f ) {
        m_ay = ay;
        m_by = by;
      }
      inline void setnXnY( unsigned int nx, unsigned int ny ) {
        m_nx = nx;
        m_ny = ny;
      }
      inline unsigned int nx() const noexcept { return m_nx; }
      inline unsigned int ny() const noexcept { return m_ny; }

      // BackProjection
      inline float y( float z ) const noexcept { return ( m_ay + m_by * ( z - zReference ) ); }
      inline float yRef() const noexcept { return m_ay; }
      inline float y0() const noexcept { return m_ay - m_by * zReference; }
      // Slope by
      inline float ySlope() const noexcept { return m_by; }
      // y positon on Track
      inline float yOnTrack( const PrHit& hit ) const noexcept { return hit.yOnTrack( y0(), m_by ); }
      // distance from Hit of the track
      inline float distance( const PrHit& hit ) const noexcept {
        float yTra = yOnTrack( hit ); // is it needed for x - layers?
        return hit.x( yTra ) - x( hit.z( yTra ) );
      }
      inline float chi2( const PrHit& hit ) const noexcept {
        const float d = distance( hit );
        float       w = hit.w();
        return d * d * w;
      }

      inline float deltaY( const PrHit& hit ) const noexcept {
        if ( hit.isX() ) return 0.f;
        return distance( hit ) / hit.dxDy();
      }

      template <typename Operator>
      struct CompareBySize {
        bool operator()( const SeedTrack& lhs, const SeedTrack& rhs ) const noexcept {
          constexpr auto cmp = Operator{};
          return cmp( std::forward_as_tuple( lhs.hits().size(), rhs.chi2PerDoF() ),
                      std::forward_as_tuple( rhs.hits().size(), lhs.chi2PerDoF() ) );
        }
      };
      static constexpr auto LowerBySize   = CompareBySize<std::greater<>>{};
      static constexpr auto GreaterBySize = CompareBySize<std::less<>>{};

      inline unsigned int size() const noexcept { return m_hits.size(); }

    protected:
    private:
      // Global Private variables
    private:
      unsigned int  m_nx{0};
      unsigned int  m_ny{0};
      SeedTrackHits m_hits{};
      float         m_ay{0.f};
      float         m_by{0.f};
    };

    typedef std::vector<SeedTrack> SeedTracks;

    /** @brief Set the Chi2/nDoF value and Chi2DoF for the track after a fit including nPars parameters
     */
    inline void updateChi2( SeedTrack& track, unsigned int nPars ) {
      float chi2 =
          std::accumulate( track.hits().begin(), track.hits().end(), 0.f,
                           [&track]( float a, const ModPrHit& hit ) { return a + track.chi2( *( hit.hit ) ); } );
      track.setChi2( chi2, track.size() - nPars );
    }
  } // namespace Hybrid
} // namespace Pr
#endif // PRHYBRIDSEEDTRACK
