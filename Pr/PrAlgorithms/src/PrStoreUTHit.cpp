/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "PrStoreUTHit.h"
#include "Event/UTTELL1BoardErrorBank.h"
#include "GaudiKernel/IRegistry.h"
#include "Kernel/UTLexicalCaster.h"
#include "Kernel/UTPPRepresentation.h"
#include "UTDet/DeUTDetector.h"
#include "boost/lexical_cast.hpp"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( LHCb::Pr::StoreUTHit, "PrStoreUTHit" )

namespace LHCb::Pr {

  StoreUTHit::StoreUTHit( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     KeyValue{"RawEventLocations", Gaudi::Functional::concat_alternatives( RawEventLocation::Tracker,
                                                                                           RawEventLocation::Other,
                                                                                           RawEventLocation::Default )},
                     KeyValue{"UTHitsLocation", UT::Info::HitLocation} ) {}

  StatusCode StoreUTHit::initialize() {

    auto sc = Transformer::initialize();
    if ( sc.isFailure() ) return sc;

    // TODO : alignment need the updateSvc for detector ( UT experts needed )
    m_utDet = getDet<DeUTDetector>( DeUTDetLocation::UT );

    // TODO: rebuild cache on geometry changes
    for ( int srcId = 0; srcId < NBoards; srcId++ ) {
      const UTTell1ID tel1ID( srcId );
      auto            aBoard = m_readoutTool->findByBoardID( tel1ID );
      if ( aBoard ) {
        std::size_t i = 0;
        for ( auto& sector : aBoard->sectorIDs() ) {

          auto aSector = m_utDet->getSector( sector );
          if ( !aSector ) { warning() << tel1ID << " " << sector << " gave NULL UT sector" << endmsg; }

          const std::size_t geomIdx = srcId * NSectorPerBoard + i++;
          assert( geomIdx < m_sectors_cache.size() );
          assert( geomIdx < m_fullchan_cache.size() );

          m_sectors_cache[geomIdx]  = aSector;
          m_fullchan_cache[geomIdx] = {sector.station(), sector.layer(),        sector.detRegion(),
                                       sector.sector(),  sector.uniqueSector(), (unsigned int)sector};
        }
      }
    }

    return StatusCode::SUCCESS;
  }

  UT::HitHandler StoreUTHit::operator()( const LHCb::RawEvent& rawEvt ) const {
    UT::HitHandler hitHandler;
    StatusCode     sc = decodeBanks( rawEvt, hitHandler );
    if ( sc.isFailure() ) {
      throw GaudiException( "Problems in decoding event skipped", "PrStoreUTHit", StatusCode::FAILURE );
    }
    return hitHandler;
  }

  std::vector<unsigned int> StoreUTHit::missingInAction( span<const RawBank*> banks ) const {

    std::vector<unsigned int> missing;
    if ( banks.size() != m_readoutTool->nBoard() ) {
      for ( unsigned int iBoard = 0u; iBoard < m_readoutTool->nBoard(); ++iBoard ) {
        int  testID = m_readoutTool->findByOrder( iBoard )->boardID().id();
        auto iterBank =
            std::find_if( banks.begin(), banks.end(), [&]( const auto b ) { return b->sourceID() == testID; } );
        if ( iterBank == banks.end() ) {
          missing.push_back( (unsigned int)testID );
          std::string lostBank = "lost bank " + boost::lexical_cast<std::string>( testID );
          Warning( lostBank, StatusCode::SUCCESS, 0 ).ignore();
        }
      } // iBoard
    }
    return missing;
  }

  unsigned int StoreUTHit::pcnVote( span<const RawBank*> banks ) const {

    // make a majority vote to get the correct PCN in the event
    std::map<unsigned int, unsigned int> pcns;
    for ( const auto& bank : banks ) {
      UTDecoder decoder( bank->data() );
      // only the good are allowed to vote [the US system..]
      if ( !decoder.header().hasError() ) ++pcns[decoder.header().pcn()];
    } // banks

    auto max =
        std::max_element( pcns.begin(), pcns.end(),
                          []( const std::pair<unsigned int, unsigned int>& lhs,
                              const std::pair<unsigned int, unsigned int>& rhs ) { return lhs.second < rhs.second; } );
    return max == pcns.end() ? UTDAQ::inValidPcn : max->first;
  }

  std::unique_ptr<UTTELL1BoardErrorBanks> StoreUTHit::decodeErrors( const RawEvent& raw ) const {

    // make an empty output vector
    std::unique_ptr<UTTELL1BoardErrorBanks> outputErrors = std::make_unique<UTTELL1BoardErrorBanks>();

    // Pick up ITError bank
    const span<const RawBank*> itf = raw.banks( RawBank::BankType( RawBank::UTError ) );

    if ( !itf.empty() ) {
      ++m_eventsWithError;
      m_errorBanksCounter += itf.size();
    }

    for ( const auto& bank : itf ) {

      std::string errorBank = "sourceID " + boost::lexical_cast<std::string>( bank->sourceID() );
      ++counter( errorBank );

      if ( bank->magic() != RawBank::MagicPattern ) {
        std::string pattern = "wrong magic pattern " + boost::lexical_cast<std::string>( bank->sourceID() );
        Warning( pattern, StatusCode::SUCCESS, 2 ).ignore();
        continue;
      }

      const unsigned int* p       = bank->data();
      unsigned int        w       = 0;
      const unsigned int  bankEnd = bank->size() / sizeof( unsigned int );

      // bank has to be at least 28 words
      if ( bankEnd < UTDAQ::minErrorBankWords ) {
        warning() << "Error bank length is " << bankEnd << " and should be at least " << UTDAQ::minErrorBankWords
                  << endmsg;
        Warning( "Error bank too short --> not decoded for TELL1 " + UT::toString( bank->sourceID() ),
                 StatusCode::SUCCESS, 2 )
            .ignore();
        continue;
      }

      // and less than 56 words
      if ( bankEnd > UTDAQ::maxErrorBankWords ) {
        warning() << "Error bank length is " << bankEnd << " and should be at most " << UTDAQ::maxErrorBankWords
                  << endmsg;
        Warning( "Error bank too long --> not decoded for TELL1 " + UT::toString( bank->sourceID() ),
                 StatusCode::SUCCESS, 2 )
            .ignore();
        continue;
      }

      // make an empty tell1 data object
      UTTELL1BoardErrorBank* myData = new UTTELL1BoardErrorBank();
      outputErrors->insert( myData, bank->sourceID() );

      for ( unsigned int ipp = 0; ipp < UTDAQ::npp && w != bankEnd; ++ipp ) {

        // we must find 5 words
        if ( bankEnd - w < 5 ) {
          Warning( "Ran out of words to read", StatusCode::SUCCESS, 2 ).ignore();
          break;
        }

        UTTELL1Error* errorInfo = new UTTELL1Error( p[w], p[w + 1], p[w + 2], p[w + 3], p[w + 4] );
        myData->addToErrorInfo( errorInfo );
        w += 5; // read 5 first words

        const unsigned int nOptional = errorInfo->nOptionalWords();

        // we must find the optional words + 2 more control words
        if ( bankEnd - w < nOptional + 2 ) {
          Warning( "Ran out of words to read", StatusCode::SUCCESS, 2 ).ignore();
          break;
        }

        const unsigned int* eInfo = nullptr;

        if ( errorInfo->hasErrorInfo() ) {
          // errorInfo->setOptionalErrorWords(p[w], p[w+1], p[w+2], p[w+3], p[w+4]);
          eInfo = &p[w];
          w += 5;
        } // has error information

        errorInfo->setWord10( p[w] );
        ++w;
        errorInfo->setWord11( p[w] );
        ++w;

        // then some more optional stuff
        if ( errorInfo->hasNZS() ) {
          errorInfo->setWord12( p[w] );
          ++w;
        } // nsz info...

        // then some more optional stuff
        if ( errorInfo->hasPed() ) {
          errorInfo->setWord13( p[w] );
          ++w;
        }

        if ( errorInfo->hasErrorInfo() ) {
          errorInfo->setOptionalErrorWords( eInfo[0], eInfo[1], eInfo[2], eInfo[3], eInfo[4] );
        } // has error information

      } //  loop ip [ppx's]

      if ( w != bankEnd ) { error() << "read " << w << " words, expected: " << bankEnd << endmsg; }

    } // end of loop over banks of a certain type

    return outputErrors;
  }

  bool StoreUTHit::canBeRecovered( const UTTELL1BoardErrorBank* bank, const UTClusterWord& word,
                                   const unsigned int pcn ) const {
    UTDAQ::UTPPRepresentation ppRep = UTDAQ::UTPPRepresentation( UTDAQ::UTStripRepresentation( word.channelID() ) );
    unsigned int              pp, beetle, port, strip;
    ppRep.decompose( pp, beetle, port, strip ); // split up the word
    const auto errorInfo = bank->ppErrorInfo( pp );
    bool       ok        = false;
    if ( errorInfo != 0 ) {
      if ( errorInfo->linkInfo( beetle, port, pcn ) == UTTELL1Error::FailureMode::kNone ) { ok = true; }
    }
    return ok;
  }

  inline StatusCode StoreUTHit::decodeBanks( const RawEvent& rawEvt, UT::HitHandler& hitHandler ) const {
    std::unique_ptr<UTTELL1BoardErrorBanks> errorBanks;

    const auto tBanks  = rawEvt.banks( RawBank::UT );
    const auto missing = missingInAction( tBanks );
    if ( UNLIKELY( !missing.empty() ) ) {
      m_lostBanks += missing.size();
      if ( UNLIKELY( tBanks.empty() ) ) {
        ++m_noBanksFound;
        return StatusCode::SUCCESS;
      }
    }

    bool errorBanksFailed = false;

    // info() << "Start Decode" << endmsg;

    hitHandler.reserve( 10000 );

    auto validBanksBuf    = m_validBanks.buffer();
    auto skippedBanksBuf  = m_skippedBanks.buffer();
    auto validSourceIDBuf = m_validSourceID.buffer();
    for ( auto& bank : tBanks ) {
      ++validBanksBuf;
      // get the board and data
      if ( UNLIKELY( bank->magic() != RawBank::MagicPattern ) ) {
        Warning( "wrong magic pattern " + std::to_string( bank->sourceID() ), StatusCode::SUCCESS, 2 ).ignore();
        skippedBanksBuf += tBanks.size();
        continue;
      }
      UTTell1Board* aBoard = m_readoutTool->findByBoardID( UTTell1ID( bank->sourceID() ) );
      if ( UNLIKELY( !aBoard ) ) {
        Warning( "Invalid source ID --> skip bank" + std::to_string( bank->sourceID() ), StatusCode::SUCCESS, 2 )
            .ignore();
        skippedBanksBuf += 1;
        continue;
      }

      // Error handling part
      bool                   pcnVoted  = false;
      bool                   recover   = false;
      UTTELL1BoardErrorBank* errorBank = nullptr;
      auto                   pcn       = UTDAQ::inValidPcn;
      const SiHeaderWord     hWord( bank->data()[0] );
      if ( UNLIKELY( !m_skipErrors && hWord.hasError() ) ) {
        if ( UNLIKELY( !m_recoverMode ) ) {
          Warning( "bank has errors, skip sourceID " + std::to_string( bank->sourceID() ), StatusCode::SUCCESS, 2 )
              .ignore();
          skippedBanksBuf += 1;
          continue;
        }
        // flag that need to recover....
        ++counter( "recovered banks" + std::to_string( bank->sourceID() ) );
        recover = true;
        // ok this is a bit ugly.....
        if ( !errorBanks.get() && !errorBanksFailed ) {
          try {
            errorBanks = decodeErrors( rawEvt );
          } catch ( GaudiException& e ) {
            errorBanksFailed = true;
            warning() << e.what() << endmsg;
          }
        }
        if ( errorBanks.get() ) {
          // vote for pcn if needed
          if ( !pcnVoted ) {
            pcn = pcnVote( tBanks );
            if ( UNLIKELY( pcn == UTDAQ::inValidPcn ) ) {
              skippedBanksBuf += tBanks.size();
              return Warning( "PCN vote failed", StatusCode::SUCCESS, 2 );
            }
          }
          errorBank          = errorBanks->object( bank->sourceID() );
          const auto bankpcn = hWord.pcn();
          if ( pcn != bankpcn && !m_skipErrors ) {
            debug() << "Expected " << pcn << " found " << bankpcn << endmsg;
            skippedBanksBuf += 1;
            continue;
          }
        } // errorbank == 0
      }

      ++validSourceIDBuf;
      const auto bankVersion = UTDAQ::version( bank->version() );

      // check the integrity of the bank --> always skip if not ok
      if ( UNLIKELY( !m_skipErrors ) ) {
        UTDecoder decoder( bank->data() );
        if ( UNLIKELY( !checkDataIntegrity( decoder, aBoard, bank->size(), bankVersion ) ) ) continue;
      }

      // make local decoder
      UTDecoder decoder( bank->data() );
      // read in the first half of the bank
      for ( auto iterDecoder = decoder.posBegin(); iterDecoder != decoder.posEnd(); ++iterDecoder ) {
        if ( !recover || ( errorBank && canBeRecovered( errorBank, *iterDecoder, pcn ) ) ) {
          const auto& aWord     = *iterDecoder;
          const auto  fracStrip = aWord.fracStripBits();

          const std::size_t geomIdx = bank->sourceID() * 6 + ( aWord.channelID() / 512 );
          const std::size_t strip   = ( aWord.channelID() & 511 ) + 1;

          // auto&& [fullChan, strip, interStrip] = aBoard->DAQToOfflineFull( fracStrip, bankVersion, aWord.channelID()
          // ); get the sector associated to the channelID and use it to create the Hit auto aSector =
          // m_utDet->getSector( fullChan.station, fullChan.layer, fullChan.detRegion, fullChan.sector,
          // fullChan.uniqueSector );
          assert( geomIdx < m_sectors_cache.size() );
          assert( geomIdx < m_fullchan_cache.size() );
          const auto& aSector  = m_sectors_cache[geomIdx];
          const auto& fullChan = m_fullchan_cache[geomIdx];

          // note that the channel ID given to AddHit does no have strip bits set
          // this is fine as they are never used. The only use of the given chanID
          // is in the call to planeCode that uses only the station and layer bits
          hitHandler.AddHit( aSector, fullChan.station, fullChan.layer, fullChan.detRegion, fullChan.sector, strip,
                             fracStrip, UTChannelID{(int)( fullChan.chanID + strip )}, // rebuild full ID by adding
                                                                                       // the strip part
                             aWord.pseudoSizeBits(), aWord.hasHighThreshold() );
        }
      }
    }

    // info() << "Num Hits Added = " << hitHandler.nbHits() << endmsg;

    return StatusCode::SUCCESS;
  }

  bool StoreUTHit::checkDataIntegrity( UTDecoder& decoder, const UTTell1Board* aBoard, const unsigned int bankSize,
                                       const UTDAQ::version& /*bankVersion*/ ) const {
    // check the consistancy of the data

    bool ok          = true;
    auto iterDecoder = decoder.posAdcBegin();
    for ( ; iterDecoder != decoder.posAdcEnd(); ++iterDecoder ) {

      const UTClusterWord aWord = iterDecoder->first;

      // make some consistancy checks
      if ( ( iterDecoder->second.size() - 1u < aWord.pseudoSize() ) ) {
        Warning( "ADC values do not match", StatusCode::SUCCESS, 2 ).ignore();
        ok = false;
        break;
      }

      // decode the channel
      if ( !aBoard->validChannel( aWord.channelID() ) ) {
        Warning( "Invalid tell1 channel", StatusCode::SUCCESS, 2 ).ignore();
        ok = false;
        break;
      }

    } // loop clusters

    // final check that we read the total number of bytes in the bank
    if ( ok && (unsigned int)iterDecoder.bytesRead() != bankSize ) {
      ok = false;
      Warning( "Inconsistant byte count", StatusCode::SUCCESS ).ignore();
    }

    if ( !ok ) m_errorBanksCounter += 1;

    return ok;
  }
} // namespace LHCb::Pr
