/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRLINEFITTERY_H
#define PRLINEFITTERY_H 1

// Include files
#include "Math/CholeskyDecomp.h"
#include "PrHybridSeedTrack.h"
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrHit.h"
#include "PrPlaneHybridCounter.h"
#include "SIMDLine.h"

/** @class PrLineFitterY PrLineFitterY.h
 *  Support class to perform Fit of Lines using solely hits in UV layers setting the parameters of
 *  the xz projection and fitting passing iterators of the hough-like cluster
 *  @author renato quagliani
 *  @date   2015-08-01
 */
//#define Truth_Match// (switch on if want to store minCoord; maxCoord and hits uv)

class PrLineFitterY final {
public:
  /// Standard constructor setting
  PrLineFitterY() = default;
  /// Standard constructor
  PrLineFitterY( const Pr::Hybrid::SeedTrackX& track )
      : m_ax( track.ax() ), m_bx( track.bx() ), m_cx( track.cx() ), m_dRatio( track.dRatio() ){};
  /** Set values of the xzprojection
   */
  void setXProj( const Pr::Hybrid::SeedTrackX& track ) noexcept {
    m_ax     = track.ax();
    m_bx     = track.bx();
    m_cx     = track.cx();
    m_dRatio = track.dRatio();
  }
  void reset() {
    m_ay       = std::numeric_limits<float>::max();
    m_by       = std::numeric_limits<float>::max();
    m_chi2line = std::numeric_limits<float>::max();
  }
  // return xzprojection slope at a given z
  float xSlope( const float z ) const noexcept {
    const float dz = z - Pr::Hybrid::zReference;
    return m_bx + 2.f * m_cx * dz + 3.f * m_cx * m_dRatio * dz * dz;
  }
  // return xzprojection x position at a given z
  float x( const float z ) const noexcept {
    const float dz = z - Pr::Hybrid::zReference;
    return ( m_ax + m_bx * dz + m_cx * dz * dz * ( 1.f + m_dRatio * dz ) );
  }
  // return track-hit distance
  float distance( const ModPrHit& hit ) const noexcept {
    const float z    = hit.hit->z();
    const float yAtZ = y( z );
    return ( hit.hit->x( yAtZ ) - x( z ) ); // new
  }

  float distanceAt0( const ModPrHit& hit ) const noexcept {
    const float z = hit.hit->z();
    return ( hit.hit->x() - x( z ) ); // new
  }

  // return the chi2 contribution from a hit
  float chi2hit( const ModPrHit& hit ) const noexcept {
    float       erry = hit.hit->w(); //*(corr*corr);
    const float dist = distance( hit );
    return dist * dist * erry;
  }

  // return yOnTrack in rotated ref frame taking into account the dzDy
  float yOnTrack( const ModPrHit& hit ) const noexcept {
    return hit.hit->yOnTrack( m_ay - Pr::Hybrid::zReference * m_by, m_by );
  }

  // return the y of the track at a given z
  float y( float z ) const noexcept { return m_ay + m_by * ( z - Pr::Hybrid::zReference ); }

  //--- not const method since it updates private variables
  bool fit2( SeedTrackHitsConstIter itBeg, SeedTrackHitsConstIter itEnd,
             const PrPlaneHybridCounter& counter ) noexcept {
    std::fill( m_rhs, m_rhs + 2, 0.f );
    std::fill( m_mat, m_mat + 3, 0.f );
    for ( auto it = itBeg; itEnd != it; ++it ) {
      const PrHit* hit = it->hit;
      // const double wdxDy = (hit)->w()/(float)counter.nbInPlane[ (*hit)->planeCode()];
      const float  nbInPlane = (float)counter.nbInPlane( it->planeCode );
      const double dz        = hit->z() - Pr::Hybrid::zReference;
      const double dist      = distanceAt0( *it );
      const double dxDy      = hit->dxDy();
      // const double CorrError = 1./(1.+ 1.27389* std::fabs( std::atan( xSlope( (*hit) ->z( )) )));
      // const double err2 = hit->werr()*CorrError;
      const double wdxDy   = hit->w() * dxDy / nbInPlane;
      const double wdxDydz = wdxDy * dz;
      m_mat[0] += wdxDy * dxDy;
      m_mat[1] += wdxDy * dxDy * dz;
      m_mat[2] += wdxDydz * dz * dxDy;
      m_rhs[0] -= wdxDy * dist;
      m_rhs[1] -= wdxDydz * dist;
    }
    ROOT::Math::CholeskyDecomp<double, 2> decomp( m_mat );
    if ( !decomp ) {
      m_ay       = std::numeric_limits<float>::max();
      m_by       = std::numeric_limits<float>::max();
      m_chi2line = std::numeric_limits<float>::max();
      return false;
    }
    decomp.Solve( m_rhs );
    m_ay       = m_rhs[0];
    m_by       = m_rhs[1];
    m_chi2line = 0.f;
    for ( auto hit = itBeg; itEnd != hit; ++hit ) { m_chi2line += chi2hit( *hit ); }
    m_chi2line = m_chi2line / (float)( itEnd - itBeg - 2 );
    return true;
  }

  // method to fit the track passing the iterators pointing to the initial and last element of the hough clusters
  bool fit( SeedTrackHitsConstIter itBeg, SeedTrackHitsConstIter itEnd ) noexcept {
    // initialize params to 0
    //---LoH: it should be useless, why is it not?
    m_ay = 0.f;
    m_by = 0.f;
    float __attribute__( ( aligned( 64 ) ) ) dzeta[8];
    float __attribute__( ( aligned( 16 ) ) ) dw[8];
    float __attribute__( ( aligned( 16 ) ) ) dy[8];
    unsigned j = 0;
    for ( auto it = itBeg; itEnd != it; ++it, ++j ) {
      const PrHit* hit    = it->hit;
      const float  dxDy   = hit->dxDy();
      const float  errhit = hit->werr();
      dzeta[j]            = hit->z() - Pr::Hybrid::zReference;
      dw[j]               = std::fabs( dxDy ) * errhit;
      dy[j]               = distanceAt0( *it ) / dxDy;
    }
    while ( j & 3 ) {
      dzeta[j] = dw[j] = dy[j] = 0.f;
      ++j;
    }
    SIMDLineFit fit;
    fit.addHits( dzeta, &dzeta[j], dw, dy );
    SIMDLine line = fit.solve();
    if ( !line.ok() ) {
      m_chi2line = std::numeric_limits<float>::max();
      m_ay       = std::numeric_limits<float>::max();
      m_by       = std::numeric_limits<float>::max();
      return false;
    } else {
      m_ay -= line.x();
      m_by -= line.tx();
      m_chi2line = line.chi2( dzeta, &dzeta[j], dw, dy ) / float( itEnd - itBeg - 2 );
      return true;
    }
    return false;
  }

  float ay() const noexcept { return m_ay; }
  float ay0() const noexcept { return m_ay - m_by * Pr::Hybrid::zReference; }
  float by() const noexcept { return m_by; }
  float Chi2DoF() const noexcept { return m_chi2line; }

protected:
private:
  //  bool  m_vectorised;
  float m_mat[3];
  float m_rhs[2];
  float m_ay{std::numeric_limits<float>::max()};
  float m_by{std::numeric_limits<float>::max()};
  float m_chi2line{std::numeric_limits<float>::max()};
  float m_ax{std::numeric_limits<float>::max()};
  float m_bx{std::numeric_limits<float>::max()};
  float m_cx{std::numeric_limits<float>::max()};
  float m_dRatio{std::numeric_limits<float>::max()};
  // Pr::Hybrid::SeedTrack m_track;
};
#endif // PRLINEFITTERY_H
