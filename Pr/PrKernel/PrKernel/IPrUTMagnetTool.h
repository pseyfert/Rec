/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IPRUTMAGNETTOOL_H
#define IPRUTMAGNETTOOL_H 1

// Include files
#include "Kernel/LUTForFunction.h"
#include <GaudiKernel/IAlgTool.h>

/** @class IPrDebugTool IPrUTMagnetTool.h PrKernel/IPrUTMagnetTool.h
 *  Interface to the tool used by the upstream pattern recognition to
 *  obtain integrals of the B-field as a function of parameters of a
 *  Velo track
 *
 *  @author Roel Aaij
 *  @date   2019-05-29
 */
class IPrUTMagnetTool : public extend_interfaces<IAlgTool> {
public:
  DeclareInterfaceID( IPrUTMagnetTool, 1, 0 );

  virtual float bdlIntegral( float ySlopeVelo, float zOrigin, float zVelo ) const = 0;
  virtual float zBdlMiddle( float ySlopeVelo, float zOrigin, float zVelo ) const  = 0;
  virtual float dist2mom( float ySlope ) const                                    = 0;

  virtual float zMidUT() const    = 0;
  virtual float zMidField() const = 0;

  virtual float averageDist2mom() const = 0;

  virtual LUTForFunction<3, 30> const&      DxLayTable() const = 0;
  virtual LUTForFunction<30, 10, 10> const& BdlTable() const   = 0;
};

#endif
