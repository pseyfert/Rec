/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TsaKernel/TsaSeedingHit.h"

#include "TfKernel/TTStationHitManager.h"

// CRJ : Need to try and find a way to do this automatically ?

// Instantiate an instance of the TT station hit manager for the Tsa extended hits
template class Tf::TTStationHitManager<Tf::Tsa::SeedingHit>;
typedef Tf::TTStationHitManager<Tf::Tsa::SeedingHit> TsaTTStationHitManager;
DECLARE_COMPONENT( TsaTTStationHitManager )
