/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $ Exp $
#ifndef _TsaSelectorBase_H
#define _TsaSelectorBase_H

#include "GaudiAlg/GaudiTool.h"
#include "TsaKernel/ITsaSeedStep.h"

#include <string>

namespace Tf {
  namespace Tsa {

    /** @class SelectorBase TsaSelectorBase.h
     * Follow track and pick up hits
     * @author M. Needham
     **/

    class SelectorBase : public GaudiTool, virtual public ITsaSeedStep {

    public:
      /// constructer
      SelectorBase( const std::string& type, const std::string& name, const IInterface* parent );

      // destructer
      virtual ~SelectorBase();

    protected:
      // execute method
      StatusCode select( std::vector<SeedTrack*>& seeds );

    private:
      double m_fracUsed;
    };

  } // namespace Tsa
} // namespace Tf

#endif // _TsaSeedSimpleSelector_H
