/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _ITsaStubFind_H
#define _ITsaStubFind_H

#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_ITsaStubFind( "ITsaStubFind", 0, 0 );

#include <vector>

namespace Tf {
  namespace Tsa {

    class SeedStub;
    class SeedHit;

    /** @class ITsaStubFind ITsaStubFind.h TsaKernel/ITsaStubFind.h
     *
     *  Interface to Tsa Stub finder
     *
     *  @author M.Needham
     *  @date   12/11/2006
     */

    class ITsaStubFind : virtual public IAlgTool {
    public:
      /// Retrieve interface ID
      static const InterfaceID& interfaceID() { return IID_ITsaStubFind; }

      virtual StatusCode execute( std::vector<SeedHit*> hits[], std::vector<SeedHit*> sHits[],
                                  std::vector<SeedStub*> stubs[] ) = 0;
    };

  } // namespace Tsa
} // namespace Tf

#endif
