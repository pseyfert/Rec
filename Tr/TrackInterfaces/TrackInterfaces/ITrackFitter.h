/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINTERFACES_ITRACKFITTER_H
#define TRACKINTERFACES_ITRACKFITTER_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/SystemOfUnits.h"
// from LHCbKernel
#include "Kernel/TrackDefaultParticles.h"

// vector
#include <vector>

// TrackVectorFit store
#include "Event/Track.h"
#include "Kernel/VectorSOAStore.h"

// Forward declarations

/** @class ITrackFitter ITrackFitter.h TrackInterfaces/ITrackFitter.h
 *
 *  Interface for a track fitting tool.
 *
 *  @author Jose A. Hernando, Eduardo Rodrigues
 *  @date   2005-05-25
 *
 *  @author Rutger van der Eijk  07-04-1999
 *  @author Mattiew Needham
 */
struct ITrackFitter : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( ITrackFitter, 4, 0 );

  // Fit a track
  virtual StatusCode operator()( LHCb::Track& track, const LHCb::Tr::PID& pid = LHCb::Tr::PID::Pion() ) const = 0;

  // Fit a batch of tracks
  // Note: Returns SUCCESS if all tracks are Fitted
  virtual StatusCode operator()( std::vector<std::reference_wrapper<LHCb::Track>>& tracks,
                                 const LHCb::Tr::PID& pid = LHCb::Tr::PID::Pion() ) const = 0;
};

#endif // TRACKINTERFACES_ITRACKFITTER_H
