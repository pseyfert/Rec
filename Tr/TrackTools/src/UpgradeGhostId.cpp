/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Event/FTCluster.h"
#include "Event/GhostTrackInfo.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include "Event/UTCluster.h"
#include "Event/VPLightCluster.h"
#include "Kernel/HitPattern.h"
#include "PrKernel/UTHitHandler.h"
#include "TMVA/TMVA_1_25nsLL_2017_MLP.class.C"
#include "TMVA/TMVA_3_25nsLL_2017_MLP.class.C"
#include "TMVA/TMVA_4_25nsLL_2017_MLP.class.C"
#include "TMVA/TMVA_5_25nsLL_2017_MLP.class.C"
#include "TMVA/TMVA_6_25nsLL_2017_MLP.class.C"
#include "TMVA/UpdateFlattenDownstream.C"
#include "TMVA/UpdateFlattenLong.C"
#include "TMVA/UpdateFlattenTtrack.C"
#include "TMVA/UpdateFlattenUpstream.C"
#include "TMVA/UpdateFlattenVelo.C"
#include "TrackKernel/TrackFunctors.h"

#include "TMath.h"

// local
#include "UpgradeGhostId.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UpgradeGhostId
//
// 2014-12-30 : Paul Seyfert following an earlier version by Angelo Di Canto
// 2019-06-18 : Menglin Xu
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( UpgradeGhostId )

//=============================================================================
//

namespace {
  static const int largestChannelIDype =
      1 + std::max( LHCb::LHCbID::channelIDtype::UT,
                    std::max( LHCb::LHCbID::channelIDtype::VP, LHCb::LHCbID::channelIDtype::FT ) );
  static const int largestTrackTypes = 1 + LHCb::Track::Types::Ttrack;

  static const std::vector<std::string> veloVars = {"UpgradeGhostInfo_obsVP",
                                                    "UpgradeGhostInfo_FitVeloChi2",
                                                    "UpgradeGhostInfo_FitVeloNDoF",
                                                    "UpgradeGhostInfo_veloHits",
                                                    "UpgradeGhostInfo_utHits",
                                                    "TRACK_CHI2",
                                                    "TRACK_NDOF",
                                                    "TRACK_ETA"};

  static const std::vector<std::string> upstreamVars = {"UpgradeGhostInfo_obsVP",
                                                        "UpgradeGhostInfo_FitVeloChi2",
                                                        "UpgradeGhostInfo_FitVeloNDoF",
                                                        "UpgradeGhostInfo_obsUT",
                                                        "UpgradeGhostInfo_UToutlier",
                                                        "UpgradeGhostInfo_veloHits",
                                                        "UpgradeGhostInfo_utHits",
                                                        "TRACK_CHI2",
                                                        "TRACK_NDOF",
                                                        "TRACK_PT",
                                                        "TRACK_ETA"};

  static const std::vector<std::string> downstreamVars = {"UpgradeGhostInfo_obsFT",
                                                          "UpgradeGhostInfo_FitTChi2",
                                                          "UpgradeGhostInfo_FitTNDoF",
                                                          "UpgradeGhostInfo_obsUT",
                                                          "UpgradeGhostInfo_UToutlier",
                                                          "UpgradeGhostInfo_veloHits",
                                                          "UpgradeGhostInfo_utHits",
                                                          "TRACK_CHI2",
                                                          "TRACK_PT",
                                                          "TRACK_ETA"};

  static const std::vector<std::string> longVars = {"UpgradeGhostInfo_obsVP",
                                                    "UpgradeGhostInfo_FitVeloChi2",
                                                    "UpgradeGhostInfo_FitVeloNDoF",
                                                    "UpgradeGhostInfo_obsFT",
                                                    "UpgradeGhostInfo_FitTChi2",
                                                    "UpgradeGhostInfo_FitTNDoF",
                                                    "UpgradeGhostInfo_obsUT",
                                                    "UpgradeGhostInfo_FitMatchChi2",
                                                    "UpgradeGhostInfo_UToutlier",
                                                    "UpgradeGhostInfo_veloHits",
                                                    "UpgradeGhostInfo_utHits",
                                                    "TRACK_CHI2",
                                                    "TRACK_PT",
                                                    "TRACK_ETA"};

  static const std::vector<std::string> ttrackVars = {"UpgradeGhostInfo_obsFT",
                                                      "UpgradeGhostInfo_FitTChi2",
                                                      "UpgradeGhostInfo_FitTNDoF",
                                                      "UpgradeGhostInfo_veloHits",
                                                      "UpgradeGhostInfo_utHits",
                                                      "TRACK_CHI2",
                                                      "TRACK_NDOF",
                                                      "TRACK_PT",
                                                      "TRACK_ETA"};

} // namespace

StatusCode UpgradeGhostId::finalize() {

  std::for_each( m_readers.begin(), m_readers.end(), []( auto& up ) { up.reset(); } );
  std::for_each( m_flatters.begin(), m_flatters.end(), []( auto& up ) { up.reset(); } );
  return GaudiTool::finalize();
}

StatusCode UpgradeGhostId::initialize() {
  if ( !GaudiTool::initialize() ) return StatusCode::FAILURE;

  if ( largestTrackTypes <=
       std::max( LHCb::Track::Types::Ttrack,
                 std::max( std::max( LHCb::Track::Types::Velo, LHCb::Track::Types::Upstream ),
                           std::max( LHCb::Track::Types::Ttrack, LHCb::Track::Types::Downstream ) ) ) )
    return Warning( "ARRAY SIZE SET WRONG (largestTrackTypes is smaller than enum LHCb::Track::Types",
                    StatusCode::FAILURE );

  m_readers.clear();
  m_readers.resize( largestTrackTypes );
  m_readers[LHCb::Track::Types::Velo]       = std::make_unique<ReadMLP_1>( veloVars );
  m_readers[LHCb::Track::Types::Upstream]   = std::make_unique<ReadMLP_4>( upstreamVars );
  m_readers[LHCb::Track::Types::Downstream] = std::make_unique<ReadMLP_5>( downstreamVars );
  m_readers[LHCb::Track::Types::Long]       = std::make_unique<ReadMLP_3>( longVars );
  m_readers[LHCb::Track::Types::Ttrack]     = std::make_unique<ReadMLP_6>( ttrackVars );

  m_flatters.clear();
  m_flatters.resize( largestTrackTypes );
  m_flatters[LHCb::Track::Types::Velo]       = Update_VeloTable();
  m_flatters[LHCb::Track::Types::Upstream]   = Update_UpstreamTable();
  m_flatters[LHCb::Track::Types::Downstream] = Update_DownstreamTable();
  m_flatters[LHCb::Track::Types::Long]       = Update_LongTable();
  m_flatters[LHCb::Track::Types::Ttrack]     = Update_TtrackTable();

  return StatusCode::SUCCESS;
}

namespace {
  inline std::vector<double> subdetectorhits( const LHCb::Track& aTrack ) {

    std::vector<double> returnvalue              = std::vector<double>( largestChannelIDype, 0. );
    returnvalue[LHCb::LHCbID::channelIDtype::UT] = 0.;
    returnvalue[LHCb::LHCbID::channelIDtype::FT] = 0.;
    returnvalue[LHCb::LHCbID::channelIDtype::VP] = 0.;
    for ( auto lhcbid : aTrack.lhcbIDs() ) {
      if ( lhcbid.detectorType() >= returnvalue.size() ) { continue; } // may be a hit in a non-tracking detector
      ( returnvalue[lhcbid.detectorType()] ) += 1.;
    }
    return returnvalue;
  }
} // namespace

//=============================================================================
StatusCode UpgradeGhostId::execute( LHCb::Track& aTrack ) const {
  std::vector<double>         obsarray = subdetectorhits( aTrack );
  const LHCb::TrackFitResult* fit      = fitResult( aTrack );

  std::vector<double> variables;
  variables.reserve( 15 );
  // if (LHCb::Track::Types::Velo == tracktype || LHCb::Track::Types::Long == tracktype || LHCb::Track::Types::Upstream
  // == tracktype) {
  if ( aTrack.hasVelo() ) {
    variables.push_back( obsarray[LHCb::LHCbID::channelIDtype::VP] );
    variables.push_back( aTrack.info( LHCb::Track::AdditionalInfo::FitVeloChi2, -999 ) );
    variables.push_back( aTrack.info( LHCb::Track::AdditionalInfo::FitVeloNDoF, -999 ) );
  }
  // if (LHCb::Track::Types::Downstream == tracktype || LHCb::Track::Types::Long == tracktype ||
  // LHCb::Track::Types::Ttrack == tracktype) {
  if ( aTrack.hasT() ) {
    variables.push_back( obsarray[LHCb::LHCbID::channelIDtype::FT] );
    variables.push_back( aTrack.info( LHCb::Track::AdditionalInfo::FitTChi2, -999 ) );
    variables.push_back( aTrack.info( LHCb::Track::AdditionalInfo::FitTNDoF, -999 ) );
  }
  // if (LHCb::Track::Types::Downstream == tracktype || LHCb::Track::Types::Long == tracktype ||
  // LHCb::Track::Types::Upstream == tracktype) {
  if ( aTrack.hasUT() ) { // includes longtracks w/o ut hits
    variables.push_back( obsarray[LHCb::LHCbID::channelIDtype::UT] );
  }
  if ( LHCb::Track::Types::Long == aTrack.type() ) {
    variables.push_back( aTrack.info( LHCb::Track::AdditionalInfo::FitMatchChi2, -999 ) );
  }
  if ( aTrack.hasUT() ) {
    variables.push_back( fit->nMeasurements<LHCb::Measurement::UT>() -
                         fit->nActiveMeasurements<LHCb::Measurement::UT>() ); // "UpgradeGhostInfo_UToutlier",'F'
  }
  variables.push_back( m_vpClusters.get()->size() );
  variables.push_back( m_utClusters.get()->nbHits() );
  variables.push_back( aTrack.chi2() );
  if ( LHCb::Track::Types::Long != aTrack.type() && LHCb::Track::Types::Downstream != aTrack.type() ) {
    variables.push_back( aTrack.nDoF() );
  }
  if ( LHCb::Track::Types::Velo != aTrack.type() ) { variables.push_back( aTrack.pt() ); }
  variables.push_back( aTrack.pseudoRapidity() );

  // float netresponse = m_readers[aTrack.type()]->GetRarity(variables); // TODO rarity would be nice, see
  // https://sft.its.cern.ch/jira/browse/ROOT-7050
  float netresponse = m_readers[aTrack.type()]->GetMvaValue( variables );
  netresponse       = m_flatters[aTrack.type()]->value( netresponse );
  aTrack.setGhostProbability( 1. - netresponse );

  return StatusCode::SUCCESS;
}

std::vector<std::string> UpgradeGhostId::variableNames( LHCb::Track::Types type ) const {
  switch ( type ) {
  case LHCb::Track::Types::Velo:
    return veloVars;
  case LHCb::Track::Types::Long:
    return longVars;
  case LHCb::Track::Types::Upstream:
    return upstreamVars;
  case LHCb::Track::Types::Downstream:
    return downstreamVars;
  case LHCb::Track::Types::Ttrack:
    return ttrackVars;
  default:
    return std::vector<std::string>();
  }
}
