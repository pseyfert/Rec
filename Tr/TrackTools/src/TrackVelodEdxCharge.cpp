/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file TrackVelodEdxCharge.cpp
 *
 * Implementation file for tool TrackVelodEdxCharge
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 18/07/2006
 */
//-----------------------------------------------------------------------------

#include "TrackVelodEdxCharge.h"
#include "Event/Measurement.h"
#include "Event/TrackFitResult.h"
#include "Event/VeloCluster.h"
#include "Kernel/SiChargeFun.h"
#include "TrackKernel/TrackFunctors.h"
#include "range/v3/view/transform.hpp"

namespace {
  template <typename T>
  std::atomic<T>& operator+=( std::atomic<T>& x, const T& inc ) {
    // get the value of x
    auto current = x.load();
    // repeatedly reload "current" from x until value is as it was last time
    // then replace with current + inc
    while ( !x.compare_exchange_weak( current, current + inc ) ) /* empty on purpose*/
      ;
    return x;
  }
} // namespace

// namespaces
using namespace LHCb;

// Declaration of the Tool Factory
DECLARE_COMPONENT( TrackVelodEdxCharge )

//-----------------------------------------------------------------------------

StatusCode TrackVelodEdxCharge::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  if ( !existDet<DataObject>( detSvc(), "Conditions/ParticleID/Velo/VelodEdx" ) ) {
    Warning( "VELO dE/dx parameters not in conditions DB", StatusCode::SUCCESS ).ignore();
    m_useConditions = false;
  }
  if ( !m_useConditions ) {
    Warning( "Using VELO dE/dx parameters from options not conditions", StatusCode::SUCCESS ).ignore();
  } else {
    registerCondition( "Conditions/ParticleID/Velo/VelodEdx", m_dEdx, &TrackVelodEdxCharge::i_cachedEdx );
    sc = runUpdate();
    if ( !sc ) return sc;
  }

  info() << "VELO dE/dx charge parameters :"
         << " normalisation = " << m_Normalisation.value() << " ratio = " << m_Ratio.value() << endmsg;

  return StatusCode::SUCCESS;
}

StatusCode TrackVelodEdxCharge::nTracks( const LHCb::Track* track, double& nTks ) const {
  ++m_totalTracks;
  // initialise the charge value
  nTks = 0;
  // is track OK
  if ( !track ) return Warning( "Null track pointer passed" );

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Trying Track " << track->key() << endmsg;

  // get the measurements for the track
  const auto& measurements_ = measurements( *track );

  bool useIDs = measurements_.empty();
  if ( !useIDs ) {
    // Fill a temp vector with the velo clusters
    std::vector<const LHCb::VeloCluster*> clusters;
    clusters.reserve( 32 );
    // loop over measurements
    for ( const auto& iM : measurements_ ) {
      // is this a velo measurement
      if ( iM.is<LHCb::Measurement::VeloLite>() ) {
        // have measurements, but no pointers to clusters :(
        useIDs = true;
        break;
      }
      auto* veloFull = iM.getIf<LHCb::Measurement::VeloFull>();
      if ( veloFull ) clusters.push_back( veloFull->cluster );
    } // loop over measurements
    if ( !useIDs ) {
      // how many charges where found
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << " -> Found " << measurements_.size() << " Measurements, used " << clusters.size() << endmsg;
      if ( !clusters.empty() ) {
        nTks = SiChargeFun::truncatedMean( clusters.begin(), clusters.end(), m_Ratio ) / m_Normalisation;
        ++m_veloTracks;
        m_sumEffective += nTks;
      } else {
        // no velo clusters found
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << "   -> No VELO clusters found -> no dE/dx charge measured" << endmsg;
        return StatusCode::FAILURE;
      }
      // if we get here, all is OK
      return StatusCode::SUCCESS;
    }
  }
  // fall back option of getting the clusters from the container by LHCbID
  LHCb::VeloClusters* veloClusters = get<LHCb::VeloClusters*>( LHCb::VeloClusterLocation::Default );
  // try to load clusters from lhcbids
  std::vector<const LHCb::VeloCluster*> clusters;
  for ( auto id : track->lhcbIDs() ) {
    if ( id.isVelo() ) clusters.push_back( veloClusters->object( id.veloID() ) );
  }
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << " -> Found " << track->lhcbIDs().size() << " LHCbIDs, used " << clusters.size() << endmsg;
  if ( !clusters.empty() ) {
    nTks = SiChargeFun::truncatedMean( clusters.begin(), clusters.end(), m_Ratio ) / m_Normalisation;
    ++m_veloTracks;
    m_sumEffective += nTks;
  } else {
    nTks = 0.;
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "   -> No VELO clusters found -> no dE/dx charge measured" << endmsg;
    return StatusCode::FAILURE;
  }
  // if we get here, all is OK
  return StatusCode::SUCCESS;
}

StatusCode TrackVelodEdxCharge::i_cachedEdx() {
  m_Normalisation = m_dEdx->param<double>( "Normalisation" );
  m_Ratio         = m_dEdx->param<double>( "Ratio" );
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << "i_cachedEdx : Normalisation " << m_Normalisation << " Ratio " << m_Ratio << endmsg;

  return StatusCode::SUCCESS;
}

StatusCode TrackVelodEdxCharge::finalize() {
  if ( m_veloTracks > 0 ) {
    info() << "Total tracks " << m_totalTracks << ", with VELO " << m_veloTracks << ", average effective number "
           << m_sumEffective / (double)m_veloTracks << endmsg;
  }
  return GaudiTool::finalize(); // must be called after all other actions
}
