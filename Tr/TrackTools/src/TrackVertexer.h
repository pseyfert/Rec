/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKVERTEXER_H
#define TRACKVERTEXER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/ITrajPoca.h"
#include "TrackInterfaces/ITrackStateProvider.h"
#include "TrackInterfaces/ITrackVertexer.h" // Interface

/** @class TrackVertexer TrackVertexer.h
 *
 *  This implements the Billoir-Fruhwirth-Regler algorithm.
 *
 *  @author Wouter HULSBERGEN
 *  @date   2007-11-07
 */

class TrackVertexer : public extends<GaudiTool, ITrackVertexer> {
public:
  /// Standard constructor
  using extends::extends;

  /// Create a vertex from two track states
  std::unique_ptr<LHCb::TwoProngVertex> fit( const LHCb::State& stateA, const LHCb::State& stateB ) const override;

  /// Create a veretx from a set of states
  std::unique_ptr<LHCb::RecVertex> fit( LHCb::span<const LHCb::State* const> states ) const override;

  /// Create a vertex from a set of tracks.
  std::unique_ptr<LHCb::RecVertex> fit( LHCb::span<const LHCb::Track* const> tracks ) const override;

  /// Compute decaylength and IP chi2 wrt to PV. returns true if successful
  bool computeDecayLength( const LHCb::TwoProngVertex& vertex, const LHCb::RecVertex& pv, double& chi2,
                           double& decaylength, double& decaylengtherr ) const override;

  /// Return the ip chi2 for a track (uses stateprovider)
  double ipchi2( const LHCb::Track& track, const LHCb::RecVertex& pv ) const override;

  /// Return the ip chi2 for a track state
  double ipchi2( const LHCb::State& state, const LHCb::RecVertex& pv ) const override;

  /// initialize
  StatusCode initialize() override;

  /// finalize
  StatusCode finalize() override;

private:
  ToolHandle<ITrackStateProvider> m_stateprovider{this, "StateProvider", "TrackStateProvider"};
  ToolHandle<ITrajPoca>           m_pocatool{"TrajPoca"};
  Gaudi::Property<size_t>         m_maxNumIter{this, "MaxNumberOfIterations", 10}; ///< Max number of iterations
  Gaudi::Property<double>         m_maxDChisq{this, "MaxDeltaChisqForConvergence",
                                      0.01}; ///< Min change in chisquare to run another iteration
  Gaudi::Property<bool>           m_computeMomCov{this, "ComputeMomentumCovariance",
                                        true}; ///< Flag to switch on/off computation of momentum covariance matrix
  Gaudi::Property<bool> m_discardFailedFits{this, "DiscardFailedFits", false}; ///< Return nullptr if fit fails
};
#endif // TRACKVERTEXER_H
