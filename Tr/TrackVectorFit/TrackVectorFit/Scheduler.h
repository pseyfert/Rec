/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Types.h"
#include <bitset>
#include <ostream>

namespace Tr {

  namespace TrackVectorFit {

    namespace Sch {

      struct Item {
        Track*                      track = nullptr;
        std::vector<Node>::iterator node;
        unsigned                    trackIndex = 0;
        unsigned                    nodeIndex  = 0;

        Item() = default;

        Item( Track* track ) : track( track ) {
          trackIndex = track->m_index;
          node       = track->m_nodes.begin();
        }

        void incrementNodeIndex() {
          ++node;
          ++nodeIndex;
        }

        template <size_t N>
        friend std::ostream& operator<<( std::ostream& cout, const std::array<Item, N>& pool ) {
          cout << "{ ";
          for ( unsigned j = 0; j < pool.size(); ++j ) {
            cout << pool[j].trackIndex << "-" << pool[j].nodeIndex << " ";
          }
          cout << "}";
          return cout;
        }
      };

      template <size_t N>
      std::bitset<N> reverse( const std::bitset<N>& set ) {
        std::bitset<N> result;
        for ( size_t i = 0; i < N; i++ ) { result[i] = set[N - i - 1]; }
        return result;
      }

      template <size_t N>
      struct Blueprint {
        std::bitset<N>      in;
        std::bitset<N>      out;
        std::bitset<N>      action;
        std::array<Item, N> pool;

        Blueprint()                        = default;
        Blueprint( const Blueprint& copy ) = default;
        Blueprint( const std::bitset<N>& in, const std::bitset<N>& out, const std::bitset<N>& action,
                   const std::array<Item, N>& pool )
            : in( in ), out( out ), action( action ), pool( pool ) {}

        friend inline std::ostream& operator<<( std::ostream& cout, const Blueprint<N>& blueprint ) {
          cout << reverse<N>( blueprint.in ) << " " << reverse<N>( blueprint.out ) << " "
               << reverse<N>( blueprint.action ) << " " << blueprint.pool;

          return cout;
        }

        friend inline std::ostream& operator<<( std::ostream& cout, const std::list<Blueprint<N>>& plan ) {
          unsigned i = 0;
          for ( auto& blueprint : plan ) { cout << "#" << i++ << ": " << blueprint << std::endl; }
          return cout;
        }
      };

      template <size_t N>
      struct StaticScheduler {
        StaticScheduler()                              = default;
        StaticScheduler( const StaticScheduler& copy ) = default;

        static std::list<Blueprint<N>> generate( std::list<std::reference_wrapper<Track>>& tracks ) {
          auto getSize = []( const Track& t ) { return t.m_nodes.size(); };

          // Order tracks
          std::vector<std::reference_wrapper<Track>> ordered_tracks( tracks.begin(), tracks.end() );
          std::sort( ordered_tracks.begin(), ordered_tracks.end(), [&getSize]( const Track& t0, const Track& t1 ) {
            const auto& type0 = t0.track().type();
            const auto& type1 = t1.track().type();

            // Always prefer the non velo or velor tracks
            if ( !( type0 == LHCb::Track::Types::Velo || type0 == LHCb::Track::Types::VeloR ) &&
                 ( type1 == LHCb::Track::Types::Velo || type1 == LHCb::Track::Types::VeloR ) ) {
              return true;
            } else if ( ( type0 == LHCb::Track::Types::Velo || type0 == LHCb::Track::Types::VeloR ) &&
                        !( type1 == LHCb::Track::Types::Velo || type1 == LHCb::Track::Types::VeloR ) ) {
              return false;
            } else {
              // Otherwise, compare sizes
              const size_t size0 = getSize( t0 );
              const size_t size1 = getSize( t1 );

              return size0 > size1;
            }
          } );

          // Initialise
          std::list<Blueprint<N>> plan;
          std::bitset<N>          in; // Note: Default constructor, it's initialized with zeroes
          std::bitset<N>          out;
          std::bitset<N>          action = std::bitset<N>().set();
          std::array<Item, N>     pool;
          std::vector<unsigned>   slots( N );
          std::iota( slots.begin(), slots.end(), 0 );
          auto trackIterator = ordered_tracks.begin();

          // Iterate over all tracks, add what we need
          while ( trackIterator != ordered_tracks.end() ) {
            Track& track = *trackIterator;

            if ( getSize( track ) > 0 ) {
              // Add the element to a free slot
              const unsigned slot = slots.back();
              slots.pop_back();
              in[slot]   = true;
              pool[slot] = Item( &track );
            }

            while ( slots.empty() ) {
              // Set out mask for tracks where
              // this is the last node to process
              out = false;
              for ( size_t i = 0; i < N; ++i ) {
                Item& s = pool[i];
                if ( s.node + 1 == s.track->m_nodes.end() ) {
                  out[i] = 1;
                  slots.push_back( i );
                }
              }

              // Add to plan
              plan.emplace_back( in, out, action, pool );

              // Initialise in, and prepare nodes for next iteration
              in = false;
              std::for_each( pool.begin(), pool.end(), []( Item& s ) {
                if ( s.node + 1 != s.track->m_nodes.end() ) { s.incrementNodeIndex(); }
              } );
            }

            ++trackIterator;
          }

          // Iterations with no full vectors
          if ( slots.size() < N ) {
            // make processingIndices the indices that didn't finish yet
            std::vector<unsigned> processingIndices( N );
            std::iota( processingIndices.begin(), processingIndices.end(), 0 );
            for ( auto it = slots.rbegin(); it != slots.rend(); ++it ) {
              processingIndices.erase( processingIndices.begin() + *it );
            }

            auto processingIndicesEnd = processingIndices.end();
            while ( processingIndicesEnd != processingIndices.begin() ) {
              // Set out and action mask
              action = false;
              out    = false;
              std::for_each( processingIndices.begin(), processingIndicesEnd, [&]( const unsigned& i ) {
                Item& s   = pool[i];
                action[i] = 1;
                if ( s.node + 1 == s.track->m_nodes.end() ) { out[i] = true; }
              } );

              // Adding to the plan
              plan.emplace_back( in, out, action, pool );

              // Initialise in, and prepare nodes for next iteration
              in = false;
              processingIndicesEnd =
                  std::remove_if( processingIndices.begin(), processingIndicesEnd, [&]( const unsigned& i ) {
                    Item& s = pool[i];
                    if ( s.node + 1 == s.track->m_nodes.end() ) return true;
                    s.incrementNodeIndex();
                    return false;
                  } );
            }
          }

          return plan;
        }
      };

    } // namespace Sch

  } // namespace TrackVectorFit

} // namespace Tr
