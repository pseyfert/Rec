/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "ArrayGenCovariance.h"
#include "VectorConfiguration.h"
#include "scalar/Math.h"
#include <functional>
#include <vector>

#include "Kernel/AlignedAllocator.h"
#include "Kernel/VectorSOAIterator.h"
#include "Kernel/VectorSOAStore.h"
#include "MemView.h"

#include "Event/ChiSquare.h"
#include "Event/Measurement.h"
#include "Event/Node.h"
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include "Event/TrackTypes.h"
#include "TrackKernel/TrackFunctors.h"

#include "GaudiKernel/VectorMap.h"
#include "GaudiKernel/boost_allocator.h"
#include "LHCbMath/MatrixManip.h"
#include "LHCbMath/Similarity.h"
#include "LHCbMath/ValueWithError.h"
#include "Math/SMatrix.h"

namespace Tr {

  namespace TrackVectorFit {

    /**
     * @brief      TrackVectorFit version of TrackFitEvent::FitNode.
     */
    struct FitNode : public LHCb::Node {
      LHCb::State m_forwardLHCbState;
      LHCb::State m_backwardLHCbState;

      FitNode()                 = default;
      FitNode( const FitNode& ) = default;
      FitNode( const double& z, const LHCb::State::Location& location = LHCb::State::Location::LocationUnknown )
          : LHCb::Node( z, location ) {}
      FitNode( const LHCb::Measurement* meas ) : LHCb::Node( meas ) {}

      // Method to clone this FitNode, without losing the FitNode information
      // Hopefully, this is gone in future
      LHCb::Node* clone() const override { return new FitNode( *this ); }

      void setForwardState( const Gaudi::TrackVector& forwardState, const Gaudi::TrackSymMatrix& forwardCovariance ) {
        m_forwardLHCbState = LHCb::State{forwardState, forwardCovariance, z(), state().location()};
      }

      void setBackwardState( const Gaudi::TrackVector&    backwardState,
                             const Gaudi::TrackSymMatrix& backwardCovariance ) {
        m_backwardLHCbState = LHCb::State{backwardState, backwardCovariance, z(), state().location()};
      }

      LHCb::State& forwardState() { return m_forwardLHCbState; }

      const LHCb::State& forwardState() const { return m_forwardLHCbState; }

      LHCb::State& backwardState() { return m_backwardLHCbState; }

      const LHCb::State& backwardState() const { return m_backwardLHCbState; }
    };

    struct Node {
      Mem::View::TrackMatrix<25> m_forwardTransportMatrix;
      Mem::View::TrackMatrix<25> m_backwardTransportMatrix;
      Mem::View::SmoothState     m_smoothState;
      Mem::View::NodeParameters  m_nodeParameters;
      Mem::View::State           m_forwardState;
      Mem::View::State           m_backwardState;

      unsigned m_index;
      int      m_ndof         = 0;
      int      m_ndofBackward = 0;

      FitNode*                 m_node;
      TRACKVECTORFIT_PRECISION m_deltaEnergy =
          ( (TRACKVECTORFIT_PRECISION)0.0 ); // Change in energy in propagation from previous node to this one

      Node( FitNode* node, const unsigned& index ) : m_index( index ), m_node( node ) {}
      Node( const Node& copy ) = default;

      // Const getters
      inline const TRACKVECTORFIT_PRECISION& refResidual() const {
        assert( m_nodeParameters.m_basePointer != nullptr );
        return *m_nodeParameters.m_referenceResidual;
      }
      inline const TRACKVECTORFIT_PRECISION& deltaEnergy() const { return m_deltaEnergy; }
      inline const FitNode&                  node() const { return *m_node; }
      inline TRACKVECTORFIT_PRECISION        chi2() const {
        assert( m_forwardState.m_basePointer != nullptr && m_backwardState.m_basePointer != nullptr );
        return std::max( *( m_forwardState.m_chi2 ), *( m_backwardState.m_chi2 ) );
      }
      inline TRACKVECTORFIT_PRECISION smoothChi2() const {
        assert( m_smoothState.m_basePointer != nullptr );
        const double& res = *( m_smoothState.m_residual );
        const double& err = *( m_smoothState.m_errResidual );
        return ( res * res ) / ( err * err );
      }

      // Reference getters
      inline FitNode& node() { return *m_node; }

      // Setters
      inline void setTransportVector( const Gaudi::TrackVector& transportVector ) {
        assert( m_nodeParameters.m_basePointer != nullptr );
        m_nodeParameters.m_transportVector.copy( transportVector );
      }
      inline void setNoiseMatrix( const Gaudi::TrackSymMatrix& noiseMatrix ) {
        assert( m_nodeParameters.m_basePointer != nullptr );
        m_nodeParameters.m_noiseMatrix.copy( noiseMatrix );
      }
      inline void setRefResidual( const TRACKVECTORFIT_PRECISION& res ) {
        assert( m_nodeParameters.m_basePointer != nullptr );
        ( *m_nodeParameters.m_referenceResidual ) = res;
      }
      inline void setErrMeasure( const TRACKVECTORFIT_PRECISION& res ) {
        assert( m_nodeParameters.m_basePointer != nullptr );
        ( *m_nodeParameters.m_errorMeasure ) = res;
      }

      inline void setDeltaEnergy( const TRACKVECTORFIT_PRECISION& e ) { m_deltaEnergy = e; }
      inline void setTransportMatrix( const Gaudi::TrackMatrix& tm ) { m_forwardTransportMatrix.copy( tm ); }
      inline void calculateAndSetInverseTransportMatrix( const Gaudi::TrackMatrix& tm ) {
        Scalar::Math::invertMatrix( tm, m_backwardTransportMatrix );
      }
      inline void setRefVector( const Gaudi::TrackVector& rv ) {
        assert( m_nodeParameters.m_basePointer != nullptr );
        m_nodeParameters.m_referenceVector.copy( rv );
      }
      inline void setProjection( const Gaudi::TrackProjectionMatrix& pm, const TRACKVECTORFIT_PRECISION& rr,
                                 const TRACKVECTORFIT_PRECISION& em ) {
        assert( m_nodeParameters.m_basePointer != nullptr );
        m_nodeParameters.m_projectionMatrix.copy( pm );
        *m_nodeParameters.m_referenceResidual = rr;
        *m_nodeParameters.m_errorMeasure      = em;
      }
      inline void resetProjection() {
        assert( m_nodeParameters.m_basePointer != nullptr );
        for ( unsigned i = 0; i < 5; ++i ) { m_nodeParameters.m_projectionMatrix[i] = 0.0; }
        *m_nodeParameters.m_referenceResidual = 0.0;
        *m_nodeParameters.m_errorMeasure      = 0.0;
      }

      // The class U type specifier is conditional to S
      // One could do the same with a partially specialized class,
      // but here we want a function
      template <class R, class S,
                class U = std::conditional_t<
                    std::is_same<S, Op::Covariance>::value, Mem::View::TrackSymMatrix,
                    std::conditional_t<
                        std::is_same<S, Op::NoiseMatrix>::value, Mem::View::TrackSymMatrix,
                        std::conditional_t<
                            std::is_same<S, Op::StateVector>::value, Mem::View::TrackVector,
                            std::conditional_t<
                                std::is_same<S, Op::ReferenceVector>::value, Mem::View::TrackVector,
                                std::conditional_t<
                                    std::is_same<S, Op::ProjectionMatrix>::value, Mem::View::TrackVector,
                                    std::conditional_t<
                                        std::is_same<S, Op::TransportVector>::value, Mem::View::TrackVector,
                                        std::conditional_t<std::is_same<S, Op::TransportMatrix>::value,
                                                           Mem::View::TrackMatrix<25>, TRACKVECTORFIT_PRECISION>>>>>>>>
      inline const U& get() const;

      template <class R, class S,
                class U = std::conditional_t<
                    std::is_same<S, Op::Covariance>::value, Mem::View::TrackSymMatrix,
                    std::conditional_t<
                        std::is_same<S, Op::NoiseMatrix>::value, Mem::View::TrackSymMatrix,
                        std::conditional_t<
                            std::is_same<S, Op::StateVector>::value, Mem::View::TrackVector,
                            std::conditional_t<
                                std::is_same<S, Op::ReferenceVector>::value, Mem::View::TrackVector,
                                std::conditional_t<
                                    std::is_same<S, Op::ProjectionMatrix>::value, Mem::View::TrackVector,
                                    std::conditional_t<
                                        std::is_same<S, Op::TransportVector>::value, Mem::View::TrackVector,
                                        std::conditional_t<std::is_same<S, Op::TransportMatrix>::value,
                                                           Mem::View::TrackMatrix<25>, TRACKVECTORFIT_PRECISION>>>>>>>>
      inline U& get();

      inline void setOutlier();
    };

#include "NodeGetters.icpp"

    void Node::setOutlier() {
      // Set this node as outlier
      // Store its state before it dissappears
      auto& node = this->node();
      node.setErrMeasure( this->get<Op::NodeParameters, Op::ErrMeasure>() );
      node.setProjectionMatrix( (Gaudi::TrackProjectionMatrix)this->get<Op::NodeParameters, Op::ProjectionMatrix>() );
      node.setForwardState( (Gaudi::TrackVector)this->get<Op::Forward, Op::StateVector>(),
                            (Gaudi::TrackSymMatrix)this->get<Op::Forward, Op::Covariance>() );
      node.setBackwardState( (Gaudi::TrackVector)this->get<Op::Backward, Op::StateVector>(),
                             (Gaudi::TrackSymMatrix)this->get<Op::Backward, Op::Covariance>() );
      node.setResidual( this->get<Op::Smooth, Op::Residual>() );
      node.setErrResidual( this->get<Op::Smooth, Op::ErrResidual>() );
      const auto smoothStateVector = (Gaudi::TrackVector)this->get<Op::Smooth, Op::StateVector>();
      node.setRefVector( smoothStateVector );
      node.setState( smoothStateVector, (Gaudi::TrackSymMatrix)this->get<Op::Smooth, Op::Covariance>(), node.z() );

      node.setType( LHCb::Node::Type::Outlier );
    }

    struct Track {
      std::vector<Node> m_nodes;

      // Chi square of track
      TRACKVECTORFIT_PRECISION m_forwardFitChi2  = cast( 0. );
      TRACKVECTORFIT_PRECISION m_backwardFitChi2 = cast( 0. );
      TRACKVECTORFIT_PRECISION m_savedChi2       = cast( 0.0 );

      // Some other things
      int      m_ndof      = 0;
      int      m_savedndof = 0;
      int      m_nTrackParameters;
      bool     m_savedPrefit;
      unsigned m_iterationsToConverge;
      unsigned m_numberOfOutlierIterations;

      LHCb::Track* m_track;
      unsigned     m_index;

      // Note: This will be invoked only if the size of the scheduler is the same
      inline void updateRefVectors() {
        std::for_each( m_nodes.begin(), m_nodes.end(), []( Node& n ) {
          n.get<Op::NodeParameters, Op::ReferenceVector>().copy( n.get<Op::Smooth, Op::StateVector>() );
        } );
      }

      Track( LHCb::Track& track, const unsigned& trackIndex ) : m_track( &track ), m_index( trackIndex ) {
        // Generate VectorFit nodes
        unsigned nodeIndex = 0;
        auto     fit       = fitResult( track );
        m_nodes.reserve( fit->nodes().size() );
        for ( auto*& node : fit->nodes() ) {
          if ( node->type() == LHCb::Node::Type::HitOnTrack ) {
            m_nodes.emplace_back( static_cast<FitNode*>( node ), nodeIndex++ );
          }
        }
      }

      Track( LHCb::Track& track ) : Track( track, 0 ) {}

      Track( const Track& track ) = default;

      inline const LHCb::Track& track() const { return *m_track; }

      inline LHCb::Track& track() { return *m_track; }

      inline std::vector<Node>& nodes() { return m_nodes; }

      inline const std::vector<Node>& nodes() const { return m_nodes; }

      inline void setNTrackParameters( const int& nTrackParameters ) { m_nTrackParameters = nTrackParameters; }

      inline int nTrackParameters() const { return m_nTrackParameters; }

      inline TRACKVECTORFIT_PRECISION chi2() const { return std::max( m_forwardFitChi2, m_backwardFitChi2 ); }

      inline TRACKVECTORFIT_PRECISION minChi2() const { return std::min( m_forwardFitChi2, m_backwardFitChi2 ); }

      inline int ndof() const { return m_ndof; }

      /**
       * @brief Saves the current chi2 for posterior checks.
       */
      inline void saveChi2() {
        m_savedChi2 = track().chi2();
        m_savedndof = m_ndof;
      }

      inline TRACKVECTORFIT_PRECISION chi2Difference() const { return std::abs( m_savedChi2 - track().chi2() ); }

      inline bool isPrefit() const {
        return std::any_of( m_nodes.begin(), m_nodes.end(), []( const Node& n ) {
          const auto& node = n.node();
          if ( !node.hasMeasurement() ) return false;
          const auto* otmeas = node.measurement().template getIf<LHCb::Measurement::OT>();
          return otmeas && otmeas->driftTimeStrategy == LHCb::Measurement::OT::DriftTimeStrategy::PreFit;
        } );
      }

      inline void savePrefit() { m_savedPrefit = isPrefit(); }

      inline bool wasPrefit() const { return m_savedPrefit; }

      inline void setIterationsToConverge( const unsigned& i ) { m_iterationsToConverge = i; }

      inline void saveNumberOfOutlierIterations( const unsigned& n ) { m_numberOfOutlierIterations = n; }

      inline unsigned numberOfOutlierIterations() { return m_numberOfOutlierIterations; }

      /**
       * @brief Calculates the intermediate chi squares and updates the TrackFitResult object.
       * @details This routine calculates the chisquare contributions from
       *          different segments of the track. It uses the chisquare
       *          contributions from the bi-directional kalman fit. Summing these
       *          leads to a real chisquare only if the contributions are
       *          uncorrelated. For a Velo-TT-T track you can then calculate:
       *
       *          - the chisquare of the T segment and the T-TT segment by using the
       *          'upstream' contributions
       *
       *          - the chisquare of the Velo segment and the Velo-TT segment by
       *          using the 'downstream' contributions
       *
       *          Note that you cannot calculate the contribution of the TT segment
       *          seperately (unless there are no T or no Velo hits). Also, if
       *          there are Muon hits, you cannot calculate the T station part, so
       *          for now this only works for tracks without muon hits.
       */
      inline void calculateChi2Types() {
        LHCb::TrackFitResult& fit = *fitResult( track() );

        auto veloBegin = m_nodes.end();
        auto ttBegin   = m_nodes.end();
        auto tBegin    = m_nodes.end();
        auto muonBegin = m_nodes.end();
        auto veloEnd   = m_nodes.end();
        auto ttEnd     = m_nodes.end();
        auto tEnd      = m_nodes.end();
        auto muonEnd   = m_nodes.end();

        for ( auto it = m_nodes.begin(); it != m_nodes.end(); ++it ) {
          if ( it->node().type() != LHCb::Node::Type::HitOnTrack ) continue;
          it->node().measurement().visit( [&]( const auto& arg ) {
            using arg_t = std::decay_t<decltype( arg )>;
            if constexpr ( std::is_base_of_v<LHCb::Measurement::Velo, arg_t> ||
                           std::is_same_v<LHCb::Measurement::VP, arg_t> ) {
              if ( veloBegin == m_nodes.end() ) veloBegin = it;
              veloEnd = it;
            } else if constexpr ( std::is_base_of_v<LHCb::Measurement::TT, arg_t> ||
                                  std::is_base_of_v<LHCb::Measurement::UTLite, arg_t> ) {
              if ( ttBegin == m_nodes.end() ) ttBegin = it;
              ttEnd = it;
            } else if constexpr ( std::is_base_of_v<LHCb::Measurement::IT, arg_t> ||
                                  std::is_same_v<LHCb::Measurement::OT, arg_t> ||
                                  std::is_same_v<LHCb::Measurement::FT, arg_t> ) {
              if ( tBegin == m_nodes.end() ) tBegin = it;
              tEnd = it;
            } else if constexpr ( std::is_same_v<LHCb::Measurement::Muon, arg_t> ) {
              if ( muonBegin == m_nodes.end() ) muonBegin = it;
              muonEnd = it;
            };
          } );
        }

        const bool upstream  = m_nodes.front().node().z() > m_nodes.back().node().z();
        auto       chilambda = []( const bool& upstream, const decltype( muonBegin )& itBackward,
                             const decltype( muonBegin )& itForward ) {
          return upstream ? LHCb::ChiSquare( itForward->get<Op::Forward, Op::Chi2>(), itForward->m_ndof )
                          : LHCb::ChiSquare( itBackward->get<Op::Backward, Op::Chi2>(), itBackward->m_ndofBackward );
        };

        // Set all the ChiSquare objects in TrackFitResult
        fit.setChi2( LHCb::ChiSquare( chi2(), ndof() ) );

        if ( muonBegin != m_nodes.end() )
          fit.setChi2Muon( chilambda( upstream, muonBegin, muonEnd ) );
        else
          fit.setChi2Muon( LHCb::ChiSquare( 0.0, 0 ) );
        if ( tBegin != m_nodes.end() )
          fit.setChi2Downstream( chilambda( upstream, tBegin, tEnd ) );
        else
          fit.setChi2Downstream( fit.chi2Muon() );
        if ( veloBegin != m_nodes.end() )
          fit.setChi2Velo( chilambda( not upstream, veloBegin, veloEnd ) );
        else
          fit.setChi2Velo( LHCb::ChiSquare( 0.0, 0 ) );
        if ( ttBegin != m_nodes.end() )
          fit.setChi2Upstream( chilambda( not upstream, ttBegin, ttEnd ) );
        else
          fit.setChi2Upstream( fit.chi2Velo() );
        if ( tBegin != m_nodes.end() )
          fit.setChi2Long( chilambda( not upstream, tBegin, tEnd ) );
        else
          fit.setChi2Long( fit.chi2Upstream() );
      }
    };

    inline std::ostream& operator<<( std::ostream& s, const Node& n ) {
      const auto& node = n.node();

      s << "Node "
        << "#" << n.m_index << "\n";
      if ( n.m_forwardState.m_basePointer != nullptr ) {
        s << " Forward state: " << n.get<Op::Forward, Op::StateVector>()
          << ", covariance: " << n.get<Op::Forward, Op::Covariance>() << ", chi2: " << n.get<Op::Forward, Op::Chi2>()
          << "\n";
      }
      if ( n.m_backwardState.m_basePointer != nullptr ) {
        s << " Backward state: " << n.get<Op::Backward, Op::StateVector>()
          << ", covariance: " << n.get<Op::Backward, Op::Covariance>() << ", chi2: " << n.get<Op::Backward, Op::Chi2>()
          << "\n";
      }
      if ( n.m_smoothState.m_basePointer != nullptr ) {
        s << " Smoothed state: " << n.get<Op::Smooth, Op::StateVector>()
          << ", covariance: " << n.get<Op::Smooth, Op::Covariance>() << "\n"
          << " Total chi2: " << n.get<Op::Forward, Op::Chi2>() << ", " << n.get<Op::Backward, Op::Chi2>() << "\n";
      }
      s << " hasInfoUpstream: -, -\n";

      s << " Node"
        << " type " << node.type();
      if ( node.hasMeasurement() ) {
        s << " measurement of type " << node.measurement().type() << " with LHCbID "
          << node.measurement().lhcbID().channelID();
      }
      s << " at z " << node.z() << " with chi2 " << n.smoothChi2() << "\n"
        << " transport matrix ";
      if ( n.get<Op::Forward, Op::TransportMatrix>().m_basePointer != nullptr ) {
        s << n.get<Op::Forward, Op::TransportMatrix>();
      }
      s << "\n inverse transport matrix ";
      if ( n.get<Op::Backward, Op::TransportMatrix>().m_basePointer != nullptr ) {
        s << n.get<Op::Backward, Op::TransportMatrix>();
      }
      if ( n.m_smoothState.m_basePointer != nullptr ) {
        s << "\n residual " << n.get<Op::Smooth, Op::Residual>() << " errResidual "
          << n.get<Op::Smooth, Op::ErrResidual>();
      }
      if ( n.m_nodeParameters.m_basePointer != nullptr ) {
        s << " projectionMatrix " << n.get<Op::NodeParameters, Op::ProjectionMatrix>() << " refVector "
          << n.get<Op::NodeParameters, Op::ReferenceVector>() << " refResidual "
          << n.get<Op::NodeParameters, Op::ReferenceResidual>() << " errMeasure "
          << n.get<Op::NodeParameters, Op::ErrMeasure>();
      }
      s << "\n noiseMatrix " << n.get<Op::NodeParameters, Op::NoiseMatrix>() << " transportVector "
        << n.get<Op::NodeParameters, Op::TransportVector>();

      return s;
    }

  } // namespace TrackVectorFit

} // namespace Tr

template <class T, unsigned long N>
inline std::ostream& operator<<( std::ostream& s, const std::array<T, N>& v ) {
  for ( unsigned i = 0; i < N; ++i ) {
    s << v[i];
    if ( i != N - 1 ) s << " ";
  }
  return s;
}
