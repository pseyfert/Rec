/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Include files

#include "Event/Measurement.h"
#include "Event/StateVector.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "OTDet/DeOTModule.h"
#include "TrajOTProjector.h"

class TrajOTCosmicsProjector : public TrajOTProjector {
public:
  /// Standard constructor
  using TrajOTProjector::TrajOTProjector;

  StatusCode initialize() override;

  using TrajOTProjector::project;

protected:
  TrackProjector::InternalProjectResult internal_project( const LHCb::StateVector& state,
                                                          const LHCb::Measurement& meas ) const override;

private:
  Gaudi::Property<double> m_tofReferenceZ{this, "TofReferenceZ", 12.8 * Gaudi::Units::m};
  Gaudi::Property<bool>   m_fitEventT0{this, "FitEventT0", true};
  Gaudi::Property<bool>   m_useConstantDriftVelocity{this, "UseConstantDriftVelocity", true};
};

//----------------------------------------------------------------------------

DECLARE_COMPONENT( TrajOTCosmicsProjector )

StatusCode TrajOTCosmicsProjector::initialize() {
  StatusCode sc = TrajOTProjector::initialize();
  info() << "Fit event t0 = " << m_fitEventT0.value() << endmsg;
  info() << "Use const v_drift = " << m_useConstantDriftVelocity.value() << endmsg;
  return sc;
}

//-----------------------------------------------------------------------------
/// Project a state onto a measurement
//-----------------------------------------------------------------------------
TrackProjector::InternalProjectResult
TrajOTCosmicsProjector::internal_project( const LHCb::StateVector& statevector,
                                          const LHCb::Measurement& genMeas ) const {
  // (Decided to catch this particlar project call such that I do no
  // need to introduce new virtual function hooks in default
  // projector.)

  // check that we ar dealing with an OTMeasurement
  const LHCb::Measurement::OT* meas = genMeas.getIf<LHCb::Measurement::OT>();
  if ( !meas ) throw StatusCode::FAILURE;

  // compute the tof correction relative to a reference z.
  double L0 = ( genMeas.z() - m_tofReferenceZ ) *
              std::sqrt( 1 + statevector.tx() * statevector.tx() + statevector.ty() * statevector.ty() );
  bool   forward = statevector.ty() < 0;
  double tof     = ( forward ? 1 : -1 ) * L0 / Gaudi::Units::c_light;
  // should we subtract a reference time-of-flight?
  //   tof -= fabs( m_tofReferenceZ - meas.z() ) / Gaudi::Units::c_light ;
  // add this to the measurement, including the phase. we'll use the
  // qop-entry of the statevector to store the phase.
  double eventt0 = statevector.parameters()[4];
  // ugly const-cast to update the measurement's time-of-flight
  const_cast<LHCb::Measurement::OT*>( meas )->deltaTimeOfFlight = (float)( tof + eventt0 );
  // need to test ambiguity before calling projector!-(
  // call the standard projector (which uses the time-of-flight)
  auto result       = TrajOTProjector::internal_project( statevector, genMeas );
  bool usedrifttime = ( meas->driftTimeStrategy == LHCb::Measurement::OT::DriftTimeStrategy::FitTime ||
                        meas->driftTimeStrategy == LHCb::Measurement::OT::DriftTimeStrategy::FitDistance );

  // update the projection matrix with the derivative to event-t0.
  if ( usedrifttime && m_fitEventT0 ) {
    if ( fitDriftTime() ) {
      result.H( 0, 4 ) = 1;
    } else {
      double vdrift = meas->module->rtRelation().drdt();
      if ( !m_useConstantDriftVelocity ) {
        // get the drift velocity. for the linearization it is best to
        // have something as closest to the truth as possible. that's not
        // the drift time. so, instead, we compute the inverse:
        double dtdr = meas->module->rtRelation().dtdr( std::abs( result.doca ) );
        vdrift      = 1 / dtdr;
      }
      // now we just need to get the sign right. I think that H is MINUS
      // the derivative of the residual in Fruhwirth's languague
      result.H( 0, 4 ) = meas->ambiguity * vdrift;
    }
  }

  return result;
}
