/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKEXTRAPOLATORS_DETAILEDMATERIALLOCATOR_H
#define TRACKEXTRAPOLATORS_DETAILEDMATERIALLOCATOR_H

// Include files
// -------------
#include "DetDesc/ITransportSvc.h"
#include "GaudiKernel/ServiceHandle.h"
#include "MaterialLocatorBase.h"

struct IGeometryInfo;

/** @class DetailedMaterialLocator DetailedMaterialLocator.h
 *
 * Implementation of a IMaterialLocator that uses the TransportSvc for
 * finding materials on a trajectory.
 *
 *  @author Wouter Hulsbergen
 *  @date   21/05/2007
 */

class DetailedMaterialLocator : public MaterialLocatorBase {
public:
  /// Constructor
  using MaterialLocatorBase::MaterialLocatorBase;

  /// intialize
  StatusCode initialize() override;

  using MaterialLocatorBase::intersect;
  using MaterialLocatorBase::intersect_r;

  /// Intersect a line with volumes in the geometry
  size_t intersect( const Gaudi::XYZPoint& p, const Gaudi::XYZVector& v,
                    ILVolume::Intersections& intersepts ) const override {
    return intersect_r( p, v, intersepts, m_accelCache );
  }

  /// Intersect a line with volumes in the geometry
  size_t intersect_r( const Gaudi::XYZPoint& p, const Gaudi::XYZVector& v, ILVolume::Intersections& intersepts,
                      std::any& accelCache ) const override;

private:
  Gaudi::Property<double>      m_minRadThickness{this, "MinRadThickness", 1e-4};       ///< minimum radiation thickness
  Gaudi::Property<std::string> m_geometrypath{this, "Geometry", "/dd/Structure/LHCb"}; ///< name of the geometry
  IGeometryInfo*               m_geometry = nullptr;
};

#endif // TRACKEXTRAPOLATORS_DETAILEDMATERIALLOCATOR_H
