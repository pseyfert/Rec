###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TrackKernel
################################################################################
gaudi_subdir(TrackKernel v3r1)

gaudi_depends_on_subdirs(Det/DetDesc
                         Tr/TrackFitEvent
                         Event/TrackEvent
                         Kernel/LHCbKernel
                         Kernel/LHCbMath)

find_package(Boost)
find_package(ROOT)
find_package(GSL)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_unit_test(test_traj tests/src/test_traj.cpp
                    LINK_LIBRARIES TrackKernel
                    TYPE Boost)

gaudi_add_library(TrackKernel
                  src/*.cpp
                  INCLUDE_DIRS GSL Boost
                  PUBLIC_HEADERS TrackKernel
                  LINK_LIBRARIES GSL Boost TrackEvent TrackFitEvent LHCbKernel LHCbMathLib)

gaudi_add_dictionary(TrackKernel
                     src/TrackKernelDict.h
                     src/TrackKernelDict.xml
                     INCLUDE_DIRS GSL Boost
                     LINK_LIBRARIES GSL Boost TrackEvent TrackFitEvent  LHCbKernel LHCbMathLib TrackKernel
                     OPTIONS "-U__MINGW32__")

