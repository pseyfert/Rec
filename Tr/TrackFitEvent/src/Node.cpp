/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "Event/Node.h"
#include "Event/Measurement.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Node
//
// 2006-06-10 : M. Needham
//-----------------------------------------------------------------------------
LHCb::Node::Node( const LHCb::Measurement* meas )
    : m_type( Type::HitOnTrack )
    , m_refIsSet( false )
    , m_measurement( meas )
    , m_residual( 0.0 )
    , m_errResidual( 0.0 )
    , m_errMeasure( 0.0 )
    , m_projectionMatrix() {
  m_refVector.setZ( meas->z() );
}

LHCb::Node* LHCb::Node::clone() const { return new LHCb::Node( *this ); }

double LHCb::Node::chi2() const {
  double res = residual();
  double err = errResidual();
  return res * res / ( err * err );
}
