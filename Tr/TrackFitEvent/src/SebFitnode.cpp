/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "Event/SebFitNode.h"
#include "Event/KalmanFitResult.h"
#include "LHCbMath/Similarity.h"

using namespace Gaudi;
using namespace Gaudi::Math;
using namespace LHCb;
/** @file SebFitNode.cpp
 *
 *  This File contains the implementation of the SebFitNode.
 *  A SebFitNode is a basket of objects at a given z position.
 *  The information inside the SebFitNode has to be sufficient
 *  to allow smoothing and refitting.
 */

namespace {

  void transportcovariance( const Gaudi::TrackMatrix& F, const Gaudi::TrackSymMatrix& origin,
                            Gaudi::TrackSymMatrix& target ) {
    bool isLine = F( 0, 4 ) == 0;
    if ( LIKELY( !isLine ) ) {

      // The temporary is actually important here. SMatrix is not
      // computing A*B*C very optimally.
      // static ROOT::Math::SMatrix<double, 5,5> FC ;
      // FC = F*origin ;
      // ROOT::Math::AssignSym::Evaluate(target,FC*Transpose(F)) ;

      // use vectorized similarity transform
      LHCb::Math::Similarity( F, origin, target );

    } else {

      target = origin;
      target( 0, 0 ) += 2 * origin( 2, 0 ) * F( 0, 2 ) + origin( 2, 2 ) * F( 0, 2 ) * F( 0, 2 );
      target( 2, 0 ) += origin( 2, 2 ) * F( 0, 2 );
      target( 1, 1 ) += 2 * origin( 3, 1 ) * F( 1, 3 ) + origin( 3, 3 ) * F( 1, 3 ) * F( 1, 3 );
      target( 3, 1 ) += origin( 3, 3 ) * F( 1, 3 );
      target( 1, 0 ) +=
          origin( 2, 1 ) * F( 0, 2 ) + origin( 3, 0 ) * F( 1, 3 ) + origin( 3, 2 ) * F( 0, 2 ) * F( 1, 3 );
      target( 2, 1 ) += origin( 3, 2 ) * F( 1, 3 );
      target( 3, 0 ) += origin( 3, 2 ) * F( 0, 2 );
    }
  }

  void inverttransport( const Gaudi::TrackMatrix& F, Gaudi::TrackMatrix& Finv ) {
    // it would save time if qw assume that Finv is either diagonal or filled with 0?
    Finv        = F;
    bool isLine = F( 0, 4 ) == 0;
    if ( isLine ) {
      Finv( 0, 2 ) = -F( 0, 2 );
      Finv( 1, 3 ) = -F( 1, 3 );
    } else {
      // Finv(0,0) = Finv(1,1) = Finv(4,4) = 1 ;
      // write
      //      ( 1  0 |  S00 S01 | U0 )
      //      ( 0  1 |  S10 S01 | U1 )
      // F =  ( 0  0 |  T00 T01 | V0 )
      //      ( 0  0 |  T10 T11 | V1 )
      //      ( 0  0 |   0   0  | 1  )
      // then we have
      // Tinv = T^{-1}
      double det   = F( 2, 2 ) * F( 3, 3 ) - F( 2, 3 ) * F( 3, 2 );
      Finv( 2, 2 ) = F( 3, 3 ) / det;
      Finv( 3, 3 ) = F( 2, 2 ) / det;
      Finv( 2, 3 ) = -F( 2, 3 ) / det;
      Finv( 3, 2 ) = -F( 3, 2 ) / det;
      // Vinv = - T^-1 * V
      Finv( 2, 4 ) = -Finv( 2, 2 ) * F( 2, 4 ) - Finv( 2, 3 ) * F( 3, 4 );
      Finv( 3, 4 ) = -Finv( 3, 2 ) * F( 2, 4 ) - Finv( 3, 3 ) * F( 3, 4 );
      // Uinv = S * T^-1 * V - U = - S * Vinv - U
      Finv( 0, 4 ) = -F( 0, 4 ) - F( 0, 2 ) * Finv( 2, 4 ) - F( 0, 3 ) * Finv( 3, 4 );
      Finv( 1, 4 ) = -F( 1, 4 ) - F( 1, 2 ) * Finv( 2, 4 ) - F( 1, 3 ) * Finv( 3, 4 );
      // Sinv  = - S * T^{-1}
      Finv( 0, 2 ) = -F( 0, 2 ) * Finv( 2, 2 ) - F( 0, 3 ) * Finv( 3, 2 );
      Finv( 0, 3 ) = -F( 0, 2 ) * Finv( 2, 3 ) - F( 0, 3 ) * Finv( 3, 3 );
      Finv( 1, 2 ) = -F( 1, 2 ) * Finv( 2, 2 ) - F( 1, 3 ) * Finv( 3, 2 );
      Finv( 1, 3 ) = -F( 1, 2 ) * Finv( 2, 3 ) - F( 1, 3 ) * Finv( 3, 3 );
    }
  }
} // namespace

namespace LHCb {

  /// Constructor from a z position
  ///  eg. SebFitNode(StateParameters::ZEndVelo, State::EndVelo)
  SebFitNode::SebFitNode( double zPos, LHCb::State::Location location )
      : m_type( Reference ), m_state( location ), m_measurement( 0 ) {
    m_refVector.setZ( zPos );
    m_predictedState[Forward].setLocation( location );
    m_predictedState[Backward].setLocation( location );
  }

  /// Constructor from a Measurement
  SebFitNode::SebFitNode( Measurement& aMeas ) : m_type( HitOnTrack ), m_measurement( &aMeas ) {
    m_refVector.setZ( aMeas.z() );
  }

  void SebFitNode::setTransportMatrix( const Gaudi::TrackMatrix& transportMatrix ) {
    m_transportMatrix = transportMatrix;
    // invert the transport matrix. We could save some time by doing this on demand only.
    inverttransport( m_transportMatrix, m_invertTransportMatrix );
  }

  //=========================================================================
  // Predict the state to this node
  //=========================================================================
  void SebFitNode::computePredictedStateSeb( int direction, SebFitNode* prevNode, bool hasInfoUpstream ) {
    // get the filtered state from the previous node. if there wasn't
    // any, we will want to copy the reference vector and leave the
    // covariance the way it is
    m_predictedState[direction].setZ( z() );
    TrackVector&    stateVec = m_predictedState[direction].stateVector();
    TrackSymMatrix& stateCov = m_predictedState[direction].covariance();
    // start by copying the refvector. that's always the starting point
    stateVec = refVector().parameters();
    if ( prevNode ) {
      const LHCb::State& previousState = prevNode->m_filteredState[direction];
      if ( !hasInfoUpstream ) {
        // just _copy_ the covariance matrix from upstream, assuming
        // that this is the seed matrix. (that saves us from copying
        // the seed matrix to every state from the start.
        stateCov = previousState.covariance();
        // new: start the backward filter from the forward filter
        if ( direction == Backward ) { stateVec = m_filteredState[Forward].stateVector(); }
      } else {
        if ( direction == Forward ) {
          const TrackMatrix& F = m_transportMatrix;
          stateVec             = F * previousState.stateVector() + m_transportVector;
          transportcovariance( F, previousState.covariance(), stateCov );
          stateCov += m_noiseMatrix;
        } else {
          const TrackMatrix& invF = prevNode->m_invertTransportMatrix;
          TrackSymMatrix     tempCov;
          tempCov  = previousState.covariance() + prevNode->m_noiseMatrix;
          stateVec = invF * ( previousState.stateVector() - prevNode->m_transportVector );
          transportcovariance( invF, tempCov, stateCov );
        }
      }
    } else {
      if ( !( stateCov( 0, 0 ) > 0 ) ) {
        throw std::string( "Error in predict " ) + ( direction == Forward ? "forward" : "backward" ) +
            " function: seed covariance is not set!";
      }
    }

    if ( !( m_predictedState[direction].covariance()( 0, 0 ) > 0 ) ) {
      throw std::string( "Error in predict " ) + ( direction == Forward ? "forward" : "backward" ) +
          " function: something goes wrong in the prediction";
    }
  }

  //=========================================================================
  // Filter this node
  //=========================================================================
  void SebFitNode::computeFilteredStateSeb( int direction, SebFitNode* pn, int nTrackParam ) {
    // get a reference on the filtered state
    LHCb::State& state = m_filteredState[direction];
    // copy the predicted state
    state                  = m_predictedState[direction];
    m_totalChi2[direction] = pn ? pn->totalChi2( direction ) : LHCb::ChiSquare( 0, -nTrackParam );
    // apply the filter if needed
    if ( type() == HitOnTrack ) {
      const TrackProjectionMatrix& H = this->projectionMatrix();
      if ( !( std::abs( H( 0, 0 ) ) + std::abs( H( 0, 1 ) ) > 0 ) ) {
        throw std::string( "Error in filter " ) + ( direction == Forward ? "forward" : "backward" ) +
            " projection matrix is not set!";
      }
      double chi2 = LHCb::Math::Filter( state.stateVector(), state.covariance(), refVector().parameters(),
                                        projectionMatrix(), m_refResidual, errMeasure2() );
      // set the chisquare contribution
      m_totalChi2[direction] += LHCb::ChiSquare( chi2, 1 );
    } else {
      m_totalChi2[direction] += LHCb::ChiSquare();
    }
    if ( !( state.covariance()( 0, 0 ) > 0 && state.covariance()( 1, 1 ) > 0 ) ) {
      throw std::string( "Error in filter " ) + ( direction == Forward ? "forward" : "backward" ) +
          " something goes wrong in the filtering";
    }
  }

  //=========================================================================
  // Bi-directional smoother
  //=========================================================================
  void SebFitNode::computeBiSmoothedStateSeb( bool hasInfoUpstreamForward, bool hasInfoUpstreamBackward ) {
    LHCb::State& state = m_state;
    if ( !hasInfoUpstreamForward ) {
      // last node in backward direction
      state = m_filteredState[Backward];
    } else if ( !hasInfoUpstreamBackward ) {
      // last node in forward direction
      state = m_filteredState[Forward];
    } else {
      // Take the weighted average of two states. We now need to
      // choose for which one we take the filtered state. AFAIU the
      // weighted average behaves better if the weights are more
      // equal. So, we filter the 'worst' prediction. In the end, it
      // all doesn't seem to make much difference.
      const LHCb::State *s1, *s2;
      if ( m_predictedState[Backward].covariance()( 0, 0 ) > m_predictedState[Forward].covariance()( 0, 0 ) ) {
        s1 = &( m_filteredState[Backward] );
        s2 = &( m_predictedState[Forward] );
      } else {
        s1 = &( m_filteredState[Forward] );
        s2 = &( m_predictedState[Backward] );
      }
      state.setZ( z() ); // the disadvantage of having this information more than once
      bool success = LHCb::Math::Average( s1->stateVector(), s1->covariance(), s2->stateVector(), s2->covariance(),
                                          state.stateVector(), state.covariance() );
      if ( !success ) { throw std::string( "Error in smooth function: error in matrix inversion" ); }
    }
    if ( !isPositiveDiagonal( state.covariance() ) ) {
      throw std::string( "Error in smooth function: non positive diagonal element in coveriance matrix" );
    }
    updateResidual( state );

    // copy back state's vector into the Node's refVector. This was previouly done in a
    // separate step and extra iteration at the MasterFitter level. In the new HLT1Fitter
    // we count on this to be done here
    setRefVector( state.stateVector() );
  }

  void SebFitNode::updateResidual( const LHCb::State& smoothedState ) {
    Gaudi::Math::ValueWithError res;
    double                      r( 0 ), R( 0 );
    if ( hasMeasurement() ) {
      // We should put this inside the This->
      const TrackProjectionMatrix& H    = this->projectionMatrix();
      const TrackVector&           refX = this->refVector().parameters();
      r                                 = m_refResidual + ( H * ( refX - smoothedState.stateVector() ) )( 0 );
      double V                          = errMeasure2();
      double HCH                        = LHCb::Math::Similarity( H, smoothedState.covariance() );
      double sign                       = type() == LHCb::SebFitNode::HitOnTrack ? -1 : 1;
      R                                 = V + sign * HCH;
      if ( R <= 0 ) { throw std::string( "Error in compute residual: non positive variance" ); }
    }
    m_residual    = r;
    m_errResidual = std::sqrt( R );
  }

  inline bool SebFitNode::isPositiveDiagonal( const Gaudi::TrackSymMatrix& mat ) const {
    unsigned int i = 0u;
    for ( ; i < Gaudi::TrackSymMatrix::kRows && mat( i, i ) > 0.0; ++i ) {}
    return i == Gaudi::TrackSymMatrix::kRows;
  }

} // namespace LHCb
