/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PVOFFLINETOOL_H
#define PVOFFLINETOOL_H 1
// Include files:
// from STL
#include <optional>
#include <string>
#include <vector>
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
// Interfaces
#include "IPVFitter.h"
#include "IPVSeeding.h"
#include "PVOfflineRecalculate.h"
#include "TrackInterfaces/IPVOfflineTool.h"
// Track info
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "GaudiAlg/ISequencerTimerTool.h"

class PVOfflineTool : public extends<GaudiTool, IPVOfflineTool> {
public:
  // Standard constructor
  using extends::extends;
  // Destructor
  StatusCode initialize() override;
  // PV fitting

  StatusCode reDoSinglePV( const LHCb::Tracks& inputTracks, const Gaudi::XYZPoint xyzseed,
                           std::vector<const LHCb::Track*>& tracks2exclude, LHCb::RecVertex& outvtx ) const override;

  StatusCode reDoMultiPV( const LHCb::Tracks& inputTracks, const LHCb::RecVertex& invtx,
                          std::vector<const LHCb::Track*>& tracks2exclude, LHCb::RecVertex& outvtx ) const override;

  StatusCode reconstructSinglePVFromTracks( const Gaudi::XYZPoint                  xyzseed,
                                            const std::vector<const LHCb::Track*>& tracks2use,
                                            LHCb::RecVertex&                       outvtx ) const override;

  StatusCode reconstructMultiPVFromTracks( std::vector<const LHCb::Track*>& tracks2use,
                                           std::vector<LHCb::RecVertex>&    outvtxVec ) const override;

  StatusCode reconstructMultiPV( const std::vector<LHCb::Track>& inputTracks,
                                 std::vector<LHCb::RecVertex>&   outvtxVec ) const override;

  StatusCode reconstructSinglePV( const LHCb::Tracks& inputTracks, const Gaudi::XYZPoint xyzseed,
                                  LHCb::RecVertex& outvtx ) const override;

  StatusCode removeTracksAndRecalculatePV( const LHCb::RecVertex*                 pvin,
                                           const std::vector<const LHCb::Track*>& tracks2remove,
                                           LHCb::RecVertex&                       vtx ) const override;

private:
  Gaudi::Property<bool> m_requireVelo{this, "RequireVelo", true, "Option to use tracks with VELO segment only"};
  Gaudi::Property<bool> m_saveSeedsAsPV{this, "SaveSeedsAsPV", false, "Save seeds as PVs (for monitoring"};

  // Tools
  ToolHandle<IPVFitter>            m_pvfit{this, "PVFitterName", "LSAdaptPVFitter"};    // PV fitting tool
  ToolHandle<IPVSeeding>           m_pvSeedTool{this, "PVSeedingName", "PVSeed3DTool"}; // Seeding tool
  ToolHandle<PVOfflineRecalculate> m_pvRecalc{this, "PVOfflineRecalculate"
                                                    "PVOfflineRecalculate"};

  Gaudi::Property<double> m_pvsChi2Separation{this, "PVsChi2Separation", 25.};
  Gaudi::Property<double> m_pvsChi2SeparationLowMult{this, "PVsChi2SeparationLowMult", 91.};

  Gaudi::Property<bool>         m_useBeamSpotRCut{this, "UseBeamSpotRCut", false};
  Gaudi::Property<double>       m_beamSpotRCut{this, "BeamSpotRCut", 0.2};
  Gaudi::Property<double>       m_beamSpotRCutHMC{this, "BeamSpotRHighMultiplicityCut", 0.4};
  Gaudi::Property<unsigned int> m_beamSpotRMT{this, "BeamSpotRMultiplicityTreshold", 10};
  double                        m_beamSpotX = 0;
  double                        m_beamSpotY = 0;
  Gaudi::Property<double>       m_resolverBound{this, "ResolverBound", 5 * Gaudi::Units::mm};
  bool                          m_veloClosed = false;

  // Member functions
  LHCb::RecVertex* matchVtxByTracks( const LHCb::RecVertex& invtx, std::vector<LHCb::RecVertex>& outvtxvec ) const;

  std::vector<const LHCb::Track*> readTracks( const LHCb::Tracks& inputTracks ) const;

  void removeTracksByLHCbIDs( std::vector<const LHCb::Track*>&       tracks,
                              const std::vector<const LHCb::Track*>& tracks2remove ) const;

  void removeTracks( std::vector<const LHCb::Track*>&       tracks,
                     const std::vector<const LHCb::Track*>& tracks2remove ) const;
  void removeTracks( std::vector<const LHCb::Track*>& tracks, const SmartRefVector<LHCb::Track>& tracks2remove ) const;

  void removeTracksUsedByVertex( std::vector<const LHCb::Track*>& tracks, LHCb::RecVertex& rvtx ) const;

  StatusCode UpdateBeamSpot();

  // timing
  Gaudi::Property<bool>           m_doTiming{this, "TimingMeasurement", false};
  ToolHandle<ISequencerTimerTool> m_timerTool{"SequencerTimerTool/Timer", this};
  std::array<int, 3>              m_timer;

  enum class timers_t { Total = 0, Seeding, Fitting };
  class TimerGuard {
    ISequencerTimerTool* m_tool;
    int                  m_timer;

  public:
    TimerGuard( const ISequencerTimerTool* t, int i ) : m_tool( const_cast<ISequencerTimerTool*>( t ) ), m_timer( i ) {
      m_tool->start( m_timer );
    }
    ~TimerGuard() { m_tool->stop( m_timer ); }
  };
  std::optional<TimerGuard> make_timeguard( timers_t type ) const {
    if ( !m_doTiming ) return {};
    return TimerGuard{m_timerTool.get(), m_timer[static_cast<int>( type )]};
  }
  // trivial accessor to minimum allowed chi2
  double minAllowedChi2( const LHCb::RecVertex& rvtx ) const {
    return ( rvtx.tracks().size() < 7 ? std::max( m_pvsChi2Separation, m_pvsChi2SeparationLowMult )
                                      : m_pvsChi2Separation );
  }
};
#endif // PVOFFLINETOOL_H
