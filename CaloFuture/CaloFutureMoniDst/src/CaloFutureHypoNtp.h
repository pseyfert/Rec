/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREHYPONTP_H
#define CALOFUTUREHYPONTP_H 1

#include "CaloFutureInterfaces/ICaloFuture2MCTool.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "Event/CaloHypo.h"
#include "Event/L0DUReport.h"
#include "Event/ODIN.h"
#include "Event/RecVertex.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IEventTimeDecoder.h"

// List of Consumers dependencies
namespace {
  using ODIN     = LHCb::ODIN;
  using L0       = LHCb::L0DUReport;
  using Hypos    = LHCb::CaloHypo::Container;
  using Tracks   = LHCb::Tracks;
  using Vertices = LHCb::RecVertices;
} // namespace

// ============================================================================

class CaloFutureHypoNtp final
    : public Gaudi::Functional::Consumer<void( const ODIN&, const L0&, const Hypos&, const Tracks&, const Vertices& ),
                                         Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
public:
  CaloFutureHypoNtp( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void       operator()( const ODIN&, const L0&, const Hypos&, const Tracks&, const Vertices& ) const override;

private:
  ToolHandle<ICaloFuture2MCTool>                     m_2MC         = {"CaloFuture2MCTool", this};
  ToolHandle<IFutureCounterLevel>                    m_counterStat = {"FutureCounterLevel"};
  ToolHandle<LHCb::Calo::Interfaces::IHypoEstimator> m_estimator   = {"CaloFutureHypoEstimator", this};

  Gaudi::Property<bool> m_extrapol{this, "Extrapolation", true};
  Gaudi::Property<bool> m_seed{this, "AddSeed", false};
  Gaudi::Property<bool> m_neig{this, "AddNeighbors", false};

  Gaudi::Property<std::pair<double, double>> m_et{this, "RangePt", {100., 15000.}};
  Gaudi::Property<std::pair<double, double>> m_e{this, "RangeE", {0., 5000000}};
  Gaudi::Property<std::pair<double, double>> m_spdM{this, "RangeSpdM", {0., 5000000.}};
  Gaudi::Property<std::pair<double, double>> m_prsE{this, "RangePrsE", {0., 9999.}};

  Gaudi::Property<std::vector<std::string>> m_hypos{this, "Hypos", {"Electrons", "Photons", "MergedPi0s"}};

  Gaudi::Property<bool> m_tupling{this, "Tupling", true};
  Gaudi::Property<bool> m_checker{this, "CheckerMode", false};
  Gaudi::Property<bool> m_print{this, "Printout", false};
  Gaudi::Property<bool> m_stat{this, "Statistics", true};
  Gaudi::Property<int>  m_mcID{this, "MCID", -99999999};
};
#endif // CALOFUTUREHYPONTP_H
