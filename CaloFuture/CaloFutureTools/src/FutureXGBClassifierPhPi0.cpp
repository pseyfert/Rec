/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "FutureXGBClassifierPhPi0.h"
#include <stdexcept>

FutureXGBClassifierPhPi0::FutureXGBClassifierPhPi0() {
  XGDMatrixCreateFromMat( m_predictionsCache.data(), 1, m_predictionsCache.size(), 0.5, &m_cache_matrix );
}

FutureXGBClassifierPhPi0::FutureXGBClassifierPhPi0( const std::string& path ) {
  // std::cout<<"PATH str: "<< path << std::endl;
  XGDMatrixCreateFromMat( &m_predictionsCache.at( 0 ), 1, m_predictionsCache.size(), 0.5, &m_cache_matrix );
  XGBoosterCreate( &m_cache_matrix, 1, &m_booster );
  int err_code = XGBoosterLoadModel( m_booster, path.c_str() );
  if ( err_code == -1 ) throw std::runtime_error( "Failed to load XGBModel from " + path );
}

double FutureXGBClassifierPhPi0::getClassifierValues( LHCb::span<const double> featureValues ) const {
  // currently XGBoost only supports float
  std::vector<float> features_f( featureValues.begin(), featureValues.end() );

  auto lock = std::lock_guard{m_mut};
  // fill the feature vector into a XGBoost DMatrix
  XGDMatrixCreateFromMat( features_f.data(), 1, features_f.size(), 0, &m_feature_matrix );

  // XGBoost returns the predictions into a arrayish object and return
  // its size
  unsigned long predictions_length = 0;
  const float*  predictions        = nullptr;
  XGBoosterPredict( m_booster, m_feature_matrix, 0, 0, &predictions_length, &predictions );
  assert( predictions != nullptr );
  assert( predictions_length > 0 );
  return predictions[0];
}
