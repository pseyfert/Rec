/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/TrackDefaultParticles.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"

/** @class CaloFutureElectron CaloFutureElectron.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2006-11-30
 */
class CaloFutureElectron final : public extends<GaudiTool, LHCb::Calo::Interfaces::IElectron> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;

  bool set( LHCb::ProtoParticle const& proto, std::string const& det, CaloPlane::Plane plane, double delta ) override;

  LHCb::State           caloState() const override { return m_state; }
  LHCb::State           closestState() const override;
  LHCb::CaloHypo const* electron() const override { return m_electron; }
  LHCb::CaloHypo const* bremstrahlung() const override { return m_bremstrahlung; }
  LHCb::CaloMomentum    bremCaloFutureMomentum() const override;
  double                eOverP() const override { return m_status ? m_electron->e() / m_track->p() : 0.; }
  double                caloTrajectoryL( CaloPlane::Plane plane ) const override;

private:
  LHCb::State caloState( CaloPlane::Plane plane, double delta = 0 ) const;

  // configuration-dependent state
  ITrackExtrapolator*          m_extrapolator = nullptr;
  Gaudi::Property<std::string> m_extrapolatorType{this, "ExtrapolatorType", "TrackRungeKuttaExtrapolator"};
  Gaudi::Property<float>       m_tolerance{this, "Tolerance", 0.01};

  // call-dependent state -- set by 'set', and afterwards const -- so could be returned as data from 'set'...
  bool                  m_status        = false;
  const LHCb::Track*    m_track         = nullptr;
  const LHCb::CaloHypo* m_electron      = nullptr;
  const LHCb::CaloHypo* m_bremstrahlung = nullptr;
  LHCb::State           m_state;
  DeCalorimeter*        m_calo = nullptr;
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureElectron )

namespace {
  // fix name clash with Track2Calo::closestState
  decltype( auto ) closest_state( const LHCb::Track& t, const Gaudi::Plane3D& p ) { return closestState( t, p ); }
} // namespace

//=============================================================================
StatusCode CaloFutureElectron::initialize() {
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) return Error( "Failed to initialize", sc );
  m_extrapolator = tool<ITrackExtrapolator>( m_extrapolatorType, "Extrapolator", this );
  return StatusCode::SUCCESS;
}
//=============================================================================
bool CaloFutureElectron::set( LHCb::ProtoParticle const& proto, std::string const& det, CaloPlane::Plane plane,
                              double delta ) {
  m_electron      = nullptr;
  m_bremstrahlung = nullptr;
  m_calo          = nullptr;
  m_state         = {};
  m_track         = proto.track();

  m_status = ( m_track != nullptr );
  if ( !m_status ) return false;

  for ( const LHCb::CaloHypo* hypo : proto.calo() ) {
    if ( !hypo ) continue;
    switch ( hypo->hypothesis() ) {
    case LHCb::CaloHypo::Hypothesis::EmCharged:
      m_electron = hypo;
      break;
    case LHCb::CaloHypo::Hypothesis::Photon:
      m_bremstrahlung = hypo;
      break;
    default:; // nothing;
    }
  }
  m_status = ( m_electron && m_electron->position() ); // Electron hypo is mandatory - brem. not
  if ( !m_status ) return false;

  m_calo  = getDet<DeCalorimeter>( det );
  m_state = caloState( plane, delta );
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << " CaloFutureElectron setting [" << *m_track << "," << det << "] status : " << m_status << endmsg;
  return m_status;
}
//=============================================================================
LHCb::CaloMomentum CaloFutureElectron::bremCaloFutureMomentum() const {
  if ( !m_status || !m_bremstrahlung ) return {};
  Gaudi::XYZPoint     point;
  Gaudi::SymMatrix3x3 matrix;
  m_track->position( point, matrix );
  return {m_bremstrahlung, point, matrix};
}

//=============================================================================
LHCb::State CaloFutureElectron::closestState() const {
  if ( !m_status ) return {};

  // get state on Front of Ecal
  LHCb::State calostate = caloState( CaloPlane::Front );
  if ( calostate.z() == 0 ) return {};

  // get frontPlane
  ROOT::Math::Plane3D frontPlane = m_calo->plane( CaloPlane::Front );

  // get hypo position
  const auto& params = m_electron->position()->parameters();
  double      x      = params( LHCb::CaloPosition::Index::X );
  double      y      = params( LHCb::CaloPosition::Index::Y );

  // Define calo line (from transversal barycenter) and track line in Ecal
  Gaudi::XYZVector  normal = frontPlane.Normal();
  double            zEcal  = ( -normal.X() * x - normal.Y() * y - frontPlane.HesseDistance() ) / normal.Z(); // tilt
  Gaudi::XYZPoint   point{x, y, zEcal};
  Gaudi::Math::Line cLine{point, frontPlane.Normal()};
  Gaudi::Math::Line tLine{calostate.position(), calostate.slopes()};

  // Find points of closest distance between calo Line and track Line
  Gaudi::XYZPoint cP, tP;
  Gaudi::Math::closestPoints( cLine, tLine, cP, tP );

  // default to electron
  auto pid = LHCb::Tr::PID::Electron();
  // propagate the state the new Z of closest distance
  StatusCode sc = m_extrapolator->propagate( calostate, tP.Z(), pid );

  if ( sc.isFailure() ) return {};
  return calostate;
}
//=============================================================================
double CaloFutureElectron::caloTrajectoryL( CaloPlane::Plane refPlane ) const {
  LHCb::State         theState = closestState();
  LHCb::State         refState = caloState( refPlane );
  Gaudi::XYZVector    depth    = theState.position() - refState.position();
  ROOT::Math::Plane3D plane    = m_calo->plane( refPlane );
  double              dist     = plane.Distance( theState.position() ); // signed distance to refPlane
  return depth.R() * dist / fabs( dist );
}

//=============================================================================
LHCb::State CaloFutureElectron::caloState( CaloPlane::Plane plane, double delta ) const {
  if ( !m_status ) return {};

  // get caloPlane
  ROOT::Math::Plane3D refPlane = m_calo->plane( plane );
  // propagate state to refPlane
  const LHCb::Tr::PID pid       = LHCb::Tr::PID::Pion();
  LHCb::State         calostate = closest_state( *m_track, refPlane );
  StatusCode          sc        = m_extrapolator->propagate( calostate, refPlane, m_tolerance, pid );
  if ( sc.isFailure() ) return {};

  if ( 0. == delta ) return calostate;

  Gaudi::XYZVector dir( calostate.tx(), calostate.ty(), 1. );
  Gaudi::XYZPoint  point = calostate.position() + delta * dir / dir.R();
  // extrapolate to the new point
  sc = m_extrapolator->propagate( calostate, point.z(), pid );
  if ( sc.isFailure() ) return {};
  return calostate;
}
//=============================================================================
