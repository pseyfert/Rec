/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "InCaloFutureAcceptanceAlg.h"

// ============================================================================
/** @class InHcalFutureAcceptanceAlg InHcalFutureAcceptanceAlg.cpp
 *  the preconfigured instance of InCaloFutureAcceptanceAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================
namespace LHCb::Calo {
  struct InHcalAcceptanceAlg final : InAcceptanceAlg {
    /// Standard constructor
    InHcalAcceptanceAlg( const std::string& name, ISvcLocator* pSvc ) : InAcceptanceAlg( name, pSvc ) {
      using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;

      updateHandleLocation( *this, "Output", CaloFutureIdLocation( "InHcal" ) );

      setProperty( "Tool", "InHcalFutureAcceptance/InHcalFuture" ).ignore();
      // track types:
      setProperty( "AcceptedType",
                   Gaudi::Utils::toString<int>( LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                                LHCb::Track::Types::Ttrack ) )
          .ignore();
    }
  };
} // namespace LHCb::Calo
// ============================================================================

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::InHcalAcceptanceAlg, "InHcalFutureAcceptanceAlg" )

// ============================================================================
