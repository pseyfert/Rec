/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREENERGYFORTRACK_H
#define CALOFUTUREENERGYFORTRACK_H 1

// Include files
#include "CaloFutureTrackTool.h"
#include "CaloFutureUtils/CaloFutureNeighbours.h"
#include "Event/CaloDigit.h"
#include "Event/Track.h"
#include "ICaloFutureTrackIdEval.h"
#include <set>

// ============================================================================
/** @class CaloFutureEnergyForTrack
 *  tool which accumulates the energy for the given track
 *  along the track line
 *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
 *  @author Zhirui Xu zhirui.xu@cern.ch
 */

namespace LHCb::Calo {
  class EnergyForTrack : public extends<TrackTool, Interfaces::ITrackIdEval> {
  public:
    using extends::extends;
    StatusCode initialize() override;

    /** The main processing method
     *  It evaluated the Track ID estimators using the calorimeter information
     *  @param  track  pointer to the object to be processed
     *  @param  digits calo digits from TES
     *  @return the value of the estimator
     */
    std::optional<double> process( const Track& track, const CaloDigits& digits ) const override;

  private:
    /** collect the cellID-s along the line
     *  @param  line   (INPUT)  the line
     *  @return the container of cells
     */
    std::optional<CaloCellID::Set> collect( const Line& line ) const;

    /** collect the cellID-s along the path of the tracks
     *  @param  track  (INPUT)  the track
     *  @return the container of cells
     */
    std::optional<CaloCellID::Set> collect( const Track& track ) const;

    /** collect the fired digits along the path of the tracks
     *  @param  track  (INPUT)  the track
     *  @param  digits (INPUT)  the container of digits
     *  @return the fired hits along the track
     */
    std::optional<CaloDigit::Set> collect( const Track& track, const CaloDigits& digits ) const;

  private:
    State::Location             m_location = State::Location::ECalShowerMax;
    std::vector<Gaudi::Plane3D> m_planes;

    Gaudi::Property<unsigned short> m_morePlanes{this, "MorePlanes", 0, "number of extra planes to be added"};
    Gaudi::Property<unsigned short> m_addNeighbours{this, "AddNeighbours", 0,
                                                    "number of neighbouring cells to be added"};
  };
} // namespace LHCb::Calo
// ============================================================================
#endif // CALOFUTUREENERGYFORTRACK_H
