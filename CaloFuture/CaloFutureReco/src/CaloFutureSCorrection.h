/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_CALOFUTURESCORRECTION_H
#define CALOFUTURERECO_CALOFUTURESCORRECTION_H 1
// Include files
// Include files
#include "CaloFutureCorrectionBase.h"
#include "GaudiKernel/Counters.h"
#include "ICaloFutureHypoTool.h"
#include <string>

/** @class CaloFutureSCorrection CaloFutureSCorrection.h
 *
 *
 *  @author Deschamps Olivier
 *  @date   2003-03-10
 *
 *  Adam Szabelski
 *  date 2019-10-15
 */

class CaloFutureSCorrection : public extends<CaloFutureCorrectionBase, LHCb::Calo::Interfaces::IProcessHypos> {
public:
  typedef std::reference_wrapper<const LHCb::CaloFuture2Track::IClusTrTable::Range>
             const_ref_range_type; // this makes it possible to be used by std::optional for matching_tracks
  StatusCode correct( LHCb::span<LHCb::CaloHypo* const>   hypos,
                      std::optional<const_ref_range_type> matching_tracks = std::nullopt )
      const override; // default value needed in this suntax for calling process(hypo) without the second argument
  StatusCode process( LHCb::span<LHCb::CaloHypo* const> hypos ) const override { return correct( hypos ); }

public:
  StatusCode initialize() override;
  StatusCode finalize() override;

  CaloFutureSCorrection( const std::string& type, const std::string& name, const IInterface* parent );

private:
  struct SCorrInputParams {
    LHCb::CaloCellID cellID;
    Gaudi::XYZPoint  seedPos;
    double           x;
    double           y;
    double           z;
  };

  struct SCorrResults {
    double xCor;
    double yCor;
    double dXhy_dXcl;
    double dYhy_dYcl;
  };

  /// calculate corrected CaloHypo position depending on CaloCluster position
  struct SCorrResults calculateSCorrections( const struct SCorrInputParams& params ) const;

  void debugDerivativesCalculation( const struct SCorrInputParams& inParams,
                                    const struct SCorrResults&     outParams ) const;

  void updateCovariance( double dXhy_dXcl, double dYhy_dYcl, LHCb::CaloHypo* hypo ) const;
  void updatePosition( double xCor, double yCor, LHCb::CaloHypo* hypo ) const;
  void printDebugInfo( const LHCb::CaloHypo* hypo, const struct SCorrInputParams& params, double xBar, double yBar,
                       double xCor, double yCor ) const;

private:
  using IncCounter    = Gaudi::Accumulators::Counter<>;
  using SCounter      = Gaudi::Accumulators::StatCounter<float>;
  using MapOfCounters = std::map<std::string, SCounter>;

  mutable IncCounter m_counterSkipNegativeEnergyCorrection{this, "Skip negative energy correction"};
  mutable SCounter   m_counterDeltaX{this, "Delta(X)"};
  mutable SCounter   m_counterDeltaY{this, "Delta(Y)"};
};
// ============================================================================
#endif // CALOFUTURERECO_CALOFUTURESCORRECTION_H
