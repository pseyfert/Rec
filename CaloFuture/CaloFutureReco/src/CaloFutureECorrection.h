/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_CALOFUTUREECORRECTION_H
#define CALOFUTURERECO_CALOFUTUREECORRECTION_H 1
// Include files
#include "CaloFutureCorrectionBase.h"
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "GaudiKernel/Counters.h"
#include "ICaloFutureDigitFilterTool.h"
#include "ICaloFutureHypoTool.h"
#include <map>
#include <string>

/** @namespace CaloFutureECorrection_Local
 */

/** @class CaloFutureECorrection CaloFutureECorrection.h
 *
 *
 *  @author Deschamps Olivier

 *  @date   2003-03-10
 */
class CaloFutureECorrection : public extends<CaloFutureCorrectionBase, LHCb::Calo::Interfaces::IProcessHypos> {

public:
  typedef std::reference_wrapper<const LHCb::CaloFuture2Track::IClusTrTable::Range>
             const_ref_range_type; // this makes it possible to be used by std::optional for matching_tracks
  StatusCode correct( LHCb::span<LHCb::CaloHypo* const>   hypos,
                      std::optional<const_ref_range_type> matching_tracks = std::nullopt )
      const override; // default value needed in this suntax for calling process(hypo) without the second argument

  StatusCode process( LHCb::span<LHCb::CaloHypo* const> hypos ) const override { return correct( hypos ); }

public:
  StatusCode initialize() override;

  CaloFutureECorrection( const std::string& type, const std::string& name, const IInterface* parent );

private:
  ICaloFutureDigitFilterTool* m_pileup = nullptr;

  struct ECorrInputParams {
    LHCb::CaloCellID cellID;
    Gaudi::XYZPoint  seedPos;
    double           x;
    double           y;
    double           z;
    double           eEcal;
    double           dtheta;
    unsigned int     area;
  };

  class ECorrOutputParams {
  public:
    ECorrOutputParams() = default;

    double eCor = 0;
    // output Jacobian elements returned from calcECorrection() to process()
    double dEcor_dXcl = 0;
    double dEcor_dYcl = 0;
    double dEcor_dEcl = 0;

    // intermediate variables calculated by calcECorrection() needed for debug printout inside process()
    double alpha = 0;
    double Asx   = 0;
    double Asy   = 0;
    double aG    = 0;
    double aE    = 0;
    double aB    = 0;
    double aX    = 0;
    double aY    = 0;
    double gT    = 0;
  };

  struct ECorrOutputParams calcECorrection( const struct ECorrInputParams& params ) const;

  /// debugging necessary in case if any new corrections are added or their sequence is changed!
  void debugDerivativesCalculation( const struct ECorrInputParams&  inParams,
                                    const struct ECorrOutputParams& outParams ) const;

private:
  /**
   * Update the covariance matrix of the calo hypothesis
   * @param dEcor_dXcl jacobian element calculated for x
   * @param dEcor_dycl jacobian element calculated for y
   * @param dEcor_decl jacobian element calculated for energy
   * @param hypo hypothesis to be modified
   **/
  void updateCovariance( double dEcor_dXcl, double dEcor_dYcl, double dEcor_dEcl, LHCb::CaloHypo* hypo ) const;
  /**
   * Update the position of the calo hypothesis
   * @param Ecor corrected energy to be applied
   * @param hypo hypothesis to be modified
   **/
  void   updateEnergy( double eCor, LHCb::CaloHypo* hypo ) const;
  void   printDebugInfo( const LHCb::CaloHypo* hypo, const struct ECorrInputParams& params,
                         const struct ECorrOutputParams& results ) const;
  bool   isHypoValid( const LHCb::CaloHypo* hypo ) const;
  bool   isEnergyNegative( const LHCb::CaloHypo* hypo ) const;
  bool   isNotMainCluster( const LHCb::CaloCluster* MainCluster ) const;
  bool   seedCellNotExist( const LHCb::CaloCluster::Entries&                entries,
                           const LHCb::CaloCluster::Entries::const_iterator iseed ) const;
  bool   isNotSeed( const LHCb::CaloDigit* seed ) const;
  double computeDTheta( const LHCb::CaloHypo* hypo, const LHCb::Track* ctrack ) const;
  double shiftAs( unsigned int cellIDColOrRow, const int shift[3], const unsigned int& area ) const;

  using IncCounter = Gaudi::Accumulators::Counter<>;
  using SCounter   = Gaudi::Accumulators::StatCounter<float>;

  LHCb::Calo::Interfaces::IElectron* m_caloElectron = nullptr;

  mutable IncCounter m_counterSkippedNegativeEnergyCorrection{this, "Skip negative energy correction"};

  mutable SCounter m_counterPileupOffset{this, "Pileup offset"};
  mutable SCounter m_counterPileupSubstractedRatio{this, "Pileup subtracted ratio"};
  mutable SCounter m_counterPileupScale{this, "Pileup scale"};

  mutable IncCounter m_counterUnphysical{this, "Unphysical d(Ehypo)/d(Ecluster)"};

  mutable SCounter m_counterCorrectedEnergy{this, "Corrected energy"};
  mutable SCounter m_counterDeltaEnergy{this, "Delta(E)"};

  mutable IncCounter m_counterUnphysicalVariance{this, "Unphysical variance(Ehypo)"};

  static constexpr int          k_numOfCaloFutureAreas{4};
  mutable std::vector<SCounter> m_countersAlpha;
};
#endif // CALOFUTURERECO_CALOFUTUREECORRECTION_H
