/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef CALOFUTURERECO_CALOFUTUREMERGEDPI0_H
#define CALOFUTURERECO_CALOFUTUREMERGEDPI0_H 1
// ============================================================================
// Include files
// ============================================================================
// from STL
// ============================================================================
#include <string>
#include <vector>
// ============================================================================
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/CaloCluster.h"
#include "FutureSubClusterSelectorTool.h"
#include "GaudiAlg/Transformer.h"
#include "ICaloFutureClusterTool.h"
#include "ICaloFutureHypoTool.h"
#include "ICaloFutureShowerOverlapTool.h"
// ============================================================================

/** @class CaloFutureMergedPi0 CaloFutureMergedPi0.h
 *
 *  Merged pi0 reconstruction with iterativ Method
 *
 * NEW IMPLEMENTATION
 *
 *  @author Olivier Deschamps
 *  @date   05/05/2014
 */

class CaloFutureMergedPi0
    : public Gaudi::Functional::MultiTransformer<std::tuple<LHCb::CaloClusters, LHCb::CaloHypos, LHCb::CaloHypos>(
                                                     const LHCb::CaloClusters&, const DeCalorimeter& ),
                                                 LHCb::DetDesc::usesConditions<DeCalorimeter>> {

public:
  CaloFutureMergedPi0( const std::string& name, ISvcLocator* svcloc );

  std::tuple<LHCb::CaloClusters, LHCb::CaloHypos, LHCb::CaloHypos> operator()( const LHCb::CaloClusters&,
                                                                               const DeCalorimeter& ) const override;

private:
  Gaudi::Property<float> m_etCut{this, "EtCut", 1500 * Gaudi::Units::MeV}; // minimal ET of clusters to consider
  Gaudi::Property<float> m_minET{this, "SplitPhotonMinET", 0.}; // minimal ET of resulting split-clusters and photons
  Gaudi::Property<int>   m_iter{this, "MaxIterations", 25};     // forwarded to CaloFutureShowerOverlapTool

  Gaudi::Property<bool> m_createClusterOnly{this, "CreateSplitClustersOnly",
                                            false}; // temporarily deprecated & unsupported

  PublicToolHandleArray<LHCb::Calo::Interfaces::IProcessHypos> m_gTools{this, "PhotonTools", {}};
  PublicToolHandleArray<LHCb::Calo::Interfaces::IProcessHypos> m_pTools{this, "Pi0Tools", {}};

  ToolHandle<ICaloFutureShowerOverlapTool> m_oTool{"CaloFutureShowerOverlapTool/SplitPhotonShowerOverlap", this};
  ToolHandle<FutureSubClusterSelectorTool> m_tagger{"FutureSubClusterSelectorTool/EcalClusterTag", this};
  ToolHandle<ICaloFutureClusterTool>       m_spread{"FutureClusterSpreadTool/EcalSpread", this};
  ToolHandle<ICaloFutureClusterTool>       m_cov{"FutureClusterCovarianceMatrixTool/EcalCovariance", this};

  mutable Gaudi::Accumulators::BinomialCounter<> m_cntNo2ndSeed{this, "Cluster without 2nd seed found"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_cntFailsToTagECluster1{this, "Fails to tag(E) cluster (1)"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_cntFailsToTagECluster2{this, "Fails to tag(E) cluster (2)"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_cntFailsToSetCovariance1{this, "Fails to set covariance (1)"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_cntFailsToSetCovariance2{this, "Fails to set covariance (2)"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_cntFailsToSetSpread1{this, "Fails to set spread (1)"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_cntFailsToSetSpread2{this, "Fails to set spread (2)"};

  mutable Gaudi::Accumulators::AveragingCounter<> m_cntPi0sSize{this, "clusters => mergedPi0s"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_cntSplitPhotonsSize{this, "clusters => splitPhotons"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_cntSplitClustersSize{this, "clusters => splitClusters"};

  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_g1_error{this, "Error from 'Tool' for g1"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_g2_error{this, "Error from 'Tool' for g2"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_pi0_error{this, "Error from 'Tool' for pi0"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_tag_failed{
      this, "SplitCluster tagging failed - keep the initial 3x3 tagging"};
};
// ============================================================================
#endif // CALOFUTUREMERGEDPI0_H
