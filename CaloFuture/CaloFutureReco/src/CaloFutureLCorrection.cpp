/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
#include "CaloFutureLCorrection.h"

DECLARE_COMPONENT( CaloFutureLCorrection )

CaloFutureLCorrection::CaloFutureLCorrection( const std::string& type, const std::string& name,
                                              const IInterface* parent )
    : extends( type, name, parent ) {

  // define conditionName
  const std::string uName( LHCb::CaloFutureAlgUtils::toUpper( name ) );
  if ( uName.find( "ELECTRON" ) != std::string::npos ) {
    m_conditionName = "Conditions/Reco/Calo/ElectronLCorrection";
  } else if ( uName.find( "MERGED" ) != std::string::npos || uName.find( "SPLITPHOTON" ) != std::string::npos ) {
    m_conditionName = "Conditions/Reco/Calo/SplitPhotonLCorrection";
  } else if ( uName.find( "PHOTON" ) ) {
    m_conditionName = "Conditions/Reco/Calo/PhotonLCorrection";
  }
  m_counterPerCaloFutureAreaDeltaZ.reserve( k_numOfCaloFutureAreas );
  m_counterPerCaloFutureAreaGamma.reserve( k_numOfCaloFutureAreas );
  m_counterPerCaloFutureAreaDelta.reserve( k_numOfCaloFutureAreas );
  std::vector<std::string> caloAreas = {"Outer", "Middle", "Inner", "PinArea"};
  for ( auto i = 0; i < k_numOfCaloFutureAreas; i++ ) {
    m_counterPerCaloFutureAreaDeltaZ.emplace_back( this, "Delta(Z) " + caloAreas[i] );
    m_counterPerCaloFutureAreaGamma.emplace_back( this, "<gamma> " + caloAreas[i] );
    m_counterPerCaloFutureAreaDelta.emplace_back( this, "<delta> " + caloAreas[i] );
  }
}

StatusCode CaloFutureLCorrection::finalize() {
  m_hypos.clear();
  return CaloFutureCorrectionBase::finalize();
}

StatusCode CaloFutureLCorrection::initialize() {
  /// first initialize the base class
  StatusCode sc = CaloFutureCorrectionBase::initialize();
  if ( sc.isFailure() ) return sc;

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Condition name : " << m_conditionName << endmsg;

  return sc;
}

StatusCode CaloFutureLCorrection::correct( LHCb::span<LHCb::CaloHypo* const>   hypos,
                                           std::optional<const_ref_range_type> wrap_ref_matching_tracks ) const {
  if ( wrap_ref_matching_tracks.has_value() && UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    debug() << "Relation table passed to L Correction. Not used here" << endmsg;
  }
  for ( auto* hypo : hypos ) {

    // check the Hypo
    if ( isHypoNotValid( hypo ) ) {
      return Error( "Invalid hypothesis -> no correction applied", StatusCode::SUCCESS );
    }

    if ( isEnergyNegative( hypo ) ) continue;

    const LHCb::CaloCluster* MainCluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo, true );
    if ( isNotMainCluster( MainCluster ) ) continue;

    const LHCb::CaloCluster::Entries&                entries = MainCluster->entries();
    const LHCb::CaloCluster::Entries::const_iterator iseed =
        LHCb::ClusterFunctors::locateDigit( entries.begin(), entries.end(), LHCb::CaloDigitStatus::SeedCell );
    if ( seedCellNotExist( entries, iseed ) ) continue;

    const LHCb::CaloDigit* seed = iseed->digit();
    if ( isNotSeed( seed ) ) continue;

    const double energy = hypo->e();
    // Cell ID for seed digit
    const auto cellID = seed->cellID();
    int        area   = cellID.area();
    assert( area >= 0 && area <= 2 ); // TODO: comment assert out

    // Account for the tilt
    const auto                plane = m_det->plane( CaloPlane::Front ); // Ecal Front-Plane
    const LHCb::CaloPosition* pos   = hypo->position();

    const double xg     = pos->x() - m_origin.X();
    const double yg     = pos->y() - m_origin.Y();
    const auto   normal = plane.Normal();
    const double Hesse  = plane.HesseDistance();
    const double z0     = ( -Hesse - normal.X() * pos->x() - normal.Y() * pos->y() ) / normal.Z();
    double       zg     = z0 - m_origin.Z();

    // hardcoded inner offset (+7.5 mm)
    if ( area == 2 ) { zg += 7.5; }

    // Uncorrected angle
    double xy_offset  = std::sqrt( xg * xg + yg * yg );
    double xyz_offset = std::sqrt( xg * xg + yg * yg + zg * zg );
    double tan_theta  = xy_offset / zg;
    double cos_theta  = zg / xyz_offset;

    // Corrected angle

    double gamma0 = getCorrection( CaloFutureCorrection::gamma0, cellID );
    double delta0 = getCorrection( CaloFutureCorrection::delta0, cellID );

    // NB: gammaP(ePrs = 0) != 0, deltaP(ePrs = 0) != 0 and depend on cellID.area()
    // get gammaP and deltaP parameters (depending on cellID.area() for each cluster
    double gammaP = getCorrection( CaloFutureCorrection::gammaP, cellID, 0. );
    double deltaP = getCorrection( CaloFutureCorrection::deltaP, cellID, 0. );
    double g      = gamma0 - gammaP;
    double d      = delta0 + deltaP;

    double tg_fps     = ( energy > 0.0 ? g * mylog( energy / Gaudi::Units::GeV ) + d : 0.0 );
    double temp       = ( 1. + tg_fps / xyz_offset );
    cos_theta         = temp / std::sqrt( tan_theta * tan_theta + temp * temp );
    const auto dz_fps = cos_theta * tg_fps;

    updateCounters( area, dz_fps, g, d );

    // Recompute Z position and fill CaloFuturePosition
    const auto zCor = z0 + dz_fps;
    hypo->position()->setZ( zCor );

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) { printDebugInfo( hypo, pos, dz_fps, z0, xg, yg, zCor ); }
  }
  return StatusCode::SUCCESS;
}

bool CaloFutureLCorrection::isHypoNotValid( const LHCb::CaloHypo* hypo ) const {
  const auto h = std::find( m_hypos.begin(), m_hypos.end(), hypo->hypothesis() );
  return m_hypos.end() == h;
}

bool CaloFutureLCorrection::isEnergyNegative( const LHCb::CaloHypo* hypo ) const {
  // No correction for negative energy :
  if ( hypo->e() < 0. ) {
    ++m_counterSkipNegativeEnergyCorrection;
    return true;
  }
  return false;
}

bool CaloFutureLCorrection::isNotMainCluster( const LHCb::CaloCluster* MainCluster ) const {
  // get cluster energy
  if ( !MainCluster ) {
    Error( "CaloCLuster* points to NULL!" ).ignore();
    return true;
  }
  return false;
}

bool CaloFutureLCorrection::seedCellNotExist( const LHCb::CaloCluster::Entries&                entries,
                                              const LHCb::CaloCluster::Entries::const_iterator iseed ) const {
  if ( entries.end() == iseed ) {
    Error( "The seed cell is not found !" ).ignore();
    return true;
  }
  return false;
}

bool CaloFutureLCorrection::isNotSeed( const LHCb::CaloDigit* seed ) const {
  if ( !seed ) {
    Error( "Seed digit points to NULL!" ).ignore();
    return true;
  }
  return false;
}

void CaloFutureLCorrection::updateCounters( int area, const double dz_fps, double g, double d ) const {
  m_counterPerCaloFutureAreaGamma.at( area ) += g;
  m_counterPerCaloFutureAreaDelta.at( area ) += d;
  m_counterPerCaloFutureAreaDeltaZ.at( area ) += dz_fps;
  m_counterDeltaZ += dz_fps;
}

void CaloFutureLCorrection::printDebugInfo( const LHCb::CaloHypo* hypo, const LHCb::CaloPosition* pos,
                                            const double dz_fps, double z0, double xg, double yg, double zCor ) const {
  debug() << "Hypothesis :" << hypo->hypothesis() << endmsg;
  debug() << " ENE  " << hypo->position()->e() << " "
          << "xg " << xg << " "
          << "yg " << yg << endmsg;
  debug() << "zg " << pos->z() << " "
          << "z0 " << z0 << " "
          << "DeltaZ " << dz_fps << " "
          << "zCor " << zCor << endmsg;
}
