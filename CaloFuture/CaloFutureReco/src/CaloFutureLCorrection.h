/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_CALOFUTURELCORRECTION_H
#define CALOFUTURERECO_CALOFUTURELCORRECTION_H 1

// from STL
#include <cmath>
#include <string>

// CaloFuture
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureCorrectionBase.h"
#include "GaudiKernel/Counters.h"
#include "ICaloFutureHypoTool.h"

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// Event
#include "Event/CaloHypo.h"

/** @class CaloFutureLCorrection CaloFutureLCorrection.h
 *
 *
 *  @author Deschamps Olivier
 *  @date   2003-03-10
 *  revised 2010

 *  Adam Szabelski
 *  date 2019-10-15
 */

class CaloFutureLCorrection : public extends<CaloFutureCorrectionBase, LHCb::Calo::Interfaces::IProcessHypos> {
public:
  typedef std::reference_wrapper<const LHCb::CaloFuture2Track::IClusTrTable::Range>
             const_ref_range_type; // this makes it possible to be used by std::optional for matching_tracks
  StatusCode correct( LHCb::span<LHCb::CaloHypo* const>   hypos,
                      std::optional<const_ref_range_type> matching_tracks = std::nullopt )
      const override; // default value needed in this suntax for calling process(hypo) without the second argument
  StatusCode process( LHCb::span<LHCb::CaloHypo* const> hypos ) const override { return correct( hypos ); }

public:
  StatusCode initialize() override;
  StatusCode finalize() override;

  CaloFutureLCorrection( const std::string& type, const std::string& name, const IInterface* parent );

private:
  using IncCounter = Gaudi::Accumulators::Counter<>;
  using SCounter   = Gaudi::Accumulators::StatCounter<float>;

  mutable IncCounter m_counterSkipNegativeEnergyCorrection{this, "Skip negative energy correction"};
  mutable SCounter   m_counterDeltaZ{this, "Delta(Z)"};

  static constexpr int          k_numOfCaloFutureAreas{4};
  mutable std::vector<SCounter> m_counterPerCaloFutureAreaDeltaZ;
  mutable std::vector<SCounter> m_counterPerCaloFutureAreaGamma;
  mutable std::vector<SCounter> m_counterPerCaloFutureAreaDelta;

  bool isHypoNotValid( const LHCb::CaloHypo* hypo ) const;

  bool isEnergyNegative( const LHCb::CaloHypo* hypo ) const;

  bool isNotMainCluster( const LHCb::CaloCluster* MainCluster ) const;

  bool seedCellNotExist( const LHCb::CaloCluster::Entries& entries,
                         const LHCb::CaloCluster::Entries::const_iterator ) const;

  bool isNotSeed( const LHCb::CaloDigit* seed ) const;

  void updateCounters( int area, const double dz_fps, double g, double d ) const;

  void printDebugInfo( const LHCb::CaloHypo* hypo, const LHCb::CaloPosition* pos, const double dz_fps, double z0,
                       double xg, double yg, double zCor ) const;
};
// ============================================================================
#endif // CALOFUTURERECO_CALOFUTURELCORRECTION_H
