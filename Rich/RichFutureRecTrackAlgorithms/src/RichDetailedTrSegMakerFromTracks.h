/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <iostream>
#include <limits>
#include <memory>
#include <optional>
#include <string>
#include <vector>

// base class
#include "RichBaseTrSegMaker.h"

// Gaudi
#include "GaudiKernel/IMagneticFieldSvc.h"
#include "GaudiKernel/ToolHandle.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Event Model
#include "Event/Track.h"

// Rich Utils
#include "RichUtils/BoostArray.h"
#include "RichUtils/RichRayTracingUtils.h"
#include "RichUtils/RichTrackSegment.h"

// Rich Det
#include "RichDet/DeRichRadiator.h"

// Interfaces
#include "GaudiAlg/IGenericTool.h"
#include "RichInterfaces/IRichDetParameters.h"
#include "RichInterfaces/IRichRadiatorTool.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackStateProvider.h"

// Rec event model
#include "RichFutureRecEvent/RichRecRelations.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {
    /// The input track container type
    using InData = LHCb::Track::Selection;
    // using InData  = LHCb::Track::Range;
    /// The output data
    using OutData = std::tuple<LHCb::RichTrackSegment::Vector,     //
                               Relations::TrackToSegments::Vector, //
                               Relations::SegmentToTrackVector>;
  } // namespace

  /** @class DetailedTrSegMakerFromTracks RichDetailedTrSegMakerFromTracks.h
   *
   *  Builds RichTrackSegments from LHCb::Tracks.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class DetailedTrSegMakerFromTracks final
      : public MultiTransformer<OutData( const InData& ), Traits::BaseClass_t<BaseTrSegMaker>> {

  public:
    /// Standard constructor
    DetailedTrSegMakerFromTracks( const std::string& name, ISvcLocator* pSvcLocator );

    /// Initialization after creation
    StatusCode initialize() override;

  public:
    /// Algorithm execution via transform
    OutData operator()( const InData& tracks ) const override;

  private:
    // methods

    /// Construct the track segments for a given track
    void constructSegments( const LHCb::Track&                  track,       ///< track object
                            LHCb::RichTrackSegment::Vector&     segments,    ///< segments container
                            const LHCb::Tracks::size_type       tkIndex,     ///< currect track index
                            Relations::TrackToSegments::Vector& tkToSegsRel, ///< track -> segment relations
                            Relations::SegmentToTrackVector&    segToTkRel,  ///< segment -> track relations
                            const bool                          gecAbort     ///< GEC fired
                            ) const;

    /** Find all intersections with the given radiator volume(s)
     *  @return The number of radiator intersections
     */
    inline void                                                       //
    getRadIntersections( const Gaudi::XYZPoint&         point,        ///< The start point
                         const Gaudi::XYZVector&        direction,    ///< The direction from the start point
                         const Rich::RadiatorType       rad,          ///< The radiator
                         Rich::RadIntersection::Vector& intersections ///< Intersections with the given radiator
                         ) const {
      // clear the intersections
      intersections.clear();
      // get the intersections
      m_radTool.get()->intersections( point, direction, rad, intersections );
    }

    /** Extrapolate a state to a new z position
     *
     * @param stateToMove  The state to extrapolate
     * @param track        The Track object
     * @param z            The z position to extrapolate the state to
     * @param refState     Reference starting state.
     *
     * @return The status of the extrapolation
     * @retval true  State was successfully extrapolated to the new z position
     * @retval false State could not be extrapolated to the z position.
     *               State remains unaltered.
     */
    bool moveState( LHCb::State&             stateToMove, //
                    const LHCb::Track&       track,       //
                    const double             z,           //
                    const Rich::DetectorType rich         //
                    ) const;

    /// Check if a track type should be skipped in a given radiator
    inline bool skipByType( const LHCb::Track&       track, //
                            const Rich::RadiatorType rad    //
                            ) const noexcept {
      // Skip RICH2 for Upstream tracks and RICH1 for T tracks
      return ( ( Rich::Rich1Gas == rad && LHCb::Track::Types::Ttrack == track.type() ) ||
               ( Rich::Rich2Gas == rad && LHCb::Track::Types::Upstream == track.type() ) );
    }

    /// Try entry state first for radiator intersections ?
    inline bool entryStateForIntersects( const LHCb::Track&       track, //
                                         const Rich::RadiatorType rad    //
                                         ) const noexcept {
      return !( Rich::Rich1Gas == rad && LHCb::Track::Types::Downstream == track.type() );
    }

    /// Track data
    struct TrackData {
      LHCb::RichTrackSegment::StateErrors errors;
      Gaudi::XYZPoint                     point;
      Gaudi::XYZVector                    momentum;
      bool                                status{false};
    };

    /// Creates the middle point information
    TrackData createMiddleInfo( const LHCb::Track&       track,  //
                                const Rich::DetectorType rich,   //
                                LHCb::State&             fState, //
                                LHCb::State&             lState  //
                                ) const;

    /// Access the magnetic field service
    inline const IMagneticFieldSvc* magFieldSvc() const noexcept { return m_magFieldSvc; }

  private:
    // data caches

    /// Cache min radius^2 at exit
    RadiatorArray<double> m_minRadLengthSq = {{}};

  private:
    // tools, det elems etc.

    /// Rich1 and Rich2 detector elements
    DetectorArray<DeRich*> m_rich = {{}};

    /// Type for pointers to RICH radiator detector elements
    using Radiators = std::vector<const DeRichRadiator*>;
    /// Pointers to RICH radiator detector elements
    Radiators m_radiators;

    /// Pointer to the Magnetic Field Service
    IMagneticFieldSvc* m_magFieldSvc = nullptr;

    /// Track state provider
    ToolHandle<const ITrackStateProvider> m_trStateP{this, "TrackStateProvider", "TrackStateProvider/StateProvider"};

    /// Track extrapolator ( Best options either TrackSTEPExtrapolator or TrackRungeKuttaExtrapolator )
    ToolHandle<const ITrackExtrapolator> m_trExt{this, "TrackExtrapolator",
                                                 "TrackRungeKuttaExtrapolator/TrackExtrapolator"};

    /// Pointer to the radiator intersections tool
    ToolHandle<const IRadiatorTool> m_radTool{this, "RadiatorTool", "Rich::Future::RadiatorTool/Radiators"};

    /// Pointer to detector radiator parameters
    ToolHandle<const IDetParameters> m_detParameters{this, "DetParameters", "Rich::Future::DetParameters"};

  private:
    // properties

    /// Allowable tolerance on state z positions
    Gaudi::Property<RadiatorArray<double>> m_zTolerance{
        this,
        "ZTolerances",
        {10 * Gaudi::Units::mm, 10 * Gaudi::Units::mm, 10 * Gaudi::Units::mm},
        "Tolerances on the z positions of requested state positions."};

    /// Nominal z positions of states at RICHes
    Gaudi::Property<std::array<double, 2 * Rich::NRiches>> m_nomZstates{
        this,
        "NominalStateZ",
        {
            990 * Gaudi::Units::mm,  ///< Place to look for Rich1 entry state
            2165 * Gaudi::Units::mm, ///< Place to look for Rich1 exit state
            9450 * Gaudi::Units::mm, ///< Place to look for Rich2 entry state
            11900 * Gaudi::Units::mm ///< Place to look for Rich2 exit state
        },
        "The z positions to look for state at the entry/exit of RICH1/RICH2."};

    /// Shifts for mirror correction
    Gaudi::Property<DetectorArray<double>> m_mirrShift{
        this,
        "MirrorShiftCorr",
        {35 * Gaudi::Units::cm, 150 * Gaudi::Units::cm},
        "z shift values to move state to be on the 'inside' of the primary mirrors."};

    /// Sanity checks on state information
    Gaudi::Property<RadiatorArray<double>> m_minStateDiff{
        this,
        "ZSanityChecks",
        {1 * Gaudi::Units::mm, 25 * Gaudi::Units::mm, 50 * Gaudi::Units::mm},
        "Minimum difference in z between the final found entry and exit points."};

    /// Minimum state movement in z to bother with (RICH1,RICH2)
    Gaudi::Property<DetectorArray<double>> m_minZmove{
        this,
        "MinimumZMove",
        {1 * Gaudi::Units::mm, 1 * Gaudi::Units::mm},
        "Minimum state z shift to bother performing state extrapolation."};

    /** Min z movement for full extrapolator treatment.
     *  Below this, just basic linear extrapolation. (RICH1,RICH2) */
    Gaudi::Property<DetectorArray<double>> m_linZmove{
        this,
        "MinimumZMoveForExtrap",
        {30 * Gaudi::Units::mm, 30 * Gaudi::Units::mm},
        "Minimum z shift to use state extrapolator. Otherwise a basic linear extrapolation is used."};

    /// Min radius at exit for each radiator
    Gaudi::Property<RadiatorArray<double>> m_minRadLength{
        this,
        "MinRadiatorPathLength",
        {0 * Gaudi::Units::mm, 500 * Gaudi::Units::mm, 1500 * Gaudi::Units::mm},
        "Minimum path length in each RICH radiator volume."};

    /// Flag to turn on/off the use of the TrackStateProvider to create missing states
    Gaudi::Property<bool> m_createMissingStates{
        this, "CreateMissingStates", false,
        "Activate the creation of missing states at requested z position using state provider tool."};

    /// Use the State provider instead of the extrapolator to move states
    Gaudi::Property<bool> m_useStateProvider{
        this, "UseStateProvider", false, //
        "Use the state provider tool instead of extrapolation tool to move states."};

    /** Flag to turn on extrapolating both first and last states, and taking an average
     *  when creating the middle point information. Default (false) is to just use
     *  the first state */
    Gaudi::Property<bool> m_useBothStatesForMiddle{
        this, "UseBothStatesForMiddle", false,
        "Use an average of the entry and exit state information when computing the middle point state. " //
        "Default is to just use the entry state."};

    /// Maximum momentum to 'finesse' state info to take any curvature into account
    Gaudi::Property<RadiatorArray<double>> m_maxPstateFinesse{
        this,
        "MaxPCurvatureFinesse",
        {10 * Gaudi::Units::GeV, 10 * Gaudi::Units::GeV},
        "Maximum track momentum value in RICH1/RICH2 to perform a finessing of the radiator intersections "
        "to account for track curvature."};

    /// Preload Geometry ?
    Gaudi::Property<bool> m_preload{
        this, "PreloadGeometry", false,
        "Preload the full LHCb geometry during initialisation. Useful for CPU timing studies."};

    /// Max number tracks GEC
    Gaudi::Property<unsigned int> m_maxTracks{this, "MaxTracks", //
                                              std::numeric_limits<unsigned int>::max(),
                                              "Maximum input track global event cut."};

  private:
    // messaging

    /// empty relations table
    mutable WarningCounter m_TkGecWarn{this, "Too many tracks -> Processing aborted"};

    /// Create missing entry state
    mutable WarningCounter m_createMissingEntrState{this, "Creating missing entry State"};

    /// Create missing exit state
    mutable WarningCounter m_createMissingExitState{this, "Creating missing exit State"};

    /// Failed to find entry state
    mutable WarningCounter m_failedEntryState{this, "Failed to find appropriate entry State"};

    /// Failed to find exit state
    mutable WarningCounter m_failedExitState{this, "Failed to find appropriate exit State"};

    /// Problem getting State from Track
    mutable ErrorCounter m_stateFromTrackErr{this, "Problem getting track state"};
  };

} // namespace Rich::Future::Rec
