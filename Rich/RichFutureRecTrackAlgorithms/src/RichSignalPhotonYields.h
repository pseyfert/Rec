/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <iomanip>
#include <tuple>

// Event Model
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/PhysicalConstants.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Utils
#include "RichUtils/ZipRange.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {
    /// Output data type
    using OutData = std::tuple<PhotonYields::Vector, PhotonSpectra::Vector>;
  } // namespace

  /** @class SignalPhotonYields RichSignalPhotonYields.h
   *
   *  Computes the emitted photon yield data from Track Segments.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class SignalPhotonYields final : public MultiTransformer<OutData( const PhotonYields::Vector&,  //
                                                                    const PhotonSpectra::Vector&, //
                                                                    const GeomEffs::Vector& ),
                                                           Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    SignalPhotonYields( const std::string& name, ISvcLocator* pSvcLocator );

  public:
    /// Algorithm execution via transform
    OutData operator()( const PhotonYields::Vector&  detYields,  //
                        const PhotonSpectra::Vector& detSpectra, //
                        const GeomEffs::Vector&      geomEffs ) const override;
  };

} // namespace Rich::Future::Rec
