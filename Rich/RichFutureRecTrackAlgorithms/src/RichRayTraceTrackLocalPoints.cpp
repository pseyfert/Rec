/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichRayTraceTrackLocalPoints.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

RayTraceTrackLocalPoints::RayTraceTrackLocalPoints( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   // input data
                   {KeyValue{"TrackGlobalPointsLocation", SpacePointLocation::SegmentsGlobal}},
                   // output data
                   {KeyValue{"TrackLocalPointsLocation", SpacePointLocation::SegmentsLocal}} ) {
  // debugging
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

SegmentPanelSpacePoints::Vector //
RayTraceTrackLocalPoints::operator()( const SegmentPanelSpacePoints::Vector& gPoints ) const {

  SegmentPanelSpacePoints::Vector lPoints;
  lPoints.reserve( gPoints.size() );

  // Convert the global points
  for ( const auto& gPoint : gPoints ) {
    if ( gPoint.ok() ) {
      // convert the global info to local
      lPoints.emplace_back( m_idTool.get()->globalToPDPanel( gPoint.point() ),
                            m_idTool.get()->globalToPDPanel( gPoint.point( Rich::left ) ),
                            m_idTool.get()->globalToPDPanel( gPoint.point( Rich::right ) ), gPoint.bestSide(),
                            gPoint.closestPD() );
    } else {
      // save a default object
      lPoints.emplace_back();
    }
    _ri_verbo << "Segment PD panel point (local) " << lPoints.back() << endmsg;
  }

  return lPoints;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RayTraceTrackLocalPoints )

//=============================================================================
