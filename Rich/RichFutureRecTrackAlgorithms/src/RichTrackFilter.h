/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <tuple>

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureKernel/RichAlgBase.h"

// Event model
#include "Event/Track.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {
    /// Output data type
    using OutData = std::tuple<LHCb::Track::Selection,  // Long
                               LHCb::Track::Selection,  // Downstream
                               LHCb::Track::Selection>; // Upstream
  }                                                     // namespace

  /** @class TrackFilter RichTrackFilter.h
   *
   *  (Temporary) algorithm that takes an input track location and splits
   *  it in shared containers by type.
   *
   *  Stop gap solution until a more general LoKiFunctor implementation is available.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackFilter final : public MultiTransformer<OutData( const LHCb::Tracks& ), Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    TrackFilter( const std::string& name, ISvcLocator* pSvcLocator );

  public:
    /// Algorithm execution via transform
    OutData operator()( const LHCb::Tracks& tracks ) const override;
  };

} // namespace Rich::Future::Rec
