###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# --------------------------------------------------------------------------------------

from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from Configurables import CondDB, LHCbApp, GaudiSequencer
import os

# --------------------------------------------------------------------------------------

# Event numbers
nEvents = 1000
EventSelector().PrintFreq = 100

# Messaging
msgSvc = getConfigurable("MessageSvc")
msgSvc.Format = "% F%30W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
# Disable tool timing output
SequencerTimerTool().OutputLevel = 4

# Auditors
AuditorSvc().Auditors += ["FPEAuditor"]
from Configurables import FPEAuditor
FPEAuditor().ActivateAt = ["Execute"]

# The overall sequence. Filled below.
all = GaudiSequencer("All")

# Finally set up the application
ApplicationMgr(
    TopAlg=[all],
    EvtMax=nEvents,  # events to be processed
    ExtSvc=['ToolSvc', 'AuditorSvc'],
    AuditAlgorithms=True)

# --------------------------------------------------------------------------------------

# Fetch required data from file
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile
fetcher = FetchDataFromFile('FetchDSTData')
fetcher.DataKeys = ['Trigger/RawEvent', 'Rich/RawEvent', 'pRec/Track/Best']
all.Members += [fetcher]

# First various raw event decodings
from Configurables import createODIN
from Configurables import Rich__Future__RawBankDecoder as RichDecoder
all.Members += [
    createODIN("ODINFutureDecode"),
    RichDecoder("RichFutureDecode")
]

# Explicitly unpack the Tracks
from Configurables import UnpackTrack
all.Members += [UnpackTrack("UnpackTracks")]

# Filter the tracks by type
from Configurables import Rich__Future__Rec__TrackFilter as TrackFilter
tkFilt = TrackFilter("TrackTypeFilter")
all.Members += [tkFilt]

# DataType
dType = "Upgrade"

# Input tracks
tkLocs = {
    "Long": tkFilt.OutLongTracksLocation,
    "Down": tkFilt.OutDownTracksLocation,
    "Up": tkFilt.OutUpTracksLocation
}

# Output PID
pidLocs = {
    "Long": "Rec/Rich/LongPIDs",
    "Down": "Rec/Rich/DownPIDs",
    "Up": "Rec/Rich/UpPIDs"
}

# Merged output
finalPIDLoc = "Rec/Rich/PIDs"

# histograms
histos = "Expert"

# --------------------------------------------------------------------------------------
# The reconstruction
from RichFutureRecSys.ConfiguredRichReco import RichRecoSequence
RichRec = RichRecoSequence(
    dataType=dType,
    inputTrackLocations=tkLocs,
    outputPIDLocations=pidLocs,
    mergedOutputPIDLocation=finalPIDLoc)
all.Members += [RichRec]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Monitoring
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecMonitors
RichMoni = RichRecMonitors(
    inputTrackLocations=tkLocs, outputPIDLocations=pidLocs, histograms=histos)
all.Members += [RichMoni]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# UnpackMC
from Configurables import UnpackMCParticle, UnpackMCVertex
from Configurables import DataPacking__Unpack_LHCb__MCRichDigitSummaryPacker_ as RichSumUnPack
fetcher.DataKeys += ['pMC/Vertices', 'pMC/Particles']
mcUnpack = GaudiSequencer("UnpackMCSeq")
mcUnpack.Members += [
    UnpackMCVertex(),
    UnpackMCParticle(),
    RichSumUnPack("RichSumUnPack")
]
all.Members += [mcUnpack]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# MC Checking
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecCheckers
RichCheck = RichRecCheckers(
    inputTrackLocations=tkLocs, outputPIDLocations=pidLocs, histograms=histos)
all.Members += [RichCheck]
# --------------------------------------------------------------------------------------
