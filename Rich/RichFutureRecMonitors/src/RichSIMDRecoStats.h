/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecRelations.h"

// Kernel
#include "Kernel/RichRadiatorType.h"

// RichUtils
#include "RichUtils/RichPoissonEffFunctor.h"
#include "RichUtils/RichStatDivFunctor.h"
#include "RichUtils/ZipRange.h"

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class SIMDRecoStats RichSIMDRecoStats.h
   *
   *  Basic monitoring of the RICH reconstruction.
   *
   *  @author Chris Jones
   *  @date   2016-11-07
   */

  class SIMDRecoStats final : public Consumer<void( const LHCb::RichTrackSegment::Vector&,     //
                                                    const Relations::TrackToSegments::Vector&, //
                                                    const SIMDCherenkovPhoton::Vector& ),
                                              Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    SIMDRecoStats( const std::string& name, ISvcLocator* pSvcLocator );

  public:
    /// Functional operator
    void operator()( const LHCb::RichTrackSegment::Vector&     segments, //
                     const Relations::TrackToSegments::Vector& tkToSegs, //
                     const SIMDCherenkovPhoton::Vector&        photons ) const override;

  private: // data
    /// # tracks per event
    mutable Gaudi::Accumulators::StatCounter<> m_nTracks{this, "# Selected Tracks"};

    /// RICH selection efficiency
    mutable Gaudi::Accumulators::BinomialCounter<> m_richEff{this, "RICH selection efficiency"};

    /// # Segments per event
    mutable DetectorArray<Gaudi::Accumulators::StatCounter<>> m_nSegs{
        {{this, "# Rich1Gas Segments"}, {this, "# Rich2Gas Segments"}}};

    /// # Photons per event
    mutable DetectorArray<Gaudi::Accumulators::StatCounter<>> m_nPhots{
        {{this, "# Rich1Gas Photons"}, {this, "# Rich2Gas Photons"}}};
  };

} // namespace Rich::Future::Rec::Moni
