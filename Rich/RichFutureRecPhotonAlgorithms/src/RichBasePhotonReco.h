/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <algorithm>
#include <array>
#include <iterator>
#include <sstream>
#include <tuple>
#include <vector>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Range.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// RichDet
#include "RichDet/DeRichBeamPipe.h"

// Rich Utils
#include "RichUtils/RichGeomPhoton.h"
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Kernel
#include "Kernel/STLExtensions.h"

namespace Rich::Future::Rec {

  /** @class BasePhotonReco RichBasePhotonReco.h
   *
   *  Base class for photon reconstruction algorithms
   *
   *  @todo Base class is GaudiAlgorithm as long as 'getDet' is still required.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class BasePhotonReco : public AlgBase<GaudiAlgorithm> {

  public:
    /// Standard constructor
    BasePhotonReco( const std::string& name, ISvcLocator* pSvcLocator );

    /// Initialization after creation
    virtual StatusCode initialize() override;

  protected:
    /// Basic precision (float)
    using FP = SIMDCherenkovPhoton::FP;
    /// SIMD version of FP
    using SIMDFP = SIMDCherenkovPhoton::SIMDFP;
    /// Type for container of mirror pointers. Same size as SIMDFP.
    using Mirrors = SIMD::STDArray<const DeRichSphMirror*, SIMDFP>;

  protected: // helpers
    /// Data Guard for containers. Will remove the last element added
    /// unless explicitly told data is OK
    template <typename TYPE>
    class DataGuard final {
    public:
      /// Set OK for this guard.
      void setOK( const bool ok = true ) noexcept { m_ok = ok; }

    public:
      /// No default constructor.
      DataGuard() = delete;
      /// Constructor with the container to guard
      explicit DataGuard( TYPE& data ) : m_data( data ) {}
      /// Destructor. Removes the last added object unless explicitly told to keep it
      ~DataGuard() noexcept {
        if ( UNLIKELY( !m_ok ) ) { m_data.pop_back(); }
      }

    private:
      /// The data container
      TYPE& m_data;
      /// Status flag. Default to false so last entry removed unless explicitly saved.
      bool m_ok = false;
    };

  protected:
    /// Check if the points a and b are on the same detector side
    template <typename POINT>
    inline decltype( auto ) sameSide( const Rich::RadiatorType rad, //
                                      const POINT&             a,   //
                                      const POINT&             b ) const {
      return ( Rich::Rich2Gas == rad ? a.x() * b.x() > 0 : a.y() * b.y() > 0 );
    }

    /// Correction for CK theta
    inline double ckThetaCorrection( const Rich::RadiatorType rad ) const {
      return m_ckJOCorrs[rad] + m_ckBiasCorrs[rad];
    }

    /** Absolute maximum Cherenkov theta value to reconstuct for given track segment
     *
     *  @param segment The track segment
     *
     *  @return The maximum Cherenkov angle to reconstruct
     */
    inline double absMaxCKTheta( const Rich::RadiatorType rad ) const noexcept { return m_maxCKtheta[rad]; }

    /** Absolute minimum Cherenkov theta value to reconstuct for given track segment
     *
     *  @param segment The track segment
     *
     *  @return The minimum Cherenkov angle to reconstruct
     */
    inline double absMinCKTheta( const Rich::RadiatorType rad ) const noexcept { return m_minCKtheta[rad]; }

    /// Check the final Cherenkov theta angle (Scalar)
    template <typename HYPODATA, typename FTYPE,
              typename std::enable_if<std::is_arithmetic<FTYPE>::value>::type* = nullptr>
    inline decltype( auto ) checkAngle( const Rich::RadiatorType rad,        //
                                        const HYPODATA&          tkCkAngles, //
                                        const HYPODATA&          tkCkRes,    //
                                        const FTYPE              ckTheta ) const noexcept {
      return (
          // First the basic checks
          ckTheta < absMaxCKTheta( rad ) && ckTheta > absMinCKTheta( rad ) &&
          // Now check each real hypo
          std::any_of( activeParticlesNoBT().begin(), activeParticlesNoBT().end(),
                       [sigma = m_nSigma[rad], &ckTheta, &tkCkAngles, &tkCkRes]( const auto id ) {
                         return fabs( ckTheta - tkCkAngles[id] ) < ( sigma * tkCkRes[id] );
                       } ) );
    }

    /// Check the final Cherenkov theta angle (SIMD)
    template <typename HYPODATA, typename FTYPE, typename MASK = typename FTYPE::mask_type,
              typename std::enable_if<!std::is_arithmetic<FTYPE>::value>::type* = nullptr>
    inline void checkAngle( const Rich::RadiatorType rad,        //
                            const HYPODATA&          tkCkAngles, //
                            const HYPODATA&          tkCkRes,    //
                            const FTYPE&             ckTheta,    //
                            MASK&                    OK ) const noexcept {
      using namespace LHCb::SIMD;
      // First the basic checks
      OK &= ( ckTheta < m_maxCKthetaSIMD[rad] && ckTheta > m_minCKthetaSIMD[rad] );
      // Now check each hypo
      if ( LIKELY( any_of( OK ) ) ) {
        auto hypoMask = !OK;
        for ( const auto hypo : activeParticlesNoBT() ) {
          hypoMask |= ( abs( ckTheta - tkCkAngles[hypo] ) < ( m_nSigmaSIMD[rad] * tkCkRes[hypo] ) );
          if ( UNLIKELY( all_of( hypoMask ) ) ) { break; }
        }
        // Update the mask
        OK &= hypoMask;
      }
    }

  protected:
    /// SIMD Square of m_maxROI
    RadiatorArray<SIMDFP> m_maxROI2PreSelSIMD = {{}};

    /// SIMD Square of m_minROI
    RadiatorArray<SIMDFP> m_minROI2PreSelSIMD = {{}};

    /// SIMD Internal cached parameter for speed
    RadiatorArray<SIMDFP> m_scalePreSelSIMD = {{}};

    /// SIMD N sigma for acceptance bands for preselection
    RadiatorArray<SIMDFP> m_nSigmaPreSelSIMD = {{}};

    /// SIMD Absolute minimum allowed Cherenkov Angle
    RadiatorArray<SIMDFP> m_minCKthetaSIMD = {{}};

    /// SIMD Absolute maximum allowed Cherenkov Angle
    RadiatorArray<SIMDFP> m_maxCKthetaSIMD = {{}};

    /// SIMD N sigma for acceptance bands
    RadiatorArray<SIMDFP> m_nSigmaSIMD = {{}};

    /// SIMD CK theta correction factors
    RadiatorArray<SIMDFP> m_ckThetaCorrSIMD = {{}};

  protected:
    // Pre-sel options

    /// Min hit radius of interest around track centres for preselection
    Gaudi::Property<RadiatorArray<float>> m_minROIPreSel{this, "PreSelMinTrackROI", {230, 0, 0}};

    /// Max hit radius of interest around track centres for preselection
    Gaudi::Property<RadiatorArray<float>> m_maxROIPreSel{this, "PreSelMaxTrackROI", {540, 110, 165}};

    /// N sigma for acceptance bands for preselection
    Gaudi::Property<RadiatorArray<float>> m_nSigmaPreSel{this, "PreSelNSigma", {17, 6, 10}};

    // cache values for speed

    /// Square of m_maxROI
    RadiatorArray<float> m_maxROI2PreSel = {{}};

    /// Square of m_minROI
    RadiatorArray<float> m_minROI2PreSel = {{}};

    /// Internal cached parameter for speed
    RadiatorArray<float> m_scalePreSel = {{}};

  protected:
    // reco options

    /// Check for photons that cross between the different RICH 'sides'
    Gaudi::Property<RadiatorArray<bool>> m_checkPhotCrossSides{this, "CheckSideCrossing", {false, false, false}};

    /// Absolute minimum allowed Cherenkov Angle
    Gaudi::Property<RadiatorArray<float>> m_minCKtheta{
        this,
        "MinAllowedCherenkovTheta",
        {0.150, 0.005, 0.005},
        "The minimum allowed CK theta values for each radiator (Aero/R1Gas/R2Gas)"};

    /// Absolute maximum allowed Cherenkov Angle
    Gaudi::Property<RadiatorArray<float>> m_maxCKtheta{
        this,
        "MaxAllowedCherenkovTheta",
        {0.310, 0.055, 0.032},
        "The maximum allowed CK theta values for each radiator (Aero/R1Gas/R2Gas)"};

    /// N sigma for acceptance bands
    Gaudi::Property<RadiatorArray<float>> m_nSigma{
        this, "NSigma", {9.0, 3.50, 4.40}, "The CK theta # sigma selection range for each radiator (Aero/R1Gas/R2Gas)"};

    /** Cherenkov theta bias corrections, specific for each photon
     *  reconstruction method. */
    RadiatorArray<float> m_ckBiasCorrs = {{}};

  private:
    /** Job-Option Corrections applied to the reconstructed theta vales.
     *  By default 0. */
    Gaudi::Property<RadiatorArray<float>> m_ckJOCorrs{this, "CKThetaQuartzRefractCorrections", {0.0, 0.0, 0.0}};

    // Parameters for the scale factor calculations

    // The CK theta value
    Gaudi::Property<RadiatorArray<float>> m_ckThetaScale{this, "ScaleFactorCKTheta", {0.24, 0.052, 0.03}};

    // The seperation the scale factors apply to
    Gaudi::Property<RadiatorArray<float>> m_sepGScale{this, "ScaleFactorSepG", {342, 98.2, 130}};
  };

} // namespace Rich::Future::Rec
